USB "driver" from SAGE2 pointer
============================
   * Luc Renambot - September 2014
   * renambot@gmail.com

Based and expanded on some code from CalVR
-------------
   * (http://ivl.calit2.net/wiki/index.php/CalVR)
   * probably by Andrew Prudhomme


Find your device
-------------
* lsusb    (might need to install `usbutils` package)
      * ...
      * Bus 007 Device 005: ID 0c16:0002 Gyration, Inc. RF Technology Receiver
      * ...

Change permission
-------------
   * enable access to the USB device
      * chmod 777 /dev/bus/usb/007/005

Run once to find 'vendorid' and 'productid' of your device
-------------
   * Manufacturer: Gyration
   * Product: Gyration RF Technology Receiver
   * VendorID: 3094
   * ProductID: 2

Edit usb.cfg
-------------
   * vendorid 3094
   * productid 2
   * url wss://iridium.evl.uic.edu:443
   * label Souris


UDEV
====

   * Could you try with this: ( file is /etc/udev/rules.d/99-logitech.rules )
   * Code: Select all
   * KERNEL="mouse*", BUS="usb", SYSFS{manufacturer}=="Logitech", MODE="0664", GROUP="users", OWNER="root", NAME="input/mice"

