

window.vtkRefs = vtkVolumeReferences;

var macro = vtkVolumeReferences.macro,
HttpDataAccessHelper = vtkVolumeReferences.HttpDataAccessHelper,
vtkBoundingBox = vtkVolumeReferences.vtkBoundingBox,
vtkColorTransferFunction = vtkVolumeReferences.vtkColorTransferFunction,
vtkFullScreenRenderWindow = vtkVolumeReferences.vtkFullScreenRenderWindow,
vtkPiecewiseFunction = vtkVolumeReferences.vtkPiecewiseFunction,
vtkVolumeController = vtkVolumeReferences.vtkVolumeController,
vtkURLExtract = vtkVolumeReferences.vtkURLExtract,
vtkVolume = vtkVolumeReferences.vtkVolume,
vtkVolumeMapper = vtkVolumeReferences.vtkVolumeMapper,
vtkXMLImageDataReader = vtkVolumeReferences.vtkXMLImageDataReader,
vtkFPSMonitor = vtkVolumeReferences.vtkFPSMonitor
;

var style = {
    fpsMonitor: "fpsMonitor",
    progress: "progress",
    bigFileDrop: "bigFileDrop",
};

let autoInit = true;
const userParams = vtkURLExtract.extractURLParameters();
const fpsMonitor = vtkFPSMonitor.newInstance();

// ----------------------------------------------------------------------------
// Add class to body if iOS device
// ----------------------------------------------------------------------------

const iOS = /iPad|iPhone|iPod/.test(window.navigator.platform);

if (iOS) {
  document.querySelector('body').classList.add('is-ios-device');
}

// ----------------------------------------------------------------------------

function emptyContainer(container) {
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
}

// ----------------------------------------------------------------------------

function preventDefaults(e) {
  e.preventDefault();
  e.stopPropagation();
}

// ----------------------------------------------------------------------------

function createViewer(rootContainer, fileContents, options) {
  
  SAGE2_AppState.titleUpdate("VTK Volume Viewer - " + vtkRefs.filenameNoExtension);

  const background = options.background
    ? options.background.split(',').map((s) => Number(s))
    : [0, 0, 0];
  const containerStyle = options.containerStyle;
  const fullScreenRenderer = vtkFullScreenRenderWindow.newInstance({
    background,
    rootContainer,
    containerStyle,
  });
  const renderer = fullScreenRenderer.getRenderer();

  const renderWindow = fullScreenRenderer.getRenderWindow();
  renderWindow.getInteractor().setDesiredUpdateRate(15);

  const vtiReader = vtkXMLImageDataReader.newInstance();
  vtiReader.parseAsArrayBuffer(fileContents);

  const source = vtiReader.getOutputData(0);
  const mapper = vtkVolumeMapper.newInstance();
  const actor = vtkVolume.newInstance();

  const dataArray =
    source.getPointData().getScalars() || source.getPointData().getArrays()[0];
  const dataRange = dataArray.getRange();

  const lookupTable = vtkColorTransferFunction.newInstance();
  const piecewiseFunction = vtkPiecewiseFunction.newInstance();

  // Pipeline handling
  actor.setMapper(mapper);
  mapper.setInputData(source);
  renderer.addActor(actor);

  // Configuration
  const sampleDistance =
    0.7 *
    Math.sqrt(
      source
        .getSpacing()
        .map((v) => v * v)
        .reduce((a, b) => a + b, 0)
    );
  mapper.setSampleDistance(sampleDistance);
  actor.getProperty().setRGBTransferFunction(0, lookupTable);
  actor.getProperty().setScalarOpacity(0, piecewiseFunction);
  // actor.getProperty().setInterpolationTypeToFastLinear();
  actor.getProperty().setInterpolationTypeToLinear();

  // For better looking volume rendering
  // - distance in world coordinates a scalar opacity of 1.0
  actor
    .getProperty()
    .setScalarOpacityUnitDistance(
      0,
      vtkBoundingBox.getDiagonalLength(source.getBounds()) /
        Math.max(...source.getDimensions())
    );
  // - control how we emphasize surface boundaries
  //  => max should be around the average gradient magnitude for the
  //     volume or maybe average plus one std dev of the gradient magnitude
  //     (adjusted for spacing, this is a world coordinate gradient, not a
  //     pixel gradient)
  //  => max hack: (dataRange[1] - dataRange[0]) * 0.05
  actor.getProperty().setGradientOpacityMinimumValue(0, 0);
  actor
    .getProperty()
    .setGradientOpacityMaximumValue(0, (dataRange[1] - dataRange[0]) * 0.05);
  // - Use shading based on gradient
  actor.getProperty().setShade(true);
  actor.getProperty().setUseGradientOpacity(0, true);
  // - generic good default
  actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
  actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
  actor.getProperty().setAmbient(0.2);
  actor.getProperty().setDiffuse(0.7);
  actor.getProperty().setSpecular(0.3);
  actor.getProperty().setSpecularPower(8.0);

  // Control UI
  const controllerWidget = vtkVolumeController.newInstance({
    size: [400, 150],
    rescaleColorMap: true,
  });
  const isBackgroundDark = background[0] + background[1] + background[2] < 1.5;
  controllerWidget.setContainer(rootContainer);
  controllerWidget.setupContent(renderWindow, actor, isBackgroundDark);
  fullScreenRenderer.setResizeCallback(({ width, height }) => {
    // 2px padding + 2x1px boder + 5px edge = 14
    if (width > 414) {
      controllerWidget.setSize(400, 150);
    } else {
      controllerWidget.setSize(width - 14, 150);
    }
    controllerWidget.render();
    fpsMonitor.update();
  });

  // First render
  renderer.resetCamera();
  renderWindow.render();

  // global.pipeline = {
  //   actor,
  //   renderer,
  //   renderWindow,
  //   lookupTable,
  //   mapper,
  //   source,
  //   piecewiseFunction,
  //   fullScreenRenderer,
  // };

  if (userParams.fps) {
    const fpsElm = fpsMonitor.getFpsMonitorContainer();
    fpsElm.classList.add(style.fpsMonitor);
    fpsMonitor.setRenderWindow(renderWindow);
    fpsMonitor.setContainer(rootContainer);
    fpsMonitor.update();
  }

		// ------------------------------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------------------------------
  // Add references to interact later
  vtkRefs.volumeControlWidget = controllerWidget;
  vtkRefs.renderer = renderer;
  vtkRefs.camera = renderer.getActiveCamera()
  vtkRefs.renderWindow = renderWindow;
  vtkRefs.redraw = function() {
    // Needs to reset the clipping plane so the center of the world isn't cut off (usually where object is)
    vtkRefs.renderer.resetCameraClippingRange();
    // This draws
    vtkRefs.renderWindow.render();
  };
  vtkRefs.getCameraState = function() {
    let state = {};
    state.cameraPosition = vtkRefs.camera.getPosition();
    state.cameraFocalPoint = vtkRefs.camera.getFocalPoint();
    state.cameraViewAngle = vtkRefs.camera.getViewAngle();
    state.cameraViewUp = vtkRefs.camera.getViewUp();
    return state;
  };
  vtkRefs.setCameraState = function(position, focalPoint, viewAngle, viewUp) {
    let a = position;
    vtkRefs.camera.setPosition(a[0], a[1], a[2]);
    a = focalPoint;
    vtkRefs.camera.setFocalPoint(a[0], a[1], a[2]);
    vtkRefs.camera.setViewAngle(viewAngle);
    a = viewUp;
    vtkRefs.camera.setViewUp(a[0], a[1], a[2]);
  }
  vtkRefs.getCameraPosition = function(){
    // returns array [3] x,y,z
    return vtkRefs.camera.getPosition();
    /*
  renderer.getActiveCamera().setPhysicalViewUp(0, -1, 0);
  renderer.getActiveCamera().setPhysicalViewNorth(0, 0, 1);
  renderer.getActiveCamera().setFocalPoint(0, 0, 1);
  renderer.getActiveCamera().setPosition(0, 0, -50);
  renderer.getActiveCamera().setViewAngle(80);
    */
  };
  vtkRefs.setCameraPosition = function(array){
    vtkRefs.camera.setPosition(array[0], array[1], array[2]);
  };

  // Send back camera values if changes have been applied
  vtkRefs.state = {};
  vtkRefs.state.preventNumberOfUpdates = 0;
  vtkRefs.state.cameraPosition = [0, 0, 0];
  // Interval for camera update
  vtkRefs.state.cameraUpdater = function() {
    if (vtkRefs.state.preventNumberOfUpdates > 0) {
      vtkRefs.state.preventNumberOfUpdates--;
      return;
    }
    let cpos = vtkRefs.getCameraPosition();
    let opos = vtkRefs.state.cameraPosition;
    if ((cpos[0] !== opos[0]) || (cpos[1] !== opos[1]) || (cpos[2] !== opos[2])){
      vtkRefs.state.cameraPosition = cpos;

      var camState = vtkRefs.getCameraState();
      var keys = Object.keys(camState);
      // skip the first one
      for (let i = 1; i < keys.length; i++) {
        // nameOfValue, value, propagate update
        SAGE2_AppState.setValue(keys[i], camState[keys[i]], false);
      }
      // Now do the first one to propagate changes
      SAGE2_AppState.setValue(keys[0], camState[keys[0]], true);
    }
  };
  setInterval(vtkRefs.state.cameraUpdater, 100);

  // Add a full handler to the state update
  SAGE2_AppState.addFullStateHandler(function(state) {
    // vtkRefs.setCameraState = function(position, focalPoint, viewAngle, viewUp)
    vtkRefs.setCameraState(state.cameraPosition,
                          state.cameraFocalPoint,
                          state.cameraViewAngle,
                          state.cameraViewUp);
    vtkRefs.state.cameraPosition = state.cameraPosition;
    vtkRefs.state.preventNumberOfUpdates = 5;
    vtkRefs.redraw();
  });
  
  
} // end createViewer












// ----------------------------------------------------------------------------

function load(container, options) {
  autoInit = false;
  emptyContainer(container);

  if (options.file) {
    if (options.ext === 'vti') {
      const reader = new FileReader();
      reader.onload = function onLoad(e) {
        createViewer(container, reader.result, options);
      };
      reader.readAsArrayBuffer(options.file);
    } else {
      console.error('Unkown file...');
    }
  } else if (options.fileURL) {
    const progressContainer = document.createElement('div');
    progressContainer.setAttribute('class', style.progress);
    container.appendChild(progressContainer);

    const progressCallback = (progressEvent) => {
      if (progressEvent.lengthComputable) {
        const percent = Math.floor(
          100 * progressEvent.loaded / progressEvent.total
        );
        progressContainer.innerHTML = `Loading ${percent}%`;
      } else {
        progressContainer.innerHTML = macro.formatBytesToProperUnit(
          progressEvent.loaded
        );
      }
    };

    HttpDataAccessHelper.fetchBinary(options.fileURL, {
      progressCallback,
    }).then((binary) => {
      container.removeChild(progressContainer);
      createViewer(container, binary, options);
    });
  }
}

function initLocalFileLoader(container) {
  const exampleContainer = document.querySelector('.content');
  const rootBody = document.querySelector('body');
  const myContainer = container || exampleContainer || rootBody;

  const fileContainer = document.createElement('div');
  fileContainer.innerHTML = `<div class="${
    style.bigFileDrop
  }"/><input type="file" accept=".vti" style="display: none;"/>`;
  myContainer.appendChild(fileContainer);

  const fileInput = fileContainer.querySelector('input');

  function handleFile(e) {
    preventDefaults(e);
    const dataTransfer = e.dataTransfer;
    const files = e.target.files || dataTransfer.files;
    if (files.length === 1) {
      myContainer.removeChild(fileContainer);
      const ext = files[0].name.split('.').slice(-1)[0];
      const options = Object.assign({ file: files[0], ext }, userParams);
      load(myContainer, options);
    }
  }

  fileInput.addEventListener('change', handleFile);
  fileContainer.addEventListener('drop', handleFile);
  fileContainer.addEventListener('click', (e) => fileInput.click());
  fileContainer.addEventListener('dragover', preventDefaults);
}

// Look at URL an see if we should load a file
// ?fileURL=https://data.kitware.com/api/v1/item/59cdbb588d777f31ac63de08/download
if (userParams.fileURL) {
  const exampleContainer = document.querySelector('.content');
  const rootBody = document.querySelector('body');
  const myContainer = exampleContainer || rootBody;
  load(myContainer, userParams);
  let filenameNoExtension = userParams.fileURL;
  filenameNoExtension = filenameNoExtension.split("/");
  filenameNoExtension = filenameNoExtension[filenameNoExtension.length - 1]; // Take last part after /
  filenameNoExtension = filenameNoExtension.split("\\");
  filenameNoExtension = filenameNoExtension[filenameNoExtension.length - 1]; // Take last part after \
  filenameNoExtension = filenameNoExtension.split(".");
  filenameNoExtension = filenameNoExtension[0]; // Take front of .
  vtkRefs.filenameNoExtension = filenameNoExtension;
}

const viewerContainers = document.querySelectorAll('.vtkjs-volume-viewer');
let nbViewers = viewerContainers.length;
while (nbViewers--) {
  const viewerContainer = viewerContainers[nbViewers];
  const fileURL = viewerContainer.dataset.url;
  const options = Object.assign(
    { containerStyle: { height: '100%' } },
    userParams,
    { fileURL }
  );
  load(viewerContainer, options);
}

// Auto setup if no method get called within 100ms
setTimeout(() => {
  if (autoInit) {
    initLocalFileLoader();
  }
}, 100);
