/**
 * RiverDisasterInfo -- Provides information from http://www.river.go.jp in a SAGE friendly way
 *
 * Written by Matthew Ready based on the WebView app.
 *
 * This script is injected into a page with a map to provide the dashed line overlay.
 *
 * Example page:
 * http://www.river.go.jp/kawabou/ipGaikyoMap.do?areaCd=82&prefCd=0201&townCd=&gamenId=01-0704&fldCtlParty=no
 */
"use strict";

(function() {
    function getMapFrame() {
        return document.querySelector("#tele-main > iframe");
    }

    function getInnerMap() {
        var mapFrame = getMapFrame();
        return mapFrame == null ? null : mapFrame.contentDocument.getElementById("gisMapDiv")
    }

    function getScreenColour(screenIndexClockwiseFromLeft) {
        var hue = Math.floor(360 * screenIndexClockwiseFromLeft / 8);
        return "hsl("+hue+", 100%, 50%)";
    }

    function dist2(x1, y1, x2, y2) {
        var dx = x1 - x2;
        var dy = y1 - y2;
        return dx * dx + dy * dy;
    }

    function removePointsOutOfMap(pointList) {
        var inner = getInnerMap();
        if (inner == null) {
            return [];
        }
        var rect = inner.getBoundingClientRect();

        var newList = [];
        for (var i = 0; i < pointList.length; i++) {
            var ix = pointList[i][0];
            var iy = pointList[i][1];
            if (0 <= ix && ix < rect.width
                && 0 <= iy && iy < rect.height) {
                newList.push(pointList[i]);
            }
        }
        return newList;
    }

    /**
     * Reads the location of each sensor from the page.
     *
     * @returns Array[Array[x, y, radius, name, js_on_click]]
     */
    function getPoints() {
        if (!getMapFrame()) {
            return [];
        }

        var query = getMapFrame().contentDocument.getElementsByTagName("script");

        for (var i = 0; i < query.length; i++) {
            var pointText = query[i].innerText;
            var start = pointText.indexOf("var pointList");
            var end = pointText.indexOf("];", start);

            if (start >= end || start == -1 || end == -1) {
                // Wrong script tag!
                continue;
            }

            var points = pointText.substr(start + 4, end - start + 2);
            var pointList;
            eval(points);
            return pointList;
        }

        console.log("Could not locate points script");
        return [];
    }

    /**
     * Removes points that have the same location as another point in the list, returns a list where each point does not
     * share a location.
     *
     * @param pts Input points
     * @returns Array[Array[x, y, radius, name, js_on_click]]
     */
    function makeDiscretePoints(pts) {
        // Just use an object as a hash map to make things discrete quickly.
        var ptsDiscrete = {};
        for (var i = 0; i < pts.length; i++) {
            ptsDiscrete[pts[i][0] + " _ " + pts[i][1]] = pts[i];
        }

        // Load a list back from the object.
        var ptsNew = [];
        for (var key in ptsDiscrete) {
            if (ptsDiscrete.hasOwnProperty(key)) {
                ptsNew.push(ptsDiscrete[key]);
            }
        }

        // Return the points as a list.
        return ptsNew;
    }

    // Load the points from the page.
    var pts = removePointsOutOfMap(getPoints());
    var discretePoints = makeDiscretePoints(pts);

    // The screens variable lists the position of the surrounding 8 web pages in clockwise order from the left.
    var screens = [
        [0, 1],
        [0, 0],

        [1, 0],
        [2, 0],

        [2, 1],
        [2, 2],

        [1, 2],
        [0, 2]
    ];

    /**
     * Get the closest eight points to the given mouse coordinates, sorted by angle.
     * @returns Array[Array[x, y, radius, name, js_on_click]]
     */
    function getTopEight(mouseX, mouseY, left, top, right, bottom) {
        function compMouseDist(a, b) {
            var da = dist2(a[0], a[1], mouseX, mouseY);
            var db = dist2(b[0], b[1], mouseX, mouseY);
            return Math.sign(da - db);
        };

        function compX(a,b){
            return Math.sign(a[0] - b[0]);
        };

        function compY(a,b){
            return Math.sign(a[1] - b[1]);
        };

        // Sort the discretePoints list. Obviously we can do much better in terms of time complexity, but this works
        // for now.
        discretePoints.sort(compMouseDist);

        var topEight = discretePoints.slice(0, 8);

        // Sort by X.
        topEight.sort(compX);

        var left3 = topEight.slice(0, 3);
        var middle2 = topEight.slice(3, 5);
        var right3 = topEight.slice(5);

        left3.sort(compY);
        middle2.sort(compY);
        right3.sort(compY);

        return [left3[1], left3[0], middle2[0], right3[0], right3[1], right3[2], middle2[1], left3[2]];
        
        /*
        var avgX = topEight.reduce(function (a, b) {
                return a + b[0];
            },0) / topEight.length;

        var avgY = topEight.reduce(function (a, b) {
                return a + b[1];
            },0) / topEight.length;

        // Sort the top eight by angle around the centroid.
        return topEight.sort(function (a, b) {
            var dirA = Math.atan2(a[1] - avgY, a[0] - avgX);
            var dirB = Math.atan2(b[1] - avgY, b[0] - avgX);

            if (dirA < dirB) {
                return -1;
            } else if (dirA > dirB) {
                return 1;
            } else {
                return 0;
            }
        });*/
    }

    /**
     * Given the top eight points, provide info to the SAGE app so it can update its web pages.
     * @param mouseX
     * @param mouseY
     * @param topEight The points to display in the surrounding pages.
     */
    function logTopEight(mouseX, mouseY, topEight) {
        for (var i = 0; i < 8; i++) {
            console.log(JSON.stringify({
                "message": "map-screen",
                "x": screens[i][0],
                "y": screens[i][1],
                "point": topEight[i], // If this is out of bounds, undefined is OK.
                "color": getScreenColour(i)
            }));
        }

        console.log(JSON.stringify({
            "message": "state-update",
            "mouseX": mouseX,
            "mouseY": mouseY
        }));
    }

    var FRAME_WAIT = 50;

    /**
     * Redraw the map onto the given canvas.
     *
     * @param canvas
     * @param state
     */
    function redrawMap(canvas, state) {
        var mouseX = state.mouseX;
        var mouseY = state.mouseY;
        var topEight = state.topEight;

        if (mouseX === undefined || mouseY === undefined) {
            // Nothing to draw.
            return;
        }

        function getScreenPos(screen) {
            return [screen[0] * canvas.width / 2,
                    screen[1] * canvas.height / 2];
        }

        var lastPointPos = state.lastPointPos;
        if (lastPointPos === undefined) {
            lastPointPos = state.lastPointPos = screens.map(getScreenPos);
        }

        var mapFrame = getMapFrame();
        var innerMap = getInnerMap();

        var mapFrameBounds = mapFrame.getBoundingClientRect();
        var innerMapBounds = innerMap.getBoundingClientRect();
        var ofX = mapFrameBounds.left + innerMapBounds.left;
        var ofY = mapFrameBounds.top + innerMapBounds.top;

        var ctx = canvas.getContext("2d");

        // Clear the canvas.
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Draw the pin.
        ctx.fillStyle = "#FF0000";
        ctx.beginPath();
        ctx.arc(ofX + mouseX, ofY + mouseY, 10, 0, 2 * Math.PI);
        ctx.fill();

        var stillAnimating = false;

        // Draw the lines to the edges of the screen from the nearest points.
        for (var i = 0; i < 8; i++) {
            ctx.beginPath();

            var point = topEight[i];

            var globalPointX;
            var globalPointY;

            if (point === undefined) {
                var screenPos = getScreenPos(i);
                globalPointX = screenPos[0];
                globalPointY = screenPos[1];
            } else {
                globalPointX = ofX + point[0];
                globalPointY = ofY + point[1];
            }

            // Halve the distance each frame. Since the framrate is fixed I'm
            // not gonna bother with Math.pow to adjust this.
            lastPointPos[i][0] = (lastPointPos[i][0] + globalPointX) / 2;
            lastPointPos[i][1] = (lastPointPos[i][1] + globalPointY) / 2;

            if (Math.abs(lastPointPos[i][0] - globalPointX) > 1 ||
                Math.abs(lastPointPos[i][1] - globalPointY) > 1) {
                globalPointX = lastPointPos[i][0];
                globalPointY = lastPointPos[i][1];
                stillAnimating = true;
            } else {
                lastPointPos[i][0] = globalPointX;
                lastPointPos[i][1] = globalPointY;
            }


            var circleRadius = 10;
            ctx.arc(globalPointX, globalPointY, circleRadius, 0, 2 * Math.PI);

            function cap(valueIfMiddleScreen, screenPos, maxDimension) {
                return screenPos == 1 ? valueIfMiddleScreen : screenPos / 2 * maxDimension;
            }

            var lineTargetX = cap(globalPointX, screens[i][0], canvas.width);
            var lineTargetY = cap(globalPointY, screens[i][1], canvas.height);

            var deltaX = lineTargetX - globalPointX;
            var deltaY = lineTargetY - globalPointY;
            var deltaD = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
            deltaX *= circleRadius / deltaD;
            deltaY *= circleRadius / deltaD;

            ctx.moveTo(globalPointX + deltaX, globalPointY + deltaY);
            ctx.lineTo(lineTargetX, lineTargetY);

            ctx.lineCap = "round";
            ctx.lineWidth = 4;
            ctx.strokeStyle = getScreenColour(i);
            ctx.stroke();
        }

        if (stillAnimating
            && (state.timeout === undefined || state.timeout === null)) {
            state.timeout = setTimeout(function() {
                state.timeout = null;
                redrawMap(canvas, state);
            }, FRAME_WAIT);
        }

        console.log("Frame");
    }

    function attachCanvas() {
        var mapFrame = getMapFrame();
        if (!mapFrame) {
            console.log(JSON.stringify({
                "message": "invalid-page"
            }));
            return;
        }
        console.log("map frame ok");

        // Create the canvas and attach it.
        var canvas = document.createElement('canvas');
        canvas.id = 'overlayId';
        canvas.setAttribute("style", "z-index:1000; width:100%; height=100%; position: fixed; pointer-events:none; left:0; top:0;");
        document.body.appendChild(canvas);

        // Create a state object that is updated.
        var state = {
            "mouseX": undefined,
            "mouseY": undefined,
            "topEight": []
        };

        // When the window is resized, make sure the canvas is updated.
        window.onresize = function () {
            canvas.width = document.body.clientWidth;
            canvas.height = document.body.clientHeight;

            redrawMap(canvas, state);
        };

        // Update the canvas size at the start.
        window.onresize();

        // When the window is scrolled, update the canvas.
        window.onscroll = function () {
            redrawMap(canvas, state);
        };

        function onMouseUp(mouseX, mouseY) {
            // Update the state
            state.mouseX = mouseX;
            state.mouseY = mouseY;
            state.topEight = getTopEight(state.mouseX, state.mouseY);

            // Send the new details
            logTopEight(state.mouseX, state.mouseY, state.topEight);

            // Redraw the map
            redrawMap(canvas, state);
        }

        var innerMap = getInnerMap();
        var oldMM = innerMap.onmouseup;
        innerMap.onmouseup = function (e) {
            onMouseUp(e.clientX, e.clientY);

            // Call the parent function if its there.
            if (oldMM !== null) {
                return oldMM.call(this, e);
            }
        };

        if (window.mapOverlayState !== undefined
            && window.mapOverlayState.mouseX >= 0
            && window.mapOverlayState.mouseY >= 0) {
            onMouseUp(window.mapOverlayState.mouseX, window.mapOverlayState.mouseY);
        }
    }

    attachCanvas();
})();
