/**
 * RiverDisasterInfo -- Provides information from http://www.river.go.jp in a SAGE friendly way
 *
 * Written by Matthew Ready based on the WebView app.
 * Updated by Dylan Kobayashi <dylank@hawaii.edu> to interact with multiple apps.
 * Copyright (c) 2017
 */
"use strict";

var rdi = SAGE2_App.extend({
    init: function(data) {
        var _this = this;
        var i;

        // Create div into the DOM
        this.SAGE2Init("div", data);

        // Set the DOM id
        this.element.id = "div_" + data.id;

        if (!this.isElectron()) {
            // Create div into the DOM
            this.SAGE2Init("div", data);
            this.element.innerHTML = "<h1>Webview only supported using Electron as a display client</h1>";
            this.mainWebView = {};
            return;
        }





        this.moveEvents = "onfinish";
        this.maxFPS = 10;

        this.moveStart = {
            x: this.sage2_x,
            y: this.sage2_y
        };


        // subwindow and adjustent
        this.subInit = true;
        this.lastSubId = 1;
        this.linesToSub = [];
        this.currentSensorSelection = null;
        this.delayedSubWindowSensorDetectionCounter = 0;
        /*
		if (ui.width > 2000 && ui.height > 2400) {
            var posAdjust = {};
            posAdjust.appPositionAndSize = {};
            posAdjust.appPositionAndSize.elemId = this.id;
            posAdjust.appPositionAndSize.elemLeft = 1400;
            posAdjust.appPositionAndSize.elemTop = ui.height / 2 - this.sage2_height / 2;
            posAdjust.appPositionAndSize.elemHeight = this.sage2_height;
            posAdjust.appPositionAndSize.elemWidth = this.sage2_width;

            wsio.emit("updateApplicationPosition", posAdjust);
        }
		*/
		
		var posAdjust = {};
		posAdjust.appPositionAndSize = {};
		posAdjust.appPositionAndSize.elemId = this.id;
		posAdjust.appPositionAndSize.elemLeft = 1400;
		posAdjust.appPositionAndSize.elemTop = 666;
		posAdjust.appPositionAndSize.elemHeight = this.sage2_height;
		posAdjust.appPositionAndSize.elemWidth = this.sage2_width;

		wsio.emit("updateApplicationPosition", posAdjust);
		

        // timeout because of the above positioning action
        setTimeout(() => {
            this.subLaunch();
            this.setupBroadcastVariables();
        }, 1000);






        this.element.style.backgroundColor = "#FF0000";

        // Make 9 web views.
        var innerHTML = "";
        for (i = 0; i < 9; i++) {
            innerHTML += "<webview></webview>"
        }

        // Make 9 dots.
        /*for (i = 0; i < 9; i++) {
            innerHTML += "<div style=\"width: 100px; height: 100px; background: red; -moz-border-radius: 50px; -webkit-border-radius: 50px; border-radius: 50px;\"></div>";
        }*/

        this.element.innerHTML = innerHTML;

        // Initialize subWindows and the array that contains the page we last set them to.
        this.subWindows = []; // Array full of webviews
        this.subWindowsSrcs = []; // Array full of URL strings for the page in each webview. Distinct from webview.src.
        for (var y = 0; y < 3; y++) {
            this.subWindows.push([]);
            this.subWindowsSrcs.push([]);
            for (var x = 0; x < 3; x++) {
                // Get the webview.
                var win = this.element.children[y*3+x];

                // Add it.
                this.subWindows[y].push(win);
                this.subWindowsSrcs[y].push("");

                // Set the background to white, and set the width and height to a temporary default value.
                win.style.backgroundColor = 'white';
                win.style.position = 'absolute';
                win.style.display = "inline-flex";

                // Webview settings
                win.autosize  = "on";
                win.plugins   = "on";
                win.allowpopups = false;
                win.allowfullscreen = false;
                win.nodeintegration = 0;

                win.minwidth  = data.width / 3;
                win.minheight = data.height / 3;

                if (x != 1 && y != 1) {
                    (function(win) {
                        win.addEventListener('console-message', function() {
                            console.log("WebView data console message:");
                            console.log(event.message);
                        });
                        
                        win.addEventListener("did-finish-load", function () {
                            _this.updateZoomFactor(win);
                            win.executeJavaScript(_this.injectCodeData);
                        });
                    })(win);
                }

                (function(win) {
                    win.addEventListener("page-title-updated", function () {
                        _this.forceLayout(win); // Hack: Electron does not seem to be updating the webview size correctly when page is zoomed in. Force layout to ensure they're correct.
                    });
                        
                    win.addEventListener("did-finish-load", function () {
                        _this.updateZoomFactor(win);
                        _this.forceLayout(win); // Hack: Electron does not seem to be updating the webview size correctly when page is zoomed in. Force layout to ensure they're correct.
                    });
                })(win);
            }
        }

        this.updateSubwindowSizes();

        // The main web view that the user can control is the subWindow in the center.
        this.mainWebView = this.subWindows[1][1];

        // Get the URL from parameter or session
        this.mainWebView.src = data.params || this.state.url;

        // move and resize callbacks
        this.resizeEvents = "continuous";

        // Auto-refresh time
        this.autoRefresh = null;

        var srcPath = this.resrcPath;

        // Download the code to inject. I'm going to just use a synchronous request here for convenience.
        var request = new XMLHttpRequest();
        request.open('GET', srcPath + '/InjectMap.js', false);
        request.send(null);
        this.injectCodeMap = request.responseText;

        request = new XMLHttpRequest();
        request.open('GET', srcPath + '/InjectData.js', false);
        request.send(null);
        this.injectCodeData = request.responseText;

        // done loading
        this.mainWebView.addEventListener("did-finish-load", function() {
            // save the url
            _this.state.url = _this.mainWebView.src;
            // set the zoom value
            _this.updateZoomFactor(_this.mainWebView);

            // Reset the current map overlay if we've changed the page.
            if (_this.state.mapOverlayStateUrl != _this.state.url) {
                _this.state.mapOverlayStateMouseX = -1;
                _this.state.mapOverlayStateMouseY = -1;
                _this.state.mapOverlayStateUrl = "";

                _this.currentSensorSelection = null;
            }

            // sync the state object
            _this.SAGE2Sync(false);

            // Inject the code that adds the map overlay and helps with webview interop.
            _this.codeInject();

            _this.varBroadcastAfterMainViewLoad();
        });

        this.mainWebView.addEventListener("did-fail-load", function(event) {
            // Something has gone terribly wrong!
            console.log("did-fail-load");
            console.log(JSON.stringify(event));
        });

        function make_real(url, gamen, observerId) {
            if (url == 'ipSuiiKobetuMini.do' || url == '/kawabou/ipSuiiKobetu.do') {
                return 'http://www.river.go.jp/kawabou/ipSuiiKobetu.do?obsrvId='+observerId+'&gamenId=01-1002&stgGrpKind=crsSect&fldCtlParty=no&fvrt=yes&timeType=10';
            } else if (url == '/kawabou/ipRainKobetu.do') {
                return 'http://www.river.go.jp/kawabou/ipRainKobetu.do?obsrvId='+observerId+'&gamenId=01-1001&fldCtlParty=no&fvrt=yes&timeType=10';
            } else if (url == '/kawabou/ipDamKobetu.do') {
                return 'http://www.river.go.jp/kawabou/ipDamKobetu.do?obsrvId='+observerId+'&gamenId=01-1004&fldCtlParty=no&fvrt=yes&timeType=10';
            } else if (url == '/kawabou/ipKaiganKobetu.do') {
                return 'http://www.river.go.jp/kawabou/ipKaiganKobetu.do?obsrvId='+observerId+'&gamenId=01-1005&fldCtlParty=no&fvrt=yes&timeType=10';
            } else if (url == '/kawabou/ipSuisituKobetuDtl.do') {
                return 'http://www.river.go.jp/kawabou/ipSuisituKobetuDtl.do?init=init&obsrvId='+observerId+'&gamenId=01-1006&timeType=60&requestType=1&fldCtlParty=no';
            } else if (url == '/kawabou/ipSnowKobetu.do') {
                return 'http://www.river.go.jp/kawabou/ipSnowKobetu.do?init=init&obsrvId='+observerId+'&gamenId=01-1007&timeType=60&requestType=1&fldCtlParty=no';
            } else if (url == '/kawabou/ipCamera.do') {
                return 'http://www.river.go.jp/kawabou/ipCamera.do?gamenId=01-0710&cameraId='+observerId
            } else {
                return "";
            }
        }

        this.mainWebView.addEventListener('console-message', function(event) {
            if (event.message.indexOf("map-screen") >= 0) {
                var screenDetails = JSON.parse(event.message);
                var point = screenDetails.point;
                var visitUrl = "about:blank";

                if (point !== undefined) {
                    var jsToRun = point[4];
                    var rematch = /onclickIconPopup\('([^']+)', '([^']+)', '([^']+)'\)/.exec(jsToRun);
                    if (rematch !== null) {
                        visitUrl = make_real(rematch[1], rematch[2], rematch[3]);
                    }

                    rematch = /onclickIconThumbnail\('([^']+)', '([^']+)'\)/.exec(jsToRun);
                    if (rematch !== null) {
                        visitUrl = make_real(rematch[1], '01-1002', rematch[2]);
                    }

                    rematch = /onclickIconCamera\('([^']+)', (?:false|true)\)/.exec(jsToRun);
                    if (rematch !== null) {
                        visitUrl = make_real('/kawabou/ipCamera.do', '01-0710', rematch[1]);
                    }
                }

                if (_this.subWindowsSrcs[screenDetails.y][screenDetails.x] != visitUrl) {
                    console.log("subwindow src updated from:" + _this.subWindowsSrcs[screenDetails.y][screenDetails.x]);
                    console.log("--to:" + visitUrl);
                    _this.subWindowsSrcs[screenDetails.y][screenDetails.x] = visitUrl;
                    var window = _this.subWindows[screenDetails.y][screenDetails.x];
                    window.src = visitUrl;
                    window.style.outline = "5px solid " + screenDetails.color;
                    // child window is 0-8
                    var y = screenDetails.y;
                    var x = screenDetails.x;
                    var wi = y * 3 + x;
                    if ((y == 2) || (y == 1 && x > 0)) {
                        wi--; // because 1,1 is the primary
                    }
                    _this.updateSubUrls(wi, visitUrl);
                }
            } else if (event.message.indexOf("state-update") >= 0) {
                var stateUpdate = JSON.parse(event.message);

                _this.state.mapOverlayStateMouseX = stateUpdate.mouseX;
                _this.state.mapOverlayStateMouseY = stateUpdate.mouseY;
                _this.state.mapOverlayStateUrl = _this.state.url;

                _this.SAGE2Sync(false);
            } else if (event.message.indexOf("invalid-page") >= 0) {
                for (var y = 0; y < 3; y++) {
                    for (var x = 0; x < 3; x++) {
                        if (y == 1 && x == 1) {
                            continue;
                        }
                        _this.subWindows[y][x].src = "about:blank";
                        _this.subWindowsSrcs[y][x] = "about:blank";
                    }
                }
                console.log("Cleared windows");
            } else {
                console.log("WebView console message:");
                console.log(event.message);
            }
        });

        // When the webview tries to open a new window
        this.mainWebView.addEventListener("new-window", function(event) {
            // only accept http protocols
            if (event.url.startsWith('http:') || event.url.startsWith('https:')) {
                _this.changeURL(event.url, false);
            } else {
                console.log('Webview>    Not http URL, not opening', event.url);
            }
        });

    },

    /**
     * Determines if electron is the renderer (instead of a browser)
     *
     * @method     isElectron
     * @return     {Boolean}  True if electron, False otherwise.
     */
    isElectron: function() {
        return (typeof window !== 'undefined' && window.process && window.process.type === "renderer");
    },

    load: function(date) {
        // sync the change
        this.mainWebView.src = this.state.url;
        this.refresh(date);
    },

    draw: function(date) {
        if (this.childrenAppIds.length < 8) {
            return;
        }
        try {
        this.updateLinesToSub();
        } catch (e) {}
    },

    changeURL: function(newlocation, remoteSync) {
        // trigger the change
        this.mainWebView.src = newlocation;
        // save the url
        this.state.url   = newlocation;
        this.SAGE2Sync(remoteSync);
    },

    forceLayout: function(element) {
        // HACK: Attempt to force relayout. Electron does not seem to be updating the
        // webviews correctly when the page is zoomed in. Not sure why, don't have time 
        // to check.
        var oldWidth = element.style.width;
        element.style.width = "0px";
        element.getBoundingClientRect(); // This forces the layout.
        element.style.width = oldWidth;
        
        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                if (this.subWindows[i][j] == element) {
                    // console.log("Forced layout of [" + i + "," + j + "]");
                }
            }
        }
    },

    updateSubwindowSizes: function() {
        var columnPos = [0, 0, 1, 1];
        var rowPos = [0, 0, 1, 1];

        for (var y=0; y<3; y++) {
            for (var x=0; x<3; x++) {
                var pageOutlineWidth;
                if (y == 1 && x == 1) {
                    pageOutlineWidth = 0;
                } else {
                    pageOutlineWidth = 5;
                }

                var left = Math.round(columnPos[x] * this.sage2_width);
                var right = Math.round(columnPos[x+1] * this.sage2_width);
                var top = Math.round(rowPos[y] * this.sage2_height);
                var bottom = Math.round(rowPos[y+1] * this.sage2_height);

                var win = this.subWindows[y][x];
                win.style.left = (left + pageOutlineWidth) + "px";
                win.style.top = (top + pageOutlineWidth) + "px";
                win.style.width = ((right - left) - pageOutlineWidth * 2) + "px";
                win.style.height = ((bottom - top) - pageOutlineWidth * 2) + "px";
                win.style.position = 'absolute';
            }
        }
    },

    resize: function(date) {
        // Called when window is resized
        this.updateSubwindowSizes();
        this.updateZoomFactor();
        this.refresh(date);
    },

    quit: function() {
        for (let i = 0; i < 8; i++) {
            this.linesToSub[i].remove();
        }
        if (this.autoRefresh) {
            // cancel the autoreload timer
            clearInterval(this.autoRefresh);
        }
    },

    /**
    Initial testing reveals:
        the page is for most intents and purposes fully visible.
            the exception is if there is a scroll bar.
        javascript operates in the given browser.
        different displays will still have the same coordinate system
            exception: random content can alter coordinate locations

        sendInputEvent
            accelerator events have names http://electron.atom.io/docs/api/accelerator/
            SAGE2 buttons can't pass symbols


    Things to look out for:
        Most errors are silent
            might be possible to use console-message event: http://electron.atom.io/docs/api/web-view-tag/#event-console-message
        alert effects still produce another window on display host
            AND pause the page

    */
    codeInject: function() {
        /* eslint-disable */
        this.mainWebView.executeJavaScript(
            '\
            var s2InjectForKeys = {};\
            \
            document.addEventListener("click", function(e) {\
                s2InjectForKeys.lastClickedElement = document.elementFromPoint(e.clientX, e.clientY);\
            });\
            \
            document.addEventListener("keydown", function(e) {\
                /* Shift */\
                if (e.keyCode == 16) {\
                    s2InjectForKeys.shift = true;\
                    return;\
                }\
                /* Backspace */\
                if (e.keyCode == 8) {\
                    s2InjectForKeys.lastClickedElement.value = s2InjectForKeys.lastClickedElement.value.substring(0, s2InjectForKeys.lastClickedElement.value.length - 1);\
                    return;\
                }\
                /* Dont set keypress value if there was no clicked div */\
                if (s2InjectForKeys.lastClickedElement.value == undefined) {\
                    return; \
                }\
                /* By default, characters are capitalized, if shift is not down, lower case them. */\
                var sendChar = String.fromCharCode(e.keyCode);\
                if (!s2InjectForKeys.shift) {\
                    sendChar = sendChar.toLowerCase();\
                } else if(e.keyCode == 49) { /* 1 */\
                    sendChar =  "!";\
                } else if(e.keyCode == 50) { /* 2 */\
                    sendChar =  "@";\
                } else if(e.keyCode == 51) { /* 3 */\
                    sendChar =  "#";\
                } else if(e.keyCode == 52) { /* 4 */\
                    sendChar =  "$";\
                } else if(e.keyCode == 53) { /* 5 */\
                    sendChar =  "%";\
                } else if(e.keyCode == 54) { /* 6 */\
                    sendChar =  "^";\
                } else if(e.keyCode == 55) { /* 7 */\
                    sendChar =  "&";\
                } else if(e.keyCode == 56) { /* 8 */\
                    sendChar =  "*";\
                } else if(e.keyCode == 57) { /* 9 */\
                    sendChar =  "(";\
                } else if(e.keyCode == 48) { /* 0 */\
                    sendChar =  ")";\
                }\
                s2InjectForKeys.lastClickedElement.value += sendChar;\
            });\
            document.addEventListener("keyup", function(e) {\
                if (e.keyCode == 0x10) {\
                    s2InjectForKeys.shift = false;\
                }\
                if (e.keyCode == 8) {\
                    s2InjectForKeys.lastClickedElement.value = s2InjectForKeys.lastClickedElement.value.substring(0, s2InjectForKeys.lastClickedElement.value.length - 1);\
                }\
            });\
            '
        );
        var script = "window.mapOverlayState = " + JSON.stringify({
                "mouseX": this.state.mapOverlayStateMouseX,
                "mouseY": this.state.mapOverlayStateMouseY
            }) + ";";
        this.mainWebView.executeJavaScript(script);
        this.mainWebView.executeJavaScript(this.injectCodeMap);
        /* eslint-enable */
    },

    getContextEntries: function() {
        var entries = [];
        var entry;

        entry = {}; // this div display none is to add more words without being visible
        entry.description = "<div style='display:none'> display plot history past values satellite view map me </div> "
            + "show sensor";
        entry.callback = "showSomething";
		entry.inputField     = true;
		entry.inputFieldSize = 3;
        entry.parameters = {};
        entries.push(entry);

        // entry = {};
        // entry.description = "<div style='display:none'> satellite view map me </div>" // trying to stack more values
        //     + "show sensor:";
        // entry.callback = "showSensorOnMap";
		// entry.inputField     = true;
		// entry.inputFieldSize = 3;
        // entry.parameters = {};
        // entries.push(entry);

        entry = {};
        entry.description = "show weather of the map location search";
        entry.callback = "showMapWeatherOfSearch";
		entry.inputField     = true;
		entry.inputFieldSize = 3;
        entry.parameters = {};
        entries.push(entry);

        entry = {};
        entry.description = "<div style='display:none'> flag mark problem issue review later </div>" // trying to stack more values
            + " flag sensor:";
        entry.callback = "flagSensorOnMap";
		entry.inputField     = true;
		entry.inputFieldSize = 3;
        entry.parameters = {};
        entries.push(entry);

        entries.push({description: "separator"});

        entry = {};
        entry.description = "Focus_map_on_sensor:";
        entry.callback = "showSensorOnMap";
		entry.inputField     = true;
		entry.inputFieldSize = 3;
        entry.parameters = {};
        entries.push(entry);

        entry = {};
        entry.description = "Open_sensor_data:";
        entry.callback = "showHistoricData";
		entry.inputField     = true;
		entry.inputFieldSize = 3;
        entry.parameters = {};
        entries.push(entry);

        entry = {};
        entry.description = "Open_all_sensor_data:";
        entry.callback = "showAllHistoricData";
        entry.parameters = {};
        entries.push(entry);

        entry = {};
        entry.description = "Open_linked_weather_search_window:";
        entry.callback = "showMapWeatherOfSearch";
        entry.parameters = {};
        entries.push(entry);

        entries.push({description: "separator"});

		entries.push({
			description: "Copy URL to clipboard",
			callback: "SAGE2_copyURL",
			parameters: {
				url: this.state.url
			}
		});

        return entries;
    },

    /**
     * Reload the content of the webview
     *
     * @method     reloadPage
     * @param      {Object}  responseObject  if time parameter passed, used as a timer
     */
    reloadPage: function(responseObject) {
        if (this.isElectron()) {
            if (responseObject.time) {
                // if an argument passed, use it for timer
                if (isMaster) {
                    // Parse the value we got
                    var interval = parseInt(responseObject.time, 10) * 1000;
                    var _this = this;
                    // build the timer
                    this.autoRefresh = setInterval(function() {
                        // send the message to the server to relay
                        _this.broadcast("reloadPage", {});
                    }, interval);
                }
            } else {
                // Just reload once
                this.mainWebView.reload();
                this.updateZoomFactor();
            }
        }
    },

    navigation: function(responseObject) {
        if (this.isElectron) {
            var action = responseObject.action;
            if (action === "back") {
                this.mainWebView.goBack();
            } else if (action === "forward") {
                this.mainWebView.goForward();
            } else if (action === "address") {
                if ((responseObject.clientInput.indexOf("://") === -1) &&
                    !responseObject.clientInput.startsWith("/")) {
                    responseObject.clientInput = "http://" + responseObject.clientInput;
                }
                this.changeURL(responseObject.clientInput, true);
            } else if (action === "search") {
                this.changeURL('https://www.google.com/#q=' + responseObject.clientInput, true);
            }
        }
    },

    updateZoomFactor: function(particularWindow) {
        if (particularWindow === undefined) {
            for (var i = 0; i < 3; i++) {
                for (var j = 0; j < 3; j++) {
                    this.updateZoomFactor(this.subWindows[i][j]);
                }
            }
            return;
        }

        var zoom = this.state.zoom;
        if (zoom == -1) {
            // Auto mode.
            var approxXZoom = this.sage2_width / 3448;
            var approxYZoom = this.sage2_height / 2465;
            zoom = Math.max(0.2, Math.min(approxXZoom, approxYZoom));
            // 3448, 2465
            // zoom = 1;
            console.log("Auto zoom. W=" + this.sage2_width + " H=" + this.sage2_height);
        }

        if (particularWindow == this.subWindows[1][1]) {
            // zoom *= 1.5;
            zoom *= 3;
            particularWindow.setZoomFactor(zoom);
        }

        for (var i = 0; i < 3; i++) {
            for (var j = 0; j < 3; j++) {
                if (this.subWindows[i][j] == particularWindow) {
                    // console.log("Auto zooming [" + i + "," + j + "] to " + zoom);
                }
            }
        }
        // particularWindow.setZoomFactor(zoom);
    },

    zoomPage: function(responseObject) {
        if (this.isElectron()) {
            var dir = responseObject.dir;

            // zoomin
            if (dir === "zoomin") {
                this.state.zoom *= 1.1;
            }

            // zoomout
            if (dir === "zoomout") {
                this.state.zoom /= 1.1;
            }

            this.updateZoomFactor();
            this.refresh();
        }
    },

    event: function(eventType, position, user_id, data, date) {
        if (this.isElectron()) {
            var globalRect = this.element.getBoundingClientRect();
            var globalX = position.x + globalRect.left;
            var globalY = position.y + globalRect.top;

            var targetElement = null;
            for (var i = 0; i < 3; i++) {
                for (var j = 0; j < 3; j++) {
                    var thisElement = this.subWindows[i][j];
                    var subRect = { left: thisElement.offsetLeft, top: thisElement.offsetTop, right: thisElement.offsetLeft + thisElement.offsetWidth, bottom: thisElement.offsetTop + thisElement.offsetHeight } // thisElement.getBoundingClientRect();
                    if (subRect.left <= position.x && position.x < subRect.right
                        && subRect.top <= position.y && position.y < subRect.bottom) {
                        targetElement = thisElement;
                        break;
                    }
                }
                if (targetElement != null) {
                    break;
                }
            }

            if (targetElement == null) {
                return;
            }
            // targetElement = this.subWindows[i][j];

            // Making Integer values, seems to be required by sendInputEvent
            // var x = Math.floor(position.x - targetElement.offsetLeft);
            // var y = Math.floor(position.y - targetElement.offsetTop);
            targetElement = this.mainWebView;
            var x = position.x; // Math.floor(position.x - targetElement.offsetLeft);
            var y = position.y; // Math.floor(position.y - targetElement.offsetTop);

            /*var targetX = Math.floor(position.x*3/this.sage2_width);
            var targetY = Math.floor(position.y*3/this.sage2_height);
            if (targetX < 0) {
                targetX = 0;
            }
            if (targetX > 2) {
                targetX = 2;
            }
            if (targetY < 0) {
                targetY = 0;
            }
            if (targetY > 2) {
                targetY = 2;
            }

            var targetElement = this.subWindows[targetY][targetX];*/

            if (eventType === "pointerPress" && (data.button === "left")) {
                // click
                targetElement.sendInputEvent({
                    type: "mouseDown",
                    x: x, y: y,
                    button: "left",
                    clickCount: 1
                });
            } else if (eventType === "pointerMove") {
                // move
                targetElement.sendInputEvent({
                    type: "mouseMove", x: x, y: y
                });
            } else if (eventType === "pointerRelease" && (data.button === "left")) {
                // click release
                targetElement.sendInputEvent({
                    type: "mouseUp",
                    x: x, y: y,
                    button: "left",
                    clickCount: 1
                });
            } else if (eventType === "pointerScroll") {
                // Scroll events: reverse the amount to get correct direction
                targetElement.sendInputEvent({
                    type: "mouseWheel",
                    deltaX: 0, deltaY: -1 * data.wheelDelta,
                    x: 0, y: 0,
                    canScroll: true
                });
            } else if (eventType === "widgetEvent") {
                // widget events
            } else if (eventType === "keyboard") {
                targetElement.sendInputEvent({
                    // type: "keyDown",
                    // Not sure why we need 'char' but it works ! -- Luc
                    type: "char",
                    keyCode: data.character
                });
                setTimeout(function() {
                    targetElement.sendInputEvent({
                        type: "keyUp",
                        keyCode: data.character
                    });
                }, 0);
            } else if (eventType === "specialKey") {
                // SHIFT key
                if (data.code === 16) {
                    if (data.state === "down") {
                        targetElement.sendInputEvent({
                            type: "keyDown",
                            keyCode: "Shift"
                        });
                    } else {
                        targetElement.sendInputEvent({
                            type: "keyUp",
                            keyCode: "Shift"
                        });
                    }
                }
                // backspace key
                if (data.code === 8 || data.code === 46) {
                    if (data.state === "down") {
                        // The delete is too quick potentially.
                        // Currently only allow on keyup have finer control
                    } else {
                        targetElement.sendInputEvent({
                            type: "keyUp",
                            keyCode: "Backspace"
                        });
                    }
                }

                if (data.code === 37 && data.state === "down") {
                    // arrow left
                    if (data.status.ALT) {
                        // navigate back
                        targetElement.goBack();
                    }
                    this.refresh(date);
                } else if (data.code === 38 && data.state === "down") {
                    // arrow up
                    if (data.status.ALT) {
                        // ALT-up_arrow zooms in
                        this.zoomPage({dir: "zoomin"});
                    } else {
                        targetElement.sendInputEvent({
                            type: "mouseWheel",
                            deltaX: 0, deltaY: 64,
                            x: 0, y: 0,
                            canScroll: true
                        });
                    }
                    this.refresh(date);
                } else if (data.code === 39 && data.state === "down") {
                    // arrow right
                    if (data.status.ALT) {
                        // navigate forward
                        targetElement.goForward();
                    }
                    this.refresh(date);
                } else if (data.code === 40 && data.state === "down") {
                    // arrow down
                    if (data.status.ALT) {
                        // ALT-down_arrow zooms out
                        this.zoomPage({dir: "zoomout"});
                    } else {
                        targetElement.sendInputEvent({
                            type: "mouseWheel",
                            deltaX: 0, deltaY: -64,
                            x: 0, y: 0,
                            canScroll: true
                        });
                    }
                    this.refresh(date);
                }
            }
        }
    },


// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// for sub window updating


startMove: function() {
    this.startMove.x = this.sage2_x;
    this.startMove.y = this.sage2_y;
},


move: function() {
    var dx = this.sage2_x - this.startMove.x;
    var dy = this.sage2_y - this.startMove.y;

    console.log("Move finished: " + dx + "," + dy);

    var dts = {};
    dts.appPositionAndSize = {};
    // only the webviews.
    for (let i = 0; i < 8; i++) {
    // for (let i = 0; i < this.childrenAppIds.length; i++) {
        dts.appPositionAndSize.elemId = this.childrenAppIds[i];
        dts.appPositionAndSize.elemLeft = applications[this.childrenAppIds[i]].sage2_x + dx;
        dts.appPositionAndSize.elemTop = applications[this.childrenAppIds[i]].sage2_y + dy - ui.titleBarHeight;
        dts.appPositionAndSize.elemWidth = applications[this.childrenAppIds[i]].sage2_width;
        dts.appPositionAndSize.elemHeight = applications[this.childrenAppIds[i]].sage2_height;

        wsio.emit("updateApplicationPosition", dts);
    }
},


updateSubUrls: function(subIndex, url) {
    var appRef = applications[this.childrenAppIds[subIndex]];
    var _refForLater;

    console.log("Given index:" + subIndex + " this matches agains:" + appRef);

    if (!appRef.rdiAutomaticTitleChangeEnabled) {
        appRef.rdiAutomaticTitleChangeEnabled = subIndex + 1;
        _refForLater = appRef;
        appRef.element.addEventListener("did-finish-load", function() {
            // save the url
            console.log("title mod for " + _refForLater.id + " has:" + _refForLater.hasHistoricDataFromRdi);
            setTimeout( function() {
                //if (applications[_refForLater.id].src.includes("~dylank/blank/")) {
                //    _refForLater.updateTitle("Sensor not selected");
                //} else 
				if (_refForLater.hasHistoricDataFromRdi) {
                    _refForLater.updateTitle('Sensor ' + _refForLater.rdiAutomaticTitleChangeEnabled
                        + " - Has historic data");
                } else {
                    _refForLater.updateTitle('Sensor ' + _refForLater.rdiAutomaticTitleChangeEnabled
                        + " - No historic data detected");
                }
            } , 1000);
		});
    }

    appRef.navigation({
        action: "address",
        clientInput: url
    });

    // also update the markers, which is now just the sensors selected.
    if (this.delayedSubWindowSensorDetectionCounter <= 0) {
        this.delayedSubWindowSensorDetectionCounter = 2;
        this.delayedSubWindowSensorDetection();
    }
},


subLaunch: function () {
    var _this = this;
    if (this.childrenAppIds.length < 8) {
        this.makeLine(this.lastSubId);
        if (this.lastSubId === 1) {
            this.launchAppWithValues("Webview",
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x - 1300, this.sage2_y - 750,
                "navigation");
            this.lastSubId++;
        } else if (this.lastSubId === 2) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x, this.sage2_y - 750,
                "navigation");
            this.lastSubId++;
        }  else if (this.lastSubId === 3) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x + this.sage2_width, this.sage2_y - 750,
                "navigation");
            this.lastSubId++;
        }  else if (this.lastSubId === 4) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x - 1300, this.sage2_y + 30,
                "navigation");
            this.lastSubId++;
        }  else if (this.lastSubId === 5) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x + this.sage2_width, this.sage2_y  + 30,
                "navigation");
            this.lastSubId++;
        }  else if (this.lastSubId === 6) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x - 1300, this.sage2_y + this.sage2_height - 100,
                "navigation");
            this.lastSubId++;
        }  else if (this.lastSubId === 7) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x, this.sage2_y + this.sage2_height - 100,
                "navigation");
            this.lastSubId++;
        }  else if (this.lastSubId === 8) {
            this.launchAppWithValues("Webview", 
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" }, // blank page
                this.sage2_x  + this.sage2_width, this.sage2_y + this.sage2_height - 100,
                "navigation");
            this.lastSubId++;
        } 


        setTimeout(function() {
            _this.subLaunch();
        }, 300); // 3 seconds to launch?
    }
},

makeLine: function(subId) {
    var colorMod = 0;


    var line = svgBackgroundForWidgetConnectors.line(0,0,0,0);
    this.linesToSub.push(line);
    var color = [
        "white",
        "#daa520",
        "#90ee90",
        "green",
        "red",
        "teal",
        "pink",
        "purple",
        "blue"
    ]

    line.attr({
        id: this.id + "lfs" + subId,
        strokeWidth: ui.widgetControlSize * 0.18,
        stroke: color[subId]
    });
},

updateLinesToSub: function() {
    var sid = 0;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x ),
            y1: (this.sage2_y ),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 1;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x + this.sage2_width / 2),
            y1: (this.sage2_y ),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 2;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x + this.sage2_width),
            y1: (this.sage2_y ),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 3;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x),
            y1: (this.sage2_y + this.sage2_height / 2),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 4;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x + this.sage2_width),
            y1: (this.sage2_y + this.sage2_height / 2),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 5;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x ),
            y1: (this.sage2_y + this.sage2_height),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 6;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x + this.sage2_width / 2),
            y1: (this.sage2_y + this.sage2_height),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
    sid = 7;
        this.linesToSub[sid].attr({
            x1: (this.sage2_x + this.sage2_width),
            y1: (this.sage2_y + this.sage2_height),
            x2: (applications[this.childrenAppIds[sid]].sage2_x + applications[this.childrenAppIds[sid]].sage2_width / 2),
            y2:(applications[this.childrenAppIds[sid]].sage2_y + applications[this.childrenAppIds[sid]].sage2_height / 2)
        });
},



// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// for handling gmap information

/*
Load resource files necessary.
Then broadcast variables to server regarding data source/ destination
*/
setupBroadcastVariables: function() {
    this.refFileTotal = 8; // town, prefecture, + 6 sensor files
    this.refFileLoadCount = 0;
    if (window.refTownGpsBounds !== undefined) {
        this.refTownGpsBounds = window.refTownGpsBounds;
        this.refPrefectures = window.refPrefectures;
        this.refSensorDam = window.refSensorDam;
        this.refSensorQuality = window.refSensorQuality;
        this.refSensorRain = window.refSensorRain;
        this.refSensorShore = window.refSensorShore;
        this.refSensorSnow = window.refSensorSnow;
        this.refSensorWater = window.refSensorWater;
        this.refFileLoadCount = this.refFileTotal = 8;
        this.broadcastDataAvailable();
        return;
    }
    // Download the reference files
	d3.json(this.resrcPath + '/dataFiles/townGpsBoundSensor.json', (error, jsObject) => {
        // arrow function retains "this" reference.
		(error) ? console.log(error) : "";
        this.refTownGpsBounds = jsObject;
        window.refTownGpsBounds = jsObject;
        console.log("Collected townGpsBounds entry count:" + Object.keys(this.refTownGpsBounds).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable(); // not actually activated until all files are loaded
	});
	d3.json(this.resrcPath + '/dataFiles/prefectures.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refPrefectures = jsObject;
        window.refPrefectures = jsObject;
        console.log("Collected Prefectures entry count:" + Object.keys(this.refPrefectures).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
    });
    // sensor data loading 6 files: dam, quality, rain, shore, snow, water
	d3.json(this.resrcPath + '/dataFiles/dam.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refSensorDam = jsObject;
        window.refSensorDam = jsObject;
        console.log("sensor data dam:" + Object.keys(this.refSensorDam).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
	});
	d3.json(this.resrcPath + '/dataFiles/quality.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refSensorQuality = jsObject;
        window.refSensorQuality = jsObject;
        console.log("sensor data Quality:" + Object.keys(this.refSensorQuality).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
	});
	d3.json(this.resrcPath + '/dataFiles/rain.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refSensorRain = jsObject;
        window.refSensorRain = jsObject;
        console.log("sensor data Rain:" + Object.keys(this.refSensorRain).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
	});
	d3.json(this.resrcPath + '/dataFiles/shore.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refSensorShore = jsObject;
        window.refSensorShore = jsObject;
        console.log("sensor data Shore:" + Object.keys(this.refSensorShore).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
	});
	d3.json(this.resrcPath + '/dataFiles/snow.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refSensorSnow = jsObject;
        window.refSensorSnow = jsObject;
        console.log("sensor data Snow:" + Object.keys(this.refSensorSnow).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
	});
	d3.json(this.resrcPath + '/dataFiles/water.json', (error, jsObject) => {
		(error) ? console.log(error) : "";
        this.refSensorWater = jsObject;
        window.refSensorWater = jsObject;
        console.log("sensor data Water:" + Object.keys(this.refSensorWater).length);
        this.refFileLoadCount++;
        this.broadcastDataAvailable();
	});
},

broadcastDataAvailable: function(){
    if (this.refFileLoadCount !== this.refFileTotal) {
        console.log("Debug: Delaying RDI var broadcast, not all files loaded");
        return;
    }
    /* can fall back to this if the full description fails
    // serverDataBroadcastSource: function(nameSuffix, value, description)
    this.serverDataBroadcastSource("currentGpsBounds", null, "Contains range of sensors");
    this.serverDataBroadcastSource("currentSensorList", null, "Contains list of sensors");

    // serverDataBroadcastDestination: function(nameSuffix, value, description, callback)
    this.serverDataBroadcastDestination("trySetPageToGps", null,
    "Based on given gps will attempt to adjust the page", "trySetPageToGps");
    */
    this.serverDataBroadcastSource("currentGpsBounds", // but actually being broadcast as a set
        null, {
            app: this.id,
            interpretAs: "set",
            dataTypes: ["gps"], // gives these datatypes, example: geojson can have more, but may only have points.
            dataFormat: "s2GeoLocation"
        }
    );
    this.serverDataBroadcastSource("currentSensorList", // but actually being broadcast as a set
        null, {
            app: this.id,
            interpretAs: "set",
            dataTypes: ["gps"], // gives these datatypes, example: geojson can have more, but may only have points.
            dataFormat: "s2GeoLocation"
        }
    );
    
    this.serverDataBroadcastDestination("trySetPageToGps", [], {
        app: this.id,
        interpretAs: "set",
        dataTypes: ["gps"], // wants these datatypes
        dataFormat: "s2GeoLocation" // should this also describe what is acceptable?
    }, "trySetPageToGps");
},

/*
Given value should be a gps coordinate. Might be in an array
*/
trySetPageToGps: function(gpsPoint) {
    if (this.surpressRedirect) {
        return; // discard while this.surpressRedirect is true;
    }
    if (Array.isArray(gpsPoint)) {
        gpsPoint = gpsPoint[0];
    }
    if (typeof gpsPoint !== "object") {
        console.log("Error, improper value given to RDI:" + JSON.stringify(gpsPoint));
        return;
    }
    this.lastGivenGpsPoint = gpsPoint;
    var _this = this;
    // if an update is not already queued
    if (!this.delayedGpsUpdateTimer) { // arrow function retains "this" ref
        this.delayedGpsUpdateTimer = true; // set status to true, then create one delayed update
        setTimeout(() => {
            this.delayedGpsToPreventOverload();
        }, 1000); // 1 second later
    }
},


delayedGpsToPreventOverload: function() {
    var gpsPoint = this.lastGivenGpsPoint;
    this.delayedGpsUpdateTimer = false;

    // attempt to determine which page to show based on gps containment.
    var townKeys = Object.keys(this.refTownGpsBounds);
    var closestTown, closestDistance = 9999, boundResult;
    // for each town
    for (let i = 0; i < townKeys.length; i++) {
        boundResult = this.isPointInGpsBound(gpsPoint, this.refTownGpsBounds[townKeys[i]]);
        // check if the point is within that town's gps bounds.
        if (true === boundResult) {
            closestTown = townKeys[i];
            break; // stop if the gps is contained
        } else if (boundResult < closestDistance){
            closestDistance = boundResult;
            closestTown = townKeys[i];
        }
    }
    
    // this is to prevent inf loop with maps.
    this.pageChangedByGpsGiven = true;

    // closest town should be filled out, be it containment or closest, following is example url
    // http://www.river.go.jp/kawabou/ipGaikyoMap.do?areaCd=82&prefCd=0501&townCd=1001201&gamenId=01-0701&fldCtlParty=no
    var urlToGoTo = "http://www.river.go.jp/kawabou/ipGaikyoMap.do?areaCd="
    + this.refPrefectures[this.refTownGpsBounds[closestTown].prefecture_code].area_code + "&prefCd="
    + this.refTownGpsBounds[closestTown].prefecture_code + "&townCd="
    + closestTown + "&gamenId=01-0701&fldCtlParty=no";

    if (urlToGoTo !== this.mainWebView.src) {
        for (let i = 0; i < 8; i++) { // 8 webview children.
            applications[this.childrenAppIds[i]].navigation(
                { action: "address", clientInput: "http://www2.hawaii.edu/~dylank/blank/" });
        }
    }

    this.changeURL(urlToGoTo, false);
},

isPointInGpsBound: function(point, bound) {
    var distanceFromBorder = 0;
    if (point.lat < bound.max.lat
    && point.lat > bound.min.lat
    && point.lng < bound.max.lng
    && point.lng > bound.min.lng) {
        return true;
    }
    // approximating the distance from border
    if (point.lat < bound.min.lat) {
        distanceFromBorder += bound.min.lat - point.lat;
    } else if (point.lat > bound.max.lat) {
        distanceFromBorder += point.lat - bound.max.lat;
    }
    if (point.lng < bound.min.lng) {
        distanceFromBorder += bound.min.lng - point.lng;
    } else if (point.lng > bound.max.lng) {
        distanceFromBorder += point.lng - bound.max.lng;
    }
    return distanceFromBorder;
},



varBroadcastAfterMainViewLoad: function() {
    if (this.refFileLoadCount !== this.refFileTotal) {
        setTimeout(() => {
            // should only happen on the first load
            this.varBroadcastAfterMainViewLoad();
        }, 500);
        return;
    }
    // perform gps containment lookup.
    if (this.mainWebView.src.includes("townCd")) {
        var town = this.determineTownFromUrl(this.mainWebView.src);
        var bounds = this.refTownGpsBounds[town];
        bounds = [
            {
                lat: bounds.max.lat,
                lng: bounds.max.lng,
            },
            {
                lat: bounds.min.lat,
                lng: bounds.min.lng,
            }
        ];
        // this check prevents potential infinite looping with gmaps
        if (true === this.pageChangedByGpsGiven) {
            this.pageChangedByGpsGiven = false;
        } else {
            // serverDataSetSourceValue: function(nameSuffix, value)
            this.serverDataSetSourceValue("currentGpsBounds", bounds);
        }
        // now send a list of all sensor coordinates
        var townSensorList = this.refTownGpsBounds[town].sensorList;
        // serverDataSetSourceValue: function(nameSuffix, value)
        this.serverDataSetSourceValue("currentSensorList", townSensorList);
		this.sendSensorListToGmap(townSensorList);
    } else {
        console.log("Unable to convert page to sensor coordinate containment");
    }
},

delayedSubWindowSensorDetection: function() {
    this.delayedSubWindowSensorDetectionCounter--;
    if (this.delayedSubWindowSensorDetectionCounter > 0) {
        setTimeout(() => {
            this.delayedSubWindowSensorDetection();
        }, 500); // counter starts at 2, so it will be a minimum of 1 second.
        return;
    }

    // getting to here means time to get the sensor cooridnates of the selection and display them on the map.
    // this.subWindowsSrcs, they will contain a url
    // it should have
    // beginning sensor type indicator like: ipSuiiKobetuMini
    // obsrvId=0128100400105, note this observer id, doesn't match with the auto id.

    if (this.mainWebView.src.includes("townCd")) {
        var town = this.determineTownFromUrl(this.mainWebView.src);
        var entry = this.refTownGpsBounds[town];
        var sensorsInTown = entry.sensorList; 
        var sensorType, obsrvId, gps;
        var dataToSend = [];

        console.log("-----------------------------------------------------------------");

        // which way are the windows organized?
        for (var y = 0; y < 3; y++) {
            for (var x = 0; x < 3; x++) {
                if (y == 1 && x == 1) {
                    continue;
                }
                if (this.subWindowsSrcs[y][x].includes("river.go.jp")) {
                    sensorType = this.determineSensorTypeFromUrl(this.subWindowsSrcs[y][x]);
                    console.log(sensorType);

                    // there was no camera data
                    if (!sensorType.includes("camera")) {
                        obsrvId = this.determineObserverIdFromUrl(this.subWindowsSrcs[y][x]);
                        obsrvId = +obsrvId;
                        gps = {};
                        for (let i = 0; i < sensorsInTown.length; i++) {
                            if (sensorsInTown[i].obsrvId === obsrvId) {
                                gps.lat = sensorsInTown[i].lat;
                                gps.lng = sensorsInTown[i].lng;
                                gps.obsAid = sensorsInTown[i].obsAid;
                                gps.obsrvId = sensorsInTown[i].obsrvId;
                                gps.wx = x;
                                gps.wy = y;
                                gps.type = sensorType;
                                dataToSend.push(gps);
                                break;
                            }
                        }
                    }
                }
            }
        }
        var dataSetToUse;
        if (dataToSend.length > 0) {
            this.serverDataSetSourceValue("currentSensorList", dataToSend);
			this.sendSensorListToGmap(dataToSend);
            
            this.currentSensorSelection = dataToSend;

            // first set all historic data trackers to false,
            for (let cid = 0; cid < 8; cid++){ // assumption that the windows do not close?
                applications[this.childrenAppIds[cid]].hasHistoricDataFromRdi = false; // modifier for title
            }

            // detect data for the sensor.
            // overloaded the data with info to detect sensors
            for (let i = 0; i < dataToSend.length; i++) {
                for (let cid = 0; cid < this.childrenAppIds.length; cid++){
                    if (applications[this.childrenAppIds[cid]].state.url
                        == this.subWindowsSrcs[dataToSend[i].wy][dataToSend[i].wx]) {
                        console.log("Subwindow " + applications[this.childrenAppIds[cid]]
                        + " line color " + this.linesToSub[cid].attr("stroke")
                        + " detected for sensor at " + dataToSend[i].lat + "," + dataToSend[i].lng
                        + " of type " + dataToSend[i].type);

                        if (dataToSend[i].type === "dam"){
                            dataSetToUse = this.refSensorDam;
                        } else if (dataToSend[i].type === "quality"){
                            dataSetToUse = this.refSensorQuality;
                        } else if (dataToSend[i].type === "rain"){
                            dataSetToUse = this.refSensorRain;
                        } else if (dataToSend[i].type === "shore"){
                            dataSetToUse = this.refSensorShore;
                        } else if (dataToSend[i].type === "snow"){
                            dataSetToUse = this.refSensorSnow;
                        } else if (dataToSend[i].type === "water"){
                            dataSetToUse = this.refSensorWater;
                        }
                        console.log("  The sensor auto id:" + dataToSend[i].obsAid);
                        if (dataSetToUse[dataToSend[i].obsAid]) {
                            console.log("  SUCCCESS: found entry in " + dataToSend[i].type + " sensor data");
                            // console.log("  value name:" + dataSetToUse.vName);
                            // console.log("  entries:" + dataSetToUse[dataToSend[i].obsAid].length);
                            applications[this.childrenAppIds[cid]].hasHistoricDataFromRdi = true;
                            console.log("  " + applications[this.childrenAppIds[cid]].id + " has historic data");
                        } else {
                            console.log("  NO RESULTS in " + dataToSend[i].type + " sensor data");
                            console.log("    " + applications[this.childrenAppIds[cid]].id);
                        }
                    }
                }
            }
        }
    } else {
        console.log("Unable to determien town from main webview could not get sensor information of subwindows");
    }

},

determineTownFromUrl: function(url) {
    if (!url.includes("townCd")) {
        return null;
    }
    var town = url; // parse out the ...&townCd=1001201&...
    town = town.substring(town.indexOf("townCd")); // townCd=1001201&...
    town = town.substring(town.indexOf("=") + 1); // 1001201&...
    if (town.indexOf("&") !== -1) {
        town = town.substring(0, town.indexOf("&"));
    }
    return town;
},

determineObserverIdFromUrl: function(url) {
    if (!url.includes("obsrvId")) {
        return null;
    }
    var id = url; // parse out the ...&obsrvId=1001201&...
    id = id.substring(id.indexOf("obsrvId")); // obsrvId=1001201&...
    id = id.substring(id.indexOf("=") + 1); // 1001201&...
    if (id.indexOf("&") !== -1) {
        id = id.substring(0, id.indexOf("&"));
    }
    return id;
},

determineSensorTypeFromUrl: function(url) {
    var type;
    if (url.includes("ipSuisituKobetuDtl")) {
        type = "quality";
    } else if (url.includes("ipRainKobetu")) {
        type = "rain";
    } else if (url.includes("ipDamKobetu")) {
        type = "dam";
    } else if (url.includes("ipSnowKobetu")) {
        type = "snow";
    } else if (url.includes("ipKaiganKobetu")) {
        type = "shore";
    } else if (url.includes("ipSuiiKobetu")) {
        type = "water";
    } else if (url.includes("ipCamera")) {
        type = "camera";
    } else {
        console.log("Error unable to get sensor type from url");
    }
    return type;
},

// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// launches plotyly data

showSomething: function(responseObject) {
    var phrase;
    var words;
    if (responseObject.clientVoiceInput) {
        phrase = responseObject.clientVoiceInput.toLowerCase();
        words = phrase.split(" ");
    } else {
        phrase = "show me sensor " + responseObject.clientInput.toLowerCase();
        words = phrase.split(" ");
    }

    // if sensor is in the phrase, probably want to see a sensor
    if (words.includes("sensor")) {
        if (words.includes("histor") || words.includes("data")) {
            this.showHistoricData(responseObject);
        } else {
            this.showSensorOnMap(responseObject);
        }
    } else if ((words.includes("go") && words.includes("to"))
        || words.includes("show") && words.includes("me") ){ // otherwise try show a location if "go to" or "show me"
        this.passLocationToMapSearch(responseObject);
    } else {
        this.makeSage2Message("Sorry unrecognized phrase:" + phrase);
    }
},

// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// launches plotyly data
showAllHistoricData: function(responseObject) {
    responseObject.clientInput = "show all historic sensor data";
    this.showHistoricData(responseObject);
},

showHistoricData: function(responseObject) {
    // need sensor selection to get current data
    if (!this.currentSensorSelection) {
        this.makeSage2Message("A current selection is needed to display historic data", 2000);
        return;
    }
    // show all sensors data if available.
    var allHistoryCheck;
    if (responseObject.clientVoiceInput) {
        allHistoryCheck = responseObject.clientVoiceInput.toLowerCase();
        if (allHistoryCheck.includes("all") && allHistoryCheck.includes("data") && allHistoryCheck.includes("sensor")) {
            this.showAllSensorHistoricData();
            // this.makeSage2Message("Showing as many sensor historic data as possible", 2000);
            return;
        }
    } else if (responseObject.clientInput.includes("all")) {
        this.showAllSensorHistoricData();
        // this.makeSage2Message("Showing as many sensor historic data as possible", 2000);
        return;
    }

    
    // only gets this far, if "all" was not used.
    var sensorNumber = this.determineSensorNumberFromVoice(responseObject.clientInput);

    // can't do anythign if number not given, or invalid number given.
    if (sensorNumber === -1){
        return;
    }

    // there was a number, figure out if the sensor is plottable
    var subWindowOfSensor = this.childrenAppIds[sensorNumber - 1]; // get id
    subWindowOfSensor = applications[subWindowOfSensor]; // get actual app.
    if (!subWindowOfSensor) {
        return;
    }
    // find the sensor observerid number
    var obsrvId = this.determineObserverIdFromUrl(subWindowOfSensor.element.src);
    var dataSetToUse;
    for (let i = 0; i < this.currentSensorSelection.length; i++) {
        console.log("Checking again id:" + this.currentSensorSelection[i].obsrvId);
        if (+this.currentSensorSelection[i].obsrvId === +obsrvId) {
            
            if (this.currentSensorSelection[i].type === "dam"){
                dataSetToUse = this.refSensorDam;
            } else if (this.currentSensorSelection[i].type === "quality"){
                dataSetToUse = this.refSensorQuality;
            } else if (this.currentSensorSelection[i].type === "rain"){
                dataSetToUse = this.refSensorRain;
            } else if (this.currentSensorSelection[i].type === "shore"){
                dataSetToUse = this.refSensorShore;
            } else if (this.currentSensorSelection[i].type === "snow"){
                dataSetToUse = this.refSensorSnow;
            } else if (this.currentSensorSelection[i].type === "water"){
                dataSetToUse = this.refSensorWater;
            }
            
            this.launchAppWithValues("plotlyTest",
                {
                    xValues: dataSetToUse.times,
                    xTitle: "Date",
                    yValues: dataSetToUse[this.currentSensorSelection[i].obsAid],
                    yTitle: dataSetToUse.vName,
                },
                3000,
                1000);

            setTimeout(() => { this.repositionChildrenIfNecessary("chart"); }, 3000);

            return;
        }
    }
    this.makeSage2Message("Coule not find obsrvId " + obsrvId + " of sensor " + sensorNumber, 2000);

},

showAllSensorHistoricData: function() {
    var sensors = this.currentSensorSelection;
    var dataSetToUse;

    var delayCount = 0;

    // for each of the sensors first figure out which subwindow is showing its data.
    for (let i = 0; i < sensors.length; i++) {
        for (let cid = 0; cid < this.childrenAppIds.length; cid++){
            if (applications[this.childrenAppIds[cid]].state.url
                == this.subWindowsSrcs[sensors[i].wy][sensors[i].wx]) {
                // console.log("Subwindow " + applications[this.childrenAppIds[cid]]
                // + " line color " + this.linesToSub[cid].attr("stroke")
                // + " detected for sensor at " + sensors[i].lat + "," + sensors[i].lng
                // + " of type " + sensors[i].type);

                // then figure out which data set to use.
                if (sensors[i].type === "dam"){
                    dataSetToUse = this.refSensorDam;
                } else if (sensors[i].type === "quality"){
                    dataSetToUse = this.refSensorQuality;
                } else if (sensors[i].type === "rain"){
                    dataSetToUse = this.refSensorRain;
                } else if (sensors[i].type === "shore"){
                    dataSetToUse = this.refSensorShore;
                } else if (sensors[i].type === "snow"){
                    dataSetToUse = this.refSensorSnow;
                } else if (sensors[i].type === "water"){
                    dataSetToUse = this.refSensorWater;
                }
                // if there is data for that sensor.
                if (dataSetToUse[sensors[i].obsAid]) {
                    // launch the ploty app

                    setTimeout(() => {
                        this.launchAppWithValues("plotlyTest", {
                                xValues: dataSetToUse.times,
                                xTitle: "Date",
                                yValues: dataSetToUse[sensors[i].obsAid],
                                yTitle: dataSetToUse.vName,
                            },
                            applications[this.childrenAppIds[cid]].sage2_x,
                            applications[this.childrenAppIds[cid]].sage2_y);
                    }, i * 1000);
                    
                    delayCount++;

                    // console.log("  SUCCCESS: found entry in " + sensors[i].type + " sensor data");
                    // console.log("  value name:" + dataSetToUse.vName);
                    // console.log("  entries:" + dataSetToUse[sensors[i].obsAid].length);
                }
            }
        }
    }

    if (delayCount > 0) {
        delayCount++;
        delayCount *= 1000;
        setTimeout(() => { this.repositionChildrenIfNecessary("chart"); }, delayCount);
    }
},



// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// launches google map and automatically binds data between.

/*
when sensor is to be shown on map, first check if map was created by this application.

*/
showSensorOnMap: function(responseObject) {
    if (!this.launchedGmapToShowSensors) { // if a map has not already been launched. launch one.
        this.launchGmapForSensors();
        setTimeout(() => { this.setupGmapDataLinks(responseObject); }, 2000);
    } else {
        this.sendGmapSensorFocus(responseObject); // else check for it then add.
    }
},

/*
need to launch the gmap.
*/
launchGmapForSensors: function() {
    this.launchedGmapToShowSensors = true;

    var x, y;
    y = this.sage2_y;
    x = this.sage2_x + this.sage2_width;
    x += applications[this.childrenAppIds[0]].sage2_width + 200; // temp position while waiting for open.

    // launchAppWithValues: function(appName, paramObj, x, y, funcToPassParams)
    // this.launchAppWithValues("gmapMod", null, x, y); // !!! this is not the original google map.
    this.launchAppWithValues("googlemaps", null, x, y); // !!! this is not the original google map.
},

/*
This is a recursive funciton until the map app is detected as launched.
When it is detected, then it will reposition children if necessary.
Finally send the sensor data.
*/
setupGmapDataLinks: function(responseObject) {
    var foundMap;
    // check if map is active
    foundMap = this.findGmapOutOfChildren();

    if (!foundMap) { // if not, wait a bit.
        setTimeout(() => { this.setupGmapDataLinks(); }, 500);
        return;
    }
    // reposition children if necessary
    this.repositionChildrenIfNecessary("map");



    /*
    Old system which isn't available on current master branch
    // need to make links for
    // gmap -> this on attempt to make url resolution
	SAGE2SharedServerData.createDataLinkOnServer(
        foundMap.id + ":source:testConvert:geoLocationCenter", this.id + ":destination:trySetPageToGps");
    // this -> gmap for sensor list
	SAGE2SharedServerData.createDataLinkOnServer(
        this.id + ":source:currentSensorList", foundMap.id + ":destination:testConvert:markerReplaceNoViewChange");

    To compensate for master branch, will do a subscription initiation.
    Helper function cannot be used due to making a different app subscribe to the value.
    */

    // first up, get the map center coordinates to this app.
    wsio.emit("serverDataSubscribeToValue", {
        nameOfValue: foundMap.id + ":source:testConvert:geoLocationCenter", // need to know name formatting
        app: this.id,
        func: "trySetPageToGps"
    });
    // now make a subscription for the map application to receive the sensor list
    /*
	wsio.emit("serverDataSubscribeToValue", {
        nameOfValue: this.id + ":source:currentSensorList", // need to know name formatting
        app: this.id, // the map's id
        func: "replaceMarkerPlotsConvertedByS2NoViewChange"
    });
	*/

    // send data.
    this.sendGmapSensorFocus(responseObject);
},

/*
find the gmap child;
return false if not found.
*/
findGmapOutOfChildren: function() {
    var foundMap = false;
    // check if the map is active
    for (let i = 0; i < this.childrenAppIds.length; i++) {
        // if (applications[this.childrenAppIds[i]].application === "gmapMod") {
        //     foundMap = applications[this.childrenAppIds[i]];
        // }
        if (applications[this.childrenAppIds[i]]
            && applications[this.childrenAppIds[i]].application === "googlemaps") {
            foundMap = applications[this.childrenAppIds[i]];
        }
    }
	
	// Modifying the function calls
	if (foundMap !== false) {
		if (foundMap.originalSetLocationFunction === undefined) {
			// Create the source variable for the map
			foundMap.serverDataBroadcastSource("currentTown", "" , "");
			foundMap.originalSetLocationFunction = foundMap.setLocation;
			foundMap.setLocation = function(msgParams) {
				// Keyword "this" is the map app
				this.originalSetLocationFunction(msgParams);
				// May not be a town
				this.serverDataSetSourceValue("currentTown", msgParams.clientInput);
				// After about 1 second, try update the center
				// This is a weird one due to the geocoder being asynchronous
				setTimeout(() => {
					this.updateCenter();
				}, 1500);
			}
		}
		if (foundMap.originalUpdateCenterFunction === undefined) {
			// Create the source variable for center
			foundMap.serverDataBroadcastSource("testConvert:geoLocationCenter",
				[foundMap.state.center], {
					app: foundMap.id,
					interpretAs: "set",
					dataTypes: ["gps"], // gives these datatypes, example: geojson can have more, but may only have points.
					dataFormat: "s2GeoLocation"
				}
			);
			// Create bounds source var
			foundMap.serverDataBroadcastSource("testConvert:geoLocationBounds", [{
				source: foundMap.id,
				location: foundMap.state.center
			}, {
				source: foundMap.id,
				location: foundMap.state.center
			}], {
				app: foundMap.id,
				interpretAs: "range",
				dataTypes: ["gps"], // gives these datatypes, example: geojson can have more, but may only have points.
				dataFormat: "s2GeoLocation"
			});
			foundMap.originalUpdateCenterFunction = foundMap.updateCenter;
			foundMap.updateCenter = function() {
				// Keyword "this" is the map app
				this.originalUpdateCenterFunction();
				if (isMaster) {
					// function(nameOfValue, value, description)
					this.serverDataSetSourceValue("testConvert:geoLocationCenter", this.state.center);
					var boundArray = [];
					var ne = this.map.getBounds().getNorthEast();
					var sw = this.map.getBounds().getSouthWest();
					boundArray.push({lat: ne.lat(), lng: ne.lng()});
					boundArray.push({lat: sw.lat(), lng: sw.lng()});
					this.serverDataSetSourceValue("testConvert:geoLocationBounds", boundArray);
				}
			}
		}
	}
	
    return foundMap;
},


/*
To show a sensor,
look for the map. no map means can't do anything anyway.
get the number of the sensor
    no number means just show japan
    number has to be valid 1-8

*/
sendGmapSensorFocus: function(responseObject) {
    var gmap;
    gmap = this.findGmapOutOfChildren();
    if (!gmap) {
        return; // unable to send data if there is no map.
    }
    
    // returns -1 or 1->8
    var sensorNumber = this.determineSensorNumberFromVoice(responseObject.clientInput);

    // false means that there was no number given, so unable to plot the map.
    if (sensorNumber === -1) { // 1 - 8
        this.makeSage2Message("A sensor number was not provided", 2000);
        return;
    }
    // there was a number, figure out if the sensor is plottable
    var subWindowOfSensor = this.childrenAppIds[sensorNumber - 1]; // get id
    subWindowOfSensor = applications[subWindowOfSensor]; // get actual app.
    if (!subWindowOfSensor) {
        console.log("Could not find sub window");
        this.showJapanOnTheGmap();
        return;
    }
    // find the sensor observerid number
    var obsrvId = this.determineObserverIdFromUrl(subWindowOfSensor.element.src);
    if (this.currentSensorSelection) {
        for (let i = 0; i < this.currentSensorSelection.length; i++) {
            console.log("Checking current sensor id:" + this.currentSensorSelection[i].obsrvId);
            if (+this.currentSensorSelection[i].obsrvId == +obsrvId) {
                this.focusGmapOnSensor(this.currentSensorSelection[i]);
                return;
            }
        }
    }
    console.log("Failed to find  obsrvId:" + obsrvId);
    this.showJapanOnTheGmap();
},


determineSensorNumberFromVoice: function(transcript) {
    transcript = transcript.toLowerCase().trim().replace(/for/g, '4'); // hard coding number conversions...
    transcript = transcript.toLowerCase().trim().replace(/three/g, '3');
    transcript = transcript.toLowerCase().trim().replace(/six/g, '6');
    var sensorNumber = transcript.toLowerCase().trim().split(' ');

    // if the input is too much need to get only the number
    if (sensorNumber.length > 1) {
        for (let i = 0; i < sensorNumber.length; i++) {
            if (!isNaN(+sensorNumber[i])) {
                sensorNumber = +sensorNumber[i];
                break;
            }
            else if (i === sensorNumber.length - 1) {
                sensorNumber = false;
            }
        }
    } else if (sensorNumber.length === 1) {
        sensorNumber = +sensorNumber[0];
        if (isNaN(sensorNumber)) {
            sensorNumber = false;
        }
    } else {
        sensorNumber = false;
    }

    // false means that there was no number given, so unable to plot the map.
    if (!sensorNumber || (sensorNumber > 8) || (sensorNumber < 1)) {
        // show just japan
        console.log("Invalid sensor number:" + sensorNumber);
        if (!sensorNumber) {
            this.makeSage2Message("Sorry, sensor '" + transcript + "' cannot be displayed", 2000);
        } else {
            this.makeSage2Message("Please use sensor 1 - 8", 2000);
        }
        return -1;
    }
    return sensorNumber;
},

showJapanOnTheGmap: function() {
    this.surpressRedirect = true;

    var gmap = this.findGmapOutOfChildren();
    if (gmap) {
        gmap.setZoomLevel({clientInput: 6});
        gmap.setLocation({clientInput: "Japan"});
    }
    this.makeSage2Message("Sorry that sensor did not have position data", 2000);
    setTimeout(() => { this.surpressRedirect = false; }, 1000); // idk how much is enough.
},

/*
should have 
    gps.lat = sensorsInTown[i].lat;
    gps.lng = sensorsInTown[i].lng;
    gps.obsAid = sensorsInTown[i].obsAid;
    gps.obsrvId = sensorsInTown[i].obsrvId;
    gps.wx = x;
    gps.wy = y;
    gps.type = sensorType;
*/
focusGmapOnSensor: function(sensorData) {
    this.surpressRedirect = true;

    var gmap = this.findGmapOutOfChildren();
    if (gmap) {
        gmap.setZoomLevel({clientInput: 12});
		// First remove current markers
		gmap.removeAllMarkersFromMap();
		gmap.addMarkerToMap({
			lat: +sensorData.lat,
			lng: +sensorData.lng,
			sourceAppId: this.id
		});
        // gmap.setViewUsingS2GeoLocation([{lat: +sensorData.lat, lng: +sensorData.lng}]);
        // gmap.replaceMarkerPlotsConvertedByS2NoViewChange([{lat: +sensorData.lat, lng: +sensorData.lng}]);
    }

    setTimeout(() => { this.surpressRedirect = false; }, 1000); // idk how much is enough.
},

makeSage2Message: function(message, timeToShow) {
    if (!timeToShow) {
        timeToShow = 2000;
    }
    var rdiMessageDialogBox =  ui.buildMessageBox('rdiMessageDialogBox', message);
    ui.main.appendChild(rdiMessageDialogBox); // Add to the DOM
    rdiMessageDialogBox.style.display = "block"; // Make the dialog visible
    // delete it later
    setTimeout(() => { deleteElement('rdiMessageDialogBox'); }, timeToShow);
},


/*
    To be called after launching google maps, or a chart.
*/
repositionChildrenIfNecessary: function() {
    var maps = [], charts = [], firstLaunched = false, typesOpen = "";
    
    // first find charts and map, if any
    console.log("repositioning children, total of " + this.childrenAppIds.length);
    console.log("  between maps, charts, messages:" + (this.childrenAppIds.length - 8));
    // search from 8 since they should always be the subwindows
    for (let i = 8; i < this.childrenAppIds.length; i++) {
        if (applications[this.childrenAppIds[i]].application.includes("map") // including map to hit gmapMod and googlemaps
            || applications[this.childrenAppIds[i]].application.includes("ebview")) { // only initial 8 + weather should be web
            maps.push(applications[this.childrenAppIds[i]]);
            if (!firstLaunched) {
                firstLaunched = "map";
            }
            if (!typesOpen.includes("map")) {
                typesOpen += "map";
            }
        } else if (applications[this.childrenAppIds[i]].application.includes("plot")) { // full name is plotlyTest
            charts.push(applications[this.childrenAppIds[i]]);
            if (!firstLaunched) {
                firstLaunched = "chart";
            }
            if (!typesOpen.includes("chart")) {
                typesOpen += "chart";
            }
        }
    }

    // should not happen...
    if (!firstLaunched) {
        console.log("Error reorganizing children of RDI? Did not detect map or chart");
        return;
    }

    // areas for placement:
    var xStartReorganization = ui.width / 2;
    var allowedSpace;
    var organizeFirst, organizeSecond;
    if (firstLaunched === "map") {
        organizeFirst = maps;
        organizeSecond = charts;
    } else if (firstLaunched === "chart") {
        organizeFirst = charts;
        organizeSecond = maps;
    } else {
        console.log("Error in RDI, unknown newest added value:" + firstLaunched);
    }
    allowedSpace = ui.width / 2; // by default only 1 type open means they get half the screen.
    // if both types are open, then they only get 1/4 the screen space
    if (typesOpen.includes("map") && typesOpen.includes("chart")) {
        allowedSpace = ui.width / 4;
    } else {
        console.log("Only one type is open");
        organizeSecond = null; // there were not both types open
    }

    // organize first
    console.log("Expecting to reposition " + organizeFirst.length + " windows");
    console.dir(organizeFirst);
    this.repositionLogic(organizeFirst, {x: xStartReorganization, y: 200, width: allowedSpace, height: ui.height});
    if (organizeSecond) {
        console.log("Expecting to reposition " + organizeSecond.length + " windows");
        console.dir(organizeSecond);
        this.repositionLogic(organizeSecond, {x: xStartReorganization + ui.width / 4, y: 200, width: allowedSpace, height: ui.height});
    }
    return;
},

/*
    This will reposition the map and charts based on a couple assumptions:
    the river app takes up the LEFT half of the screen.
    the ratio to use of app is 9:16
        this will not cause problem for displays without this ration, but the apps may appear "squished"
    if only maps or charts are active, they get half the screen.
        new: weather search shares space with map?
*/
repositionLogic: function(whatToRepos, bounds) {
    if (whatToRepos === null) {
        return;
    }

    var count = whatToRepos.length;
    var ratioOfHeightToWidth = 9 / 16;
    // var ratioWidthByHeight = bounds.width / bounds.height; 
    // hard coded for now. every three height per 1 width

    // this code prioritizes columns.
    var rows, columns, currentWidth, currentHeight, sizeFits = false, titleBarHeight = ui.titleBarHeight;
    currentWidth = bounds.width;
    columns = 1;
    while(!sizeFits) {
        // title bar is messing with calc, also it changes depending on wall
        currentHeight = currentWidth * ratioOfHeightToWidth + titleBarHeight;
        if ((parseInt(count / columns) * currentHeight) <= bounds.height) {
            sizeFits = true;
        } else {
            columns++;
            currentWidth = bounds.width / columns;
        }
    }
    // should now have a column, height, width, that works.
    rows = Math.ceil(count / columns);
    var cxPos = bounds.x, cyPos, cyOriginal = bounds.y;
    var posAdjust;
    // special case for columns 1, want to "center" them.
    if (columns === 1) {
        cyOriginal = bounds.height / (whatToRepos.length + 1) - currentHeight / 2; // divide equally
    }
    for (let ci = 0; ci < columns; ci++) {
        cyPos = cyOriginal;
        for (let ri = 0; ri < rows; ri++) {
            if (ci * rows + ri >= whatToRepos.length) {
                break; // cannot reposition more windows than available
            }
            posAdjust = {};
            posAdjust.appPositionAndSize = {};
            posAdjust.appPositionAndSize.elemId = whatToRepos[(ci * rows) + ri].id;
            posAdjust.appPositionAndSize.elemLeft = cxPos;
            posAdjust.appPositionAndSize.elemTop = cyPos;
            posAdjust.appPositionAndSize.elemHeight = currentHeight;
            posAdjust.appPositionAndSize.elemWidth = currentWidth;
            console.log("   " + whatToRepos[(ci * rows) + ri].id + " given position" + cxPos + "," + cyPos);
            // send
            wsio.emit("updateApplicationPosition", posAdjust);
            setTimeout( () => { whatToRepos[(ci * rows) + ri].sendResize(currentWidth, currentHeight); }, 100);
            cyPos += currentHeight + titleBarHeight;
        }
        cxPos += currentWidth;
    }
},


/*
This should not have to be necessary if the server was correctly giving back data.
More than anything this is a debug check.
however it is possible that children ids are the same because launching is asynchronous.
multiple calls in a very short amount of time will not give server (or display?) enough time to create the app and increment the counter.
*/
uniqueChildrenIdCheck: function() {
    var arrayCopy = this.childrenAppIds.slice();
    var dupes = [];
    var current;
    while (arrayCopy.length > 0) {
        current = arrayCopy.shift();
        if (arrayCopy.includes(current)) {
            dupes.push(current);
        }
    }
    if (dupes.length > 0) {
        console.log("Multiple children id duplicates detected: " + dupes);
        console.log("Double check this.childrenAppIds: " + this.childrenAppIds);
        console.log("Performing correction with assumptions");
        current = this.childrenAppIds.indexOf(dupes[0]) + 1; // go one after the dupe
        while (current < this.childrenAppIds.length) {
            // take the previous app id, split on _, change the right side to number, increase by 1, then rejoin with app_
            this.childrenAppIds[current] = "app_" + (+this.childrenAppIds[current - 1].split('_')[1] + 1);
            current++;
        }
        console.log("Double check this.childrenAppIds: " + this.childrenAppIds);
    }
},

// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// creates a note for sensor, probably need to check for additional information from the webpage
// reason: mark sensor, "its missing"

flagSensorOnMap: function(responseObject) {
    // returns -1 or 1->8
    var sensorNumber = this.determineSensorNumberFromVoice(responseObject.clientInput);
    var extraInfo = "";

    // false means that there was no number given, so unable to plot the map.
    if (sensorNumber === -1) { // 1 - 8
        this.makeSage2Message("Unknown sensor:" + responseObject.clientInput, 2000);
        return;
    }
    // there was a number, figure out if the sensor is plottable
    var subWindowOfSensor = this.childrenAppIds[sensorNumber - 1]; // get id
    subWindowOfSensor = applications[subWindowOfSensor]; // get actual app.
    if (!subWindowOfSensor) {
        return;
    }
    // find the sensor observerid number
    var obsrvId = this.determineObserverIdFromUrl(subWindowOfSensor.element.src);
    var sensor = false;
    for (let i = 0; i < this.currentSensorSelection.length; i++) {
        console.log("Checking current sensor id:" + this.currentSensorSelection[i].obsrvId);
        if (+this.currentSensorSelection[i].obsrvId == +obsrvId) {
            sensor = this.currentSensorSelection[i];
            break;
        }
    }

    // if there is no sensor, rather than not showing anything, instead state that it doesn't have data.
    // want: observer id, prefecture code, and town code
    if (!sensor) {
        // this.makeSage2Message("Problem with sensor " + sensorNumber, 2000);
        sensor = {};
        var xTile, yTile, sensorIndex;
        sensorIndex = sensorNumber - 1;
        if (sensorIndex < 3) { // doing a quick hardcode.
            xTile = sensorIndex;
            yTile = 0;
        } else if (sensorIndex === 3) {
            xTile = 0;
            yTile = 1;
        } else if (sensorIndex === 4) {
            xTile = 2;
            yTile = 1;
        } else {
            xTile = sensorIndex - 5; // 5, 6, 7 is bottom row but turn into 0,1,2
            yTile = 2;
        } // get full src from that window
        var oid = this.subWindowsSrcs[yTile][xTile]; // y then x
        // first get sensor type
        sensor.type = this.determineSensorTypeFromUrl(oid);
        // then get the observer id
        oid = oid.substring(oid.indexOf("obsrvId")); // chop after obsrvId
        oid = oid.substring(oid.indexOf("=") + 1, oid.indexOf("&")); // take between = and &
        // set value
        sensor.obsrvId = oid;
        sensor.missingData = true;
    }
    var areaCode, prefectureCode, townCode;
    areaCode = prefectureCode = townCode = this.mainWebView.src;
    areaCode = areaCode.substring(areaCode.indexOf("areaCd")); // chop after code
    areaCode = areaCode.substring(areaCode.indexOf("=") + 1, areaCode.indexOf("&")); // take between = and &
    prefectureCode = prefectureCode.substring(prefectureCode.indexOf("prefCd")); // chop after code
    prefectureCode = prefectureCode.substring(prefectureCode.indexOf("=") + 1, prefectureCode.indexOf("&")); // take between = and &
    townCode = townCode.substring(townCode.indexOf("townCd")); // chop after code
    townCode = townCode.substring(townCode.indexOf("=") + 1, townCode.indexOf("&")); // take between = and &

    // gps.lat = sensorsInTown[i].lat;
    // gps.lng = sensorsInTown[i].lng;
    // gps.obsAid = sensorsInTown[i].obsAid;
    // gps.obsrvId = sensorsInTown[i].obsrvId;
    // gps.wx = x;
    // gps.wy = y;
    // gps.type = sensorType;
	var data = {};
    // start with data available in both
	data.colorChoice = "salmon";
    data.clientName = "Sensor: " + sensor.obsrvId;
    data.clientInput = "--Inspection Request--\nSensor " + sensor.obsrvId + " (" + sensor.type + ").\n";
    // only possible if there was no historic data and went through the !sensor check
    if (sensor.missingData) {
        data.clientInput += "No Data History\n";
        data.clientInput += "Coordinates: N/A \n";
    } else {
        data.clientInput += "Coordinates: " + sensor.lat + "," + sensor.lng + "\n";
    }
    data.clientInput += "Area: " + areaCode + "\n";
    data.clientInput += "Prefecture: " + prefectureCode + "\n";
    data.clientInput += "Town: " + townCode + "\n";

    subWindowOfSensor = applications[this.childrenAppIds[sensorNumber - 1]];
    this.launchAppWithValues("quickNote", data, subWindowOfSensor.sage2_x, subWindowOfSensor.sage2_y);

},

// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------------------------
// open a webview and link the search from the map into the weather query

/*
Check if webview is open. Only one for webview.
If not open
*/
showMapWeatherOfSearch: function() {
    var mapWindow = this.findGmapOutOfChildren();
    var weatherWindow = this.findWeatherWindowOutOfChildren();
    if (!mapWindow) {
        this.makeSage2Message("An active map is needed", 2000);
        return;
    }
    if (!weatherWindow) {
        // launch webview to show weather
        this.launchAppWithValues("Webview", {}, this.sage2_x, this.sage2_y);
        setTimeout(() => { this.setupWebviewWeatherLinks(); }, 2000);
    }
},

/*
Searches for a webview, which this app assumes to be the only webview open as a child AFTER the initial 8.
The first 8 are the individual sensor views.
*/
findWeatherWindowOutOfChildren: function() {
    var weatherWindow = false;
    for (let i = 8; i < this.childrenAppIds.length; i++) {
        if (applications[this.childrenAppIds[i]].application === "Webview") {
            weatherWindow = applications[this.childrenAppIds[i]];
            break;
        }
    }
    return weatherWindow;
},

/*

*/
setupWebviewWeatherLinks: function(responseObject) {
    var weatherWindow = this.findWeatherWindowOutOfChildren();
    var mapWindow = this.findGmapOutOfChildren();
    if (!weatherWindow) { // if not, wait a bit.
        setTimeout(() => { this.setupWebviewWeatherLinks(); }, 500);
        return;
    }
    if (!weatherWindow) { // if no map, cannot complete action
        this.makeSage2Message("Map is missing", 2000);
        return;
    }
    // reposition children if necessary
    this.repositionChildrenIfNecessary("map");


    /*
    Old version that doesn't work on current master branch

    // need to make links for
    // gmap -> browser for weather
	SAGE2SharedServerData.createDataLinkOnServer(
        mapWindow.id + ":source:currentTown", weatherWindow.id + ":destination:weatherSearch");

    Modified to work with current master branch, this will setup a handler on RDI to pass the search
    */

    // first up, get the map center coordinates to this app.
    wsio.emit("serverDataSubscribeToValue", {
        nameOfValue: mapWindow.id + ":source:currentTown", // need to know name formatting
        app: this.id,
        func: "forwardWeatherFromMapToWebview"
    });

},

forwardWeatherFromMapToWebview: function(searchString) {
    var weatherWindow = this.findWeatherWindowOutOfChildren();

    if (weatherWindow) {
        weatherWindow.navigation({action: "search", clientInput: "weather " + searchString});
    }
},


passLocationToMapSearch: function(responseObject) {
    var mapWindow = this.findGmapOutOfChildren();
    if (mapWindow) {
        var loc = responseObject.clientVoiceInput.toLowerCase();
        if (loc.includes("go to ")) {
            loc = loc.substring(loc.indexOf("go to") + 5).trim();
        } else if ("show me ") {
            loc = loc.substring(loc.indexOf("show me") + 7).trim();
        }
        mapWindow.setLocation({clientInput: loc});
    }
},

sendSensorListToGmap: function(townSensorList) {
	// Effect semi replaces currentSensorList
    var mapWindow = this.findGmapOutOfChildren();
	if (mapWindow) {
		// Clear out all marker plots
		mapWindow.removeAllMarkersFromMap();
		// Plot the new ones.
		for (let i = 0; i < townSensorList.length; i++) {
			mapWindow.addMarkerToMap(townSensorList[i], true);
		}
	}
},

placeholder: ""
});
