//
// SAGE2 application: osgjs
// by: Luc Renambot <renambot@gmail.com>
//
// Copyright (c) 2015
//

/* global OSG */

var osgjs = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("canvas", data);
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "onfinish";

		// SAGE2 Application Settings
		//
		// Control the frame rate for an animation application
		this.maxFPS = 30.0;
		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;

		this.createSceneCallbackFunc = this.createSceneCallback.bind(this);

		// App
		try {
			var opts = {
				antialias: true,
				overrideDevicePixelRatio: 1,
				fullscreen: false,
				enableFrustumCulling: false,
				stats: true,
				statsNoGraph: false,
				scrollwheel: false
			};

			this.viewer = new OSG.osgViewer.Viewer(this.element, opts);
			this.viewer.init();
			this.viewer.getCamera().setClearColor([0.4, 0.4, 0.4, 1.0]);
			this.viewer.setSceneData(this.createSceneCallbackFunc());
			this.viewer.setupManipulator();
			this.viewer.getManipulator().computeHomePosition();

			this.angle = 0.0;

		} catch (er) {
			OSG.osg.log("exception in osgViewer " + er);
		}
	},

	createSceneCallback: function() {
		// the root node
		this._root = new OSG.osg.Node();

		// Create a box
		this._model = new OSG.osg.MatrixTransform();
		var geom = OSG.osg.createTexturedBoxGeometry(0, 0, 0, 10, 10, 10);
		this._model.addChild(geom);
		this._root.addChild(this._model);
		this._root.light = new OSG.osg.Light();

		// Load a model from file
		// this._model = new OSG.osg.MatrixTransform();
		// this._root.addChild( this._model );
		// var request = OSG.osgDB.readNodeURL( this.resrcPath + 'data/raceship.osgjs' );
		// request.then( function ( model ) {
		// 	this._model.addChild( model );
		// 	this.viewer.getManipulator().computeHomePosition();
		// }.bind(this) );

		return this._root;
	},

	load: function(date) {
		this.refresh(date);
	},

	draw: function(date) {
		var m = this._model.getMatrix();
		OSG.osg.Matrix.makeRotate(-this.angle, 0.0, 0.0, 1.0, m);
		OSG.osg.Matrix.setTrans(m, 0, 0, 0);
		this.angle += 0.02;
		// this._model.dirtyBound();

		// OSG draw function
		this.viewer.frame();
	},

	resize: function(date) {
		// Update OSG canvas
		this.viewer.updateViewport();
		// Call SAGE2 for redraw
		this.refresh(date);
	},
	move: function(date) {
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// pass
		} else if (eventType === "pointerMove" && this.dragging) {
			// pass
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// pass
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// pass
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") { // left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") { // up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") { // right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") { // down
				this.refresh(date);
			}
		}
	}
});
