// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2015

//
// SAGE2 application: p5demo
// by: Luc Renambot <renambot@gmail.com>
//

"use strict";

/* global p5 */

var p5demo = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "onfinish";
		this.passSAGE2PointerAsMouseEvents = true;

		var myinstance = function(p) {
			// var x = 100.0;
			// var y = 100;
			// var speed = 2.5;

			p.setup = function() {
				p.createCanvas(data.width, data.height);
				p.noLoop();
			};

			p.draw = function() {
				if (p.mouseIsPressed) {
					p.fill(0);
				} else {
					p.fill(255);
				}
				p.ellipse(p.mouseX, p.mouseY, 80, 80);

				// p.background(200, 20, 20);
				// p.fill(1);
				// x += speed;
				// if (x > p.width) {
				// 	x = 0;
				// }
				// p.ellipse(x, y, 50, 50);
			};
		};
		// Save the P5 object and attach it to the SAGE2 Div
		/* eslint-disable new-cap */
		this.myp5 = new p5(myinstance, this.element);

		// Control the frame rate for an animation application
		this.maxFPS = 20.0;

		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;
	},

	load: function(date) {
		this.refresh(date);
	},

	draw: function(date) {
		// Call P5 to draw
		this.myp5.redraw();
	},

	resize: function(date) {
		// Called when window is resized
		this.myp5.resizeCanvas(this.sage2_width, this.sage2_height);
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		} else if (eventType === "pointerMove" && this.dragging) {
			// move
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// click release
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// widget events
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") {
				// left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.refresh(date);
			}
		}
	}
});
