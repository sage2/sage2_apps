//
// SAGE2 application: physics3d
// by: Luc Renambot <renambot@gmail.com>
//
// Copyright (c) 2016
//

//
// Adapted from "body.html" Physijs demo
// from http://chandlerprall.github.io/Physijs/
//

"use strict";

/* global THREE, Physijs */


var physics3d = SAGE2_WebGLApp.extend({
	init: function(data) {
		// Create div into the DOM
		this.WebGLAppInit('canvas', data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// SAGE2 Application Settings
		//
		// Control the frame rate for an animation application
		this.maxFPS = 30.0;

		Physijs.scripts.worker = this.resrcPath + 'libs/physijs_worker.js';
		Physijs.scripts.ammo   = this.resrcPath + 'libs/ammo.js';

		this.renderer = null;
		this.camera   = null;
		this.scene    = null;
		this.ready    = null;

		this.frame  = 0;
		this.width  = this.element.clientWidth;
		this.height = this.element.clientHeight;
		this.ready    = false;

		// build the app
		this.initialize(data.date);
		this.resizeCanvas();
		this.refresh(data.date);
	},

	initialize: function(date) {
		// CAMERA
		this.camera = new THREE.PerspectiveCamera(35, this.width / this.height, 1, 1000);
		// SCENE
		// this.scene = new THREE.Scene();
		this.scene = new Physijs.Scene;
		this.scene.setGravity(new THREE.Vector3(0, -30, 0));
		// RENDERER
		if (this.renderer == null) {
			this.renderer = new THREE.WebGLRenderer({
				canvas: this.canvas,
				antialias: true
			});
			this.renderer.shadowMap.enabled = true;
			this.renderer.shadowMapSoft = true;
			this.renderer.setSize(this.width, this.height);
		}
		this.camera.position.set(60, 50, 60);
		this.camera.lookAt(this.scene.position);
		this.scene.add(this.camera);

		// Light
		var light = new THREE.DirectionalLight(0xFFFFFF);
		light.position.set(20, 40, -15);
		light.target.position.copy(this.scene.position);
		light.castShadow = true;
		light.shadowCameraLeft = -60;
		light.shadowCameraTop = -60;
		light.shadowCameraRight = 60;
		light.shadowCameraBottom = 60;
		light.shadowCameraNear = 20;
		light.shadowCameraFar = 200;
		light.shadowBias = -.0001;
		light.shadowMapWidth = light.shadowMapHeight = 2048;
		light.shadowDarkness = .7;
		this.scene.add(light);

		// Loader
		var loader = new THREE.TextureLoader();

		// Materials
		var ground_material = Physijs.createMaterial(
			new THREE.MeshLambertMaterial({ map: loader.load(this.resrcPath + 'images/rocks.jpg') }),
			.8, // high friction
			.4  // low restitution
		);
		ground_material.map.wrapS = ground_material.map.wrapT = THREE.RepeatWrapping;
		ground_material.map.repeat.set(3, 3);

		var box_material = Physijs.createMaterial(
			new THREE.MeshLambertMaterial({ map: loader.load(this.resrcPath + 'images/plywood.jpg') }),
			.4, // low friction
			.6 // high restitution
		);
		box_material.map.wrapS = ground_material.map.wrapT = THREE.RepeatWrapping;
		box_material.map.repeat.set(.25, .25);

		// Ground
		var ground = new Physijs.BoxMesh(
			new THREE.BoxGeometry(100, 1, 100),
			ground_material,
			0 // mass
		);
		ground.receiveShadow = true;
		this.scene.add(ground);

		this.mouse_position = null;
		this.boxes = [];
		this.box = null;
		for (var i = 0; i < 10; i++) {
			var box = new Physijs.BoxMesh(
				new THREE.BoxGeometry(4, 4, 4),
				box_material
			);
			box.position.set(
				Math.random() * 50 - 25,
				10 + Math.random() * 5,
				Math.random() * 50 - 25
			);
			box.rotation.set(
				Math.random() * Math.PI * 2,
				Math.random() * Math.PI * 2,
				Math.random() * Math.PI * 2
			);
			box.scale.set(
				Math.random() * 1 + .5,
				Math.random() * 1 + .5,
				Math.random() * 1 + .5
			);
			box.castShadow = true;
			this.scene.add(box);
			this.boxes.push(box);
			this.box = box;
		}

		this.ready = true;

		// draw!
		this.resize(date);
	},


	load: function(date) {
		this.refresh(date);
	},

	draw: function(date) {
		if (this.ready) {
			// Phys
			this.applyForce();
			this.scene.simulate(undefined, 1);
			// Graphics
			this.renderer.clear();
			this.renderer.render(this.scene, this.camera);
		}
	},

	applyForce: function() {
		if (!this.mouse_position) {
			return;
		}

		var strength = 135, distance, effect, offset;

		for (var i = 0; i < this.boxes.length; i++) {
			this.box = this.boxes[i];
			distance = this.mouse_position.distanceTo(this.box.position),
			effect = this.mouse_position.clone()
				.sub(this.box.position)
				.normalize()
				.multiplyScalar(strength / distance)
				.negate(),
			offset = this.mouse_position.clone().sub(this.box.position);
			this.box.applyImpulse(effect, offset);
		}
	},


	// Local Threejs specific resize calls.
	resizeApp: function(resizeData) {
		if (this.renderer != null && this.camera != null) {
			this.renderer.setSize(this.canvas.width, this.canvas.height);

			this.camera.setViewOffset(this.sage2_width, this.sage2_height,
				resizeData.leftViewOffset, resizeData.topViewOffset,
				resizeData.localWidth, resizeData.localHeight);
		}
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	setMousePosition: function(clientX, clientY) {
		// Find where mouse cursor intersects the ground plane
		var vector = new THREE.Vector3(
			(clientX / this.sage2_width) * 2 - 1,
			-((clientY / this.sage2_height) * 2 - 1),
			.5
		);
		vector.unproject(this.camera);
		vector.sub(this.camera.position).normalize();

		var coefficient = (this.box.position.y - this.camera.position.y) / vector.y;
		this.mouse_position = this.camera.position.clone().add(vector.multiplyScalar(coefficient));
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		} else if (eventType === "pointerMove") {
			// move
			this.setMousePosition(position.x, position.y);
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// click release
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// widget events
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") {
				// left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.refresh(date);
			}
		}
	}
});
