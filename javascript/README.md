Added:
  - eslint_client_rc : linting file options for eslint tool
  - jscs_sage2.json : style options adapted from Airbnb guide, for jscs tool
    - removed (jscs merged with eslint)


To upload:
  - find . -type d -depth 1 -exec zip -r -9 {} {} -x \*/.DS_Store \;
  - zips all the apps

Valid application:
	CSV_Reader
	ChemViewer
	ParaSAGE
	Timer
	Timezone
	atom_smasher
	car_threejs
	cave2_monitor
	evl_photos
	evl_weather
	radar
	image360

Valid tutorials:
	JqueryUIDemo
	aMepTest
	bounce
	clock
	d3_sample
	dash
	forecast
	gmaps
	kinetic_animation
	kinetic_oscillating
	modest
	osgjs
	p5demo
	physics3d
	playcanvas
	prime
	scale_div
	snap_one
	stereo_viewer
	texture_cube
	threejs_sample
	threejs_shader
	tweetcloud2
	webrtc
	widget_demo


Deprecated applications:
	tank
	iframe
	multi_pdf_viewer
	presentation
	tweetcloud
	twitter_frame
	unity
	whiteboard

