// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

"use strict";

var remoteShareControl = SAGE2_App.extend({

	init: function(data) {
		// SAGE2_App
		this.SAGE2Init("div", data); // call super-class 'init'
		this.element.id = "div" + this.id; // Set id of element
		this.maxFPS = 1.0; // FPS only works if instructions sets animation true
		this.resizeEvents = "never"; // Options: never, continuous, onfinish
		this.passSAGE2PointerAsMouseEvents = true; // Set to true to enable SAGE2 auto conversion of pointer events to mouse events

		//
		this.appSpecific();
	},

	//
	appSpecific: function() {
		this.siteStatus = [];
		this.shareMode = "none";
		this.appsAlreadyShared = [];
		this.addAllOpenAppsAsAlreadyShared();

		this.element.style.fontFamily = "arial";
		// inject html code    file to grab from,    element.innerHTML to override
		this.loadHtmlFromFile(this.resrcPath + "design.html", this.element, () => {
			this.postHtmlFillActions();
			this.initializeSiteStatus();
			this.updateRemoteSitesAndStatus();
		});
	},

	/**
	 * If a design is loaded from html, this would be where to add event listeners. Name must match the function call named in loadHtmlFromFile
	 *
	 * @method     postHtmlFillActions
	 */
	postHtmlFillActions: function() {
		// get references
		this.visuals = {};
		this.visuals.remoteSites = document.getElementById(this.id + "remoteSites");
		this.visuals.rs_title = document.getElementById(this.id + "rs_title");
		this.visuals.rs_sites = document.getElementById(this.id + "rs_sites");
		//share buttons
		this.visuals.shareModeDiv = document.getElementById(this.id + "shareModeDiv");
		this.visuals.sm_title = document.getElementById(this.id + "sm_title");
		this.visuals.sm_btn_none = document.getElementById(this.id + "sm_btn_none");
		this.visuals.sm_btn_anyNewApp = document.getElementById(this.id + "sm_btn_anyNewApp");

		// style them
		this.visuals.rs_title.style.fontSize = ui.titleTextSize * 2 + "px";
		this.visuals.sm_title.style.fontSize = ui.titleTextSize * 2 + "px";
		// buttons
		this.visuals.sm_btn_none.style.fontSize = ui.titleTextSize + "px";
		this.visuals.sm_btn_anyNewApp.style.fontSize = ui.titleTextSize + "px";

		// Add listeners to the mode select
		this.visuals.sm_btn_none.style.background = "";
		this.visuals.sm_btn_anyNewApp.style.background = "";
		this.visuals.sm_btn_none.addEventListener("mousedown", () => { this.shareModeChange("none"); });
		this.visuals.sm_btn_anyNewApp.addEventListener("mousedown", () => { this.shareModeChange("any"); });
		this.shareModeChange("any");

	},

	// Use load for view synchronization across multiple clients / remote sites
	load: function(date) {},

	// Draw is called based on the maxFPS value and animation true
	draw: function(date) {
		// Update the remote sites
		this.updateRemoteSitesAndStatus();
		// Check for new apps
		let id;
		for (let app in applications) {
			id = applications[app].id;
			if (!this.appsAlreadyShared.includes(id)) {
				this.appsAlreadyShared.push(id);
				// prevent applications that are already shared from creating inf share loops
				if (id.includes(":") && id.includes("+")) {
					continue;
				}
				if (this.shareMode == "any") {
					this.shareAppWithSites(id);
				}
			}
		}
	},

	// Needs resizeEvents set to continuous or onfinish
	resize: function(date) {},

	/**
	* To enable right click context menu support this function needs to be present.
	*
	* Must return an array of entries. An entry is an object with three properties:
	*	description: what is to be displayed to the viewer.
	*	callback: String containing the name of the function to activate in the app. It must exist.
	*	parameters: an object with specified datafields to be given to the function.
	*		The following attributes will be automatically added by server.
	*			serverDate, on the return back, server will fill this with time object.
	*			clientId, unique identifier (ip and port) for the client that selected entry.
	*			clientName, the name input for their pointer. Note: users are not required to do so.
	*			clientInput, if entry is marked as input, the value will be in this property. See pdf_viewer.js for example.
	*		Further parameters can be added. See pdf_view.js for example.
	*/
	getContextEntries: function() {
		var entries = [];


		for (let i = 0; i < this.siteStatus.length; i++) {
			if (this.siteStatus[i].selected) {
				entries.push({
						description: "Stop Sharing to " + this.siteStatus[i].name + "(" + this.siteStatus[i].status + ")",
						callback: "toggleSiteStatus",
						parameters: {
							index: i,
							status: "stop"
						}
				});
			} else {
				entries.push({
						description: "Share to " + this.siteStatus[i].name + "(" + this.siteStatus[i].status + ")",
						callback: "toggleSiteStatus",
						parameters: {
							index: i,
							status: "start"
						}
				});
			}
		}

		entries.push({ description: "separator"});

		// Share Modes
		entries.push({
			description: "Share Mode: none",
			callback: "contextSetShareMode",
			parameters: {
				mode: "none"
			}
		});
		entries.push({
			description: "Share Mode: any",
			callback: "contextSetShareMode",
			parameters: {
				mode: "any"
			}
		});

		entries.push({ description: "separator"});
	
		// var entry;
		// entry = {
		// 	description: "This is what the context menu shows in the UI",
		// 	callback: "this_function_should_exist_on_this_app",
		// 	parameters: {
		// 		info: "In addition to the above values of passed parameter object, added parameters will also be available",
		// 	}
		// };
		// entries.push(entry);
		return entries;
	},

	toggleSiteStatus: function(responseObject) {
		if (responseObject.status == "start") {
			this.siteStatus[responseObject.index].selected = false; // opposite, since it will be flipped.
		} else {
			this.siteStatus[responseObject.index].selected = true; // opposite, since it will be flipped.
		}
		this.toggleSiteSelect(this.siteStatus[responseObject.index].btn);
	},

	contextSetShareMode: function(responseObject) {
		this.shareModeChange(responseObject.mode);
	},

	// Fill this out to handle pointer events. OR use the passSAGE2PointerAsMouseEvents
	event: function(eventType, position, user_id, data, date) {},

	// Is activated before closing, use this for cleanup or saving
	quit: function() {},






	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Detection of remote sites

	addAllOpenAppsAsAlreadyShared: function() {
		for (let app in applications) {
			if (app.id) {
				this.appsAlreadyShared.push(app.id);
			} else {
				console.log("Unknown property in app: " + app);
			}
		}
		this.appsAlreadyShared.push(this.id);
	},

	initializeSiteStatus: function() {
		let site;
		let element;
		let btn;
		for (let i = 0; i < ui.json_cfg.remote_sites.length; i++) {
			site = ui.json_cfg.remote_sites[i];

			btn = document.createElement("button");
			btn.style.width = "90%";
			btn.style.marginLeft = "5%";
			btn.style.fontSize = ui.titleTextSize + "px";
			btn.textContent = site.name;
			// add handler
			btn.appReference = this;
			btn.indexReference = i;
			btn.addEventListener("mousedown", function() {
				this.appReference.toggleSiteSelect(this);
			});

			//
			element = document.getElementById(site.name);
			this.visuals.rs_sites.appendChild(btn);
			this.visuals.rs_sites.appendChild(document.createElement("br"));
			this.visuals.rs_sites.appendChild(document.createElement("br"));
			this.siteStatus.push({name: site.name, status: "unknown", selected: false, btn, element});

			// error here for now please;
		}
		// Also add the blocks
	},

	updateRemoteSitesAndStatus: function() {
		// No alpha
		var connectedColor = "rgb(55, 153, 130)";
		// var disconnectedColor = "rgba(173, 42, 42)";
		// var lockedColor = "rgba(230, 110, 0)";
		
		// Check for each remote site
		let site;
		let oldText;
		let changed = false;
		for (let i = 0; i < this.siteStatus.length; i++) {
			site = this.siteStatus[i].element;
			// The blocks use backgroundColor, not background, also they don't have the alpha part
			if (site.style.backgroundColor === connectedColor) {
				this.siteStatus[i].status = "connected";
			} else {
				this.siteStatus[i].status = "unavailable";
			}
			oldText = this.siteStatus[i].btn.textContent;
			this.siteStatus[i].btn.textContent = this.siteStatus[i].name + " (" + this.siteStatus[i].status + ")";
			if (oldText !== this.siteStatus[i].btn.textContent) {
				changed = true;
			}
		}
		if (changed) {
			this.getFullContextMenuAndUpdate();
		}
	},



	toggleSiteSelect: function(element) {
		this.siteStatus[element.indexReference].selected = !this.siteStatus[element.indexReference].selected;

		if (this.siteStatus[element.indexReference].selected) {
			element.style.background = "lightgreen";
		} else {
			element.style.background = "";
		}
		this.getFullContextMenuAndUpdate();
	},


	shareModeChange: function(mode) {
		if (mode === "none") {
			this.visuals.sm_btn_none.style.background = "lightgreen";
			this.visuals.sm_btn_anyNewApp.style.background = "";
		} else if (mode === "any") {
			this.visuals.sm_btn_none.style.background = "";
			this.visuals.sm_btn_anyNewApp.style.background = "lightgreen";
		}
		this.shareMode = mode;
		this.getFullContextMenuAndUpdate();
	},




	shareAppWithSites: function(appId) {
		// Check if there are any that are selected and connected
		let sites = [];
		for (let i = 0; i < this.siteStatus.length; i++) {
			if (this.siteStatus[i].selected && (this.siteStatus[i].status === "connected")) {
				wsio.emit("callFunctionOnApp", {
					app: appId,
					func: "SAGE2_shareWithSite",
					parameters: {
						remoteSiteIndex: i // assuming the cfg_json aligns correctly
					} 
				});
			}
		}
	},







	// ------------------------------------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------
	// Support functions

	/**
	 * This will load the visual layout from html file included in the folder
	 * Done so one doesn't have to programatically generate layout.
	 *
	 * @method     loadHtmlFromFile
	 * @param      {String}  relativePathFromAppFolder From the containing app folder, path to file
	 * @param      {String}  whereToAppend     Node who's innerHTML will be set to content
	 * @param      {String}  callback     What function to call after getting the file
	 */
	loadHtmlFromFile: function(relativePathFromAppFolder, whereToAppend, callback) {
		var _this = this;
		readFile(relativePathFromAppFolder, function(err, data) {
			_this.loadIntoAppendLocation(whereToAppend, data);
			callback();
		}, 'TEXT');
	},

	/**
	 * Called after xhr gets html content
	 * Main thing to note is that id fields are altered to be prefixed with SAGE2 assigned id
	 *
	 * @method     loadIntoAppendLocation
	 * @param      {String}  whereToAppend Node who's innerHTML will be set to content
	 * @param      {String}  responseText     Content of the file
	 */
	loadIntoAppendLocation: function(whereToAppend, responseText) {
		var content = "";
		// id and spaces because people aren't always consistent
		var idIndex;

		// find location of first id div. Because there will potentially be multiple apps.
		idIndex = this.findNextIdInHtml(responseText);

		// for each id, prefix it with this.id
		while (idIndex !== -1) {
			// based on id location move it over
			content += responseText.substring(0, idIndex);
			responseText = responseText.substring(idIndex);
			// collect up to the first double quote. design.html has double quotes, but HTML doesn't require.
			content += responseText.substring(0, responseText.indexOf('"') + 1);
			responseText = responseText.substring(responseText.indexOf('"') + 1);
			// apply id prefix
			content += this.id;
			// collect rest of id
			content += responseText.substring(0, responseText.indexOf('"') + 1);
			responseText = responseText.substring(responseText.indexOf('"') + 1);

			// find location of first id div. Because there will potentially be multiple apps.
			idIndex = this.findNextIdInHtml(responseText);
		}
		content += responseText;
		whereToAppend.innerHTML = content;
	},

	/**
	 * This returns the index of the first location of id
	 * Accounts for 0 to 3 spaces between id and =
	 *
	 * @method     findNextIdInHtml
	 */
	findNextIdInHtml: function(responseText) {
		// find location of first id div. Because there will potentially be multiple apps.
		// the multiple checks are incase writers are not consistent
		var idIndex = responseText.indexOf("id=");
		var ids1 = responseText.indexOf("id =");
		var ids2 = responseText.indexOf("id  =");
		var ids3 = responseText.indexOf("id   =");
		// if (idIndex isn't found) or (is found but ids1 also found and smaller than idIndex)
		if ((idIndex === -1) || (ids1 > -1 && ids1 < idIndex)) {
			idIndex = ids1;
		}
		if ((idIndex === -1) || (ids2 > -1 && ids2 < idIndex)) {
			idIndex = ids2;
		}
		if ((idIndex === -1) || (ids3 > -1 && ids3 < idIndex)) {
			idIndex = ids3;
		}
		return idIndex;
	},

});
