//
// SAGE2 application: dash
// by: Luc Renambot <renambot@gmail.com>
//
// Copyright (c) 2015
//

"use strict";

/* global dashjs */

var dash = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous"; // onfinish
		// this.moveEvents   = "continuous";

		// SAGE2 Application Settings
		//
		this.currentTime  = null;
		this.countClients = 0;

		this.player   = null;
		this.duration = null;
		this.ready    = false;
		this.domVideo = null;
		this.syncTime = 0;

		var _this = this;
		var url = this.resrcPath + "videos/output_dash.mpd";
		this.domVideo = document.createElement('video');
		this.element.appendChild(this.domVideo);
		this.domVideo.controls = false;
		this.domVideo.loop     = true;

		this.player = dashjs.MediaPlayer().create();
		this.player.getDebug().setLogToBrowserConsole(false);
		this.player.initialize(this.domVideo, url, false);

		this.player.on('canPlay', function() {
			console.log('canPlay');
			_this.player.setMute(true);
			if (!_this.ready) {
				_this.broadcast("dashRegister", {id: clientID});
			}
		});

		this.player.on('playbackMetaDataLoaded', function(evt) {
			if (isMaster) {
				_this.sendResize(_this.domVideo.videoWidth, _this.domVideo.videoHeight);
			}
		});
		this.player.on('bufferstalled', function() {
			console.log('bufferstalled');
		});
		this.player.on('error', function() {
			console.log('error');
		});
		this.player.on('playbackEnded', function() {
			console.log('playbackEnded');
			_this.broadcast("dashPlay", 0);
		});
		this.player.on('playbackPlaying', function() {
			console.log('playbackPlaying');
		});
		this.player.on('playbackSeeked', function() {
			console.log('playbackSeeked');
		});
		this.player.on('playbackSeeking', function() {
			console.log('playbackSeeking');
		});
		this.player.on('playbackTimeUpdated', function(evt) {
			if (isMaster) {
				_this.currentTime = evt.time;
				// connection.send(JSON.stringify({func: 'time', time: _this.currentTime}));
				_this.broadcast('dashTime', _this.currentTime);
			}
		});
		this.player.on('streaminitialized', function(evt) {
			_this.duration = evt.streamInfo.duration;
			console.log('streaminitialized', _this.duration);
		});

		this.player.on('playbackPaused', function() {
			console.log('playbackPaused');
			if (isMaster) {
				var localTime = _this.player.time();
				_this.broadcast('dashPause', localTime);
				_this.syncTime = localTime;
			}
		});
		this.player.on('playbackStarted', function() {
			console.log('playbackStarted');
			if (isMaster) {
				var localTime = _this.player.time();
				_this.broadcast('dashPlay', localTime);
				_this.syncTime = localTime;
			}
		});


		// Control the frame rate for an animation application
		this.maxFPS = 30.0;
		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;
	},

	dashRegister: function (data) {
		if (isMaster) {
			this.countClients++;
			console.log('Client ready', data.id, this.countClients, this.config.displays.length);
			if (this.countClients >= this.config.displays.length) {
				console.log('Ready');
				this.broadcast("dashPlay", 0);
			}
		}
	},
	dashPlay: function(data) {
		this.ready = true;
		this.player.seek(data);
		this.player.play();
	},
	dashPlause: function(data) {
		this.ready = true;
		this.player.pause();
		this.player.seek(data);
	},
	dashTime: function(data) {
		var curr = data;
		if ((curr - this.syncTime) > 3.0) {
			var local = this.player.time();
			var diff  = Math.abs(local - curr);
			// console.log('Time', curr, local, diff)
			if (diff > (1.0 / 24.0)) {
				console.log('Seek', diff);
				this.player.seek(curr + diff);
			} else {
				console.log('NO Seek', diff);
			}
			this.syncTime = curr;
		}
	},



	load: function(date) {
		this.refresh(date);
	},

	draw: function(date) {
		// console.log('dash> Draw');
	},

	resize: function(date) {
		// Called when window is resized
		this.domVideo.width  = this.sage2_width;
		this.domVideo.height = this.sage2_height;
		this.refresh(date);
	},

	move: function(date) {
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		} else if (eventType === "pointerMove" && this.dragging) {
			// move
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// click release
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// widget events
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") {
				// left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.refresh(date);
			}
		}
	}
});
