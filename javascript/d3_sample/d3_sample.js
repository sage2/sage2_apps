// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-16

//
// From D3.js
// Voronoi Tessellation
// https://bl.ocks.org/mbostock/4060366
//

/* global d3 */
"use strict";

//
// SAGE2 app
//
var d3_sample = SAGE2_App.extend({
	init: function(data) {
		this.SAGE2Init("div", data);

		this.element.id = "div" + data.id;

		this.resizeEvents = "continuous"; // "onfinish";

		// from voronoi example
		this.vertices = d3.range(100).map(function(d) {
			return [Math.random() * data.width, Math.random() * data.height];
		});
		this.voronoi = d3.voronoi().extent([[0, 0], [data.width, data.height]]);

		// save the view box (coordinate system)
		this.box = [data.width, data.height];

		// attach the SVG into the this.element node provided to us
		var box = "0,0," + this.box[0] + "," + this.box[1];
		this.svg = d3.select(this.element).append("svg")
			.attr("width",   data.width)
			.attr("height",  data.height)
			.attr("viewBox", box);

		// backup of the context
		// var _this = this;

		// Pass SAGE2 events into the DOM
		// this.passSAGE2PointerAsMouseEvents = true;
		// this.svg.on("mousemove", function() {
		// 	_this.vertices[0] = d3.mouse(this);
		// 	_this.draw_d3();
		// });

		this.polygon = this.svg.append("g")
			.attr("class", "polygons")
			.selectAll("path")
			.data(this.voronoi.polygons(this.vertices))
			.enter().append("path")
			.call(redrawPolygon);

		this.link = this.svg.append("g")
			.attr("class", "links")
			.selectAll("line")
			.data(this.voronoi.links(this.vertices))
			.enter().append("line")
			.call(redrawLink);

		this.site = this.svg.append("g")
			.attr("class", "sites")
			.selectAll("circle")
			.data(this.vertices)
			.enter().append("circle")
			.attr("r", 2.5)
			.call(redrawSite);

		this.draw_d3(data.date);

		this.controls.finishedAddingControls();
	},

	load: function(date) {
	},

	draw_d3: function(date) {
		var diagram = this.voronoi(this.vertices);
		this.polygon = this.polygon.data(diagram.polygons()).call(redrawPolygon);
		this.link = this.link.data(diagram.links()), this.link.exit().remove();
		this.link = this.link.enter().append("line").merge(this.link).call(redrawLink);
		this.site = this.site.data(this.vertices).call(redrawSite);
	},

	draw: function(date) {
	},

	resize: function(date) {
		this.svg.attr('width',  this.element.clientWidth);
		this.svg.attr('height', this.element.clientHeight);
		this.refresh(date);
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// press
		}
		if (eventType === "pointerMove") {
			// scale the coordinate by the viewbox (coordinate system)
			var scalex = this.box[0] / this.element.clientWidth;
			var scaley = this.box[1] / this.element.clientHeight;
			this.vertices[0] = [position.x * scalex, position.y * scaley];
			// update svg
			this.draw_d3();
		}
		if (eventType === "pointerRelease" && (data.button === "left")) {
			// release
		}
	}

});

//
// utility functions to draw primitives
//
function redrawPolygon(poly) {
	poly.attr("d", function(d) {
		return d ? "M" + d.join("L") + "Z" : null;
	}).attr("class", function(d, i) {
		return "q" + (i % 9) + "-9";
	});
}

function redrawLink(ln) {
	ln.attr("x1", function(d) {
		return d.source[0];
	}).attr("y1", function(d) {
		return d.source[1];
	}).attr("x2", function(d) {
		return d.target[0];
	}).attr("y2", function(d) {
		return d.target[1];
	});
}

function redrawSite(st) {
	st.attr("cx", function(d) {
		return d[0];
	}).attr("cy", function(d) {
		return d[1];
	});
}
