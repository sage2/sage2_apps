// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// Copyright (c) 2019



// The var name MUST match the name of the script file
// All applications must extend SAGE2_App
var helloWorld = SAGE2_App.extend({

	// The init function will be called during application creation
		// The signature must include the data parameter which will be passed to the parent through SAGE2Init()
		init: function(data) {
	
			// Create the application with a div node
			this.SAGE2Init("div", data);
	
			// Assign the div node an id.
			// `this.id` is a variable available to this app as part of creation which uniquely identifies which application it is.
			this.element.id = "div" + this.id;
	
			// Fill the element with text "Hello World"
			this.element.textContent = "Hello World";
	
			// Make the background white to ensure text visibility on SAGE2 sites with dark backgrounds.
			this.element.style.background = "white";
		},
	});
