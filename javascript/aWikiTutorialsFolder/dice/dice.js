// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// Copyright (c) 2019

var dice = SAGE2_App.extend({
	init: function (data) {
		// Create a canvas node for this app. Accessible later as this.element
		this.SAGE2Init("canvas", data);
		// As the window is resized, the resize function will be called automatically
		this.resizeEvents = "continuous";

		// Setup variables for usage later

		// Get the canvas context
		this.ctx = this.element.getContext("2d");
		// Setup spacing variables used for drawing
		this.itemSize = this.element.width / 8;
		this.itemGap = this.itemSize / 2;
		this.shouldRedraw = true;

		// Setup the dice 
		this.numberOfDice = 5;
		// Make an array to hold the dice values
		this.diceValues = Array(this.numberOfDice).fill(1);
		// Roll all of the dice values
		this.roll();
	},

	/*
		When draw is automatically called, the data parameter will be the server time it was called.
	*/
	draw: function (date) {
		// Save some time by only redrawing if the values change
		if (this.shouldRedraw) {
			// Draw a white background
			this.ctx.fillStyle = "rgb(255, 255, 255)";
			this.ctx.fillRect(0, 0, this.element.width, this.element.height);
			// Now draw black dice ...
			this.ctx.fillStyle = "black";
			for (i = 0; i < this.numberOfDice; i++) {
				// ... using the helper function
				this.drawDie(this.ctx, (i + 1) * this.itemGap + i * this.itemSize,
					this.itemSize, this.itemSize, this.diceValues[i], this.state.color);
			}
			this.shouldRedraw = false;
		}
	},

	/*
		After a resize, calculate the size of the dice and spacing between them.
		This function is automatically called because instructions.json has
		   "resize": "proportional"
	*/
	resize: function (date) {
		// Recalculate these variables to keep the dice scaled with the view
		this.itemSize = this.element.width / 8;
		this.itemGap = this.itemSize / 2;
		this.shouldRedraw = true;
		// Trigger the draw using a SAGE2 helper function passing the date param, which was the time when the resize() function was called.
		this.refresh(date);
	},

	/*
		The parameters each describe a single event. When multiple users are interacting with the app at the same time, this function will be called once per person.
			- type, a string describing the type of event. For this app we are looking for pointerRelease events.
			- position, an object containing x and y properties of the pointer location which triggered the event.
			- user, an object containing: id, label, and color properties which describe the user which triggered the event.
			- data, an object containing extra information usually for keyboard events. This app will not need to use it.
			- date, the time which the server detected the event.
		For more information please see, [SAGE2 Application API](https://bitbucket.org/sage2/sage2/wiki/SAGE2%20Application%20API)
	*/
	event: function (type, position, user, data, date) {
		// If the event was a pointer (mouse) button release.
		if (type == "pointerRelease") {
			// For each of the dice
			for (i = 0; i < this.numberOfDice; i++) {
				// Check if the pointer release was within the bounds of a die
				if (this.didUserClickInSpecificDie(position, {
					x: (i + 1) * this.itemGap + i * this.itemSize,
					y: this.itemSize,
					w: this.itemSize,
					h: this.itemSize})
				) {
					// If so, reroll only that die's values
					this.roll(i);
				}
			}
			this.shouldRedraw = true;
			// Trigger a draw
			this.refresh(date);
		}
	},



	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	// Support functions

	/*
		Will assign a new value to the die.
		The param determines which die to reroll
		If -1, then reroll all dice.
	*/
	roll: function (diceToRoll = -1) {
		// If -1, roll all dice
		if (diceToRoll === -1) {
			for (i = 0; i < this.numberOfDice; i++) {
				// Give each of the dice a random number between 1 and 6
				this.diceValues[i] = Math.floor(Math.random() * 6) + 1;
			}
		} else if (diceToRoll < this.numberOfDice)  {
			// Only the specified die gets a random 1 to 6 value
			this.diceValues[diceToRoll] = Math.floor(Math.random() * 6) + 1;
		}
		// A value has changed, time to redraw.
		this.shouldRedraw = true;
	},

	/*
		Draw one dice with the given parameter properties.
			ctx: the canvas drawing context
			x, y: top left location
			size: 
	*/
	drawDie: function (ctx, x, y, size, value, color) {
		// Save the current context state
		ctx.save();
		// Draw the outline of the die.
		ctx.strokeRect(x, y, size, size);
		// Setup drawing vars for the dots
		var padding = 0.25;
		ctx.fillStyle = color;

		// Draw top-left dot
		if (value == 2 || value == 3 || value == 4 || value == 5 || value == 6) {
			dotx = padding * size;
			doty = padding * size;
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		// Draw middle-left dot
		if (value == 6) {
			dotx = padding * size;
			doty = size * 0.5;
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		// Draw bottom-left dot
		if (value == 4 || value == 5 || value == 6) {
			dotx = padding * size;
			doty = size * (1 - padding);
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		// Draw center dot
		if (value == 1 || value == 3 || value == 5) {
			dotx = size * 0.5;
			doty = size * 0.5;
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		// Draw top-right dot
		if (value == 4 || value == 5 || value == 6) {
			dotx = size * (1 - padding);
			doty = padding * size;
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		// Draw middle-right dot
		if (value == 6) {
			dotx = size * (1 - padding);
			doty = size * 0.5;
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		// Draw bottom-right dot
		if (value == 2 || value == 3 || value == 4 || value == 5 || value == 6) {
			dotx = size * (1 - padding);
			doty = size * (1 - padding);
			ctx.beginPath();
			ctx.arc(x + dotx, y + doty, size * 0.07, 0, 2 * Math.PI);
			ctx.fill();
		}
		ctx.restore();
	},

	/*
		Check if click position within dieBounds.
			Return true if so. Otherwise false.
	*/
	didUserClickInSpecificDie: function(releasePosition, dieBounds) {
		let cx = releasePosition.x;
		let cy = releasePosition.y;
		// See if the position was within the bounds.
		if((cx > dieBounds.x)
			&& (cx < (dieBounds.x + dieBounds.w))
			&& (cy > dieBounds.y)
			&& (cy < (dieBounds.y + dieBounds.h))
		){
			return true;
		}
		return false;
	},
	
});
