// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// Copyright (c) 2019



// The var name MUST match the name of the script file
// All applications must extend SAGE2_App
var timer = SAGE2_App.extend({
	init: function(data) {
		// Initialize the app using the parent construction which performs a variety of background tasks. The first pararameter specifies which html node to generate for this app to use, later accessible through this.element.
		this.SAGE2Init("div", data);
		// Give it an id
		this.element.id = "div" + this.id;
		// Set the starting text
		this.element.innerHTML = "Timer<br>00:00:00";
		// Make the background white to ensure text visibility
		this.element.style.background = "white";

		// Create some tracking vars used to display information
		this.message = "Timer";
		this.active = false;
		this.timeCounter = 0;
	},

	/*
	By having the instructions.json have an entry for
	   "animation": true,
	This function will attempt to be called at a steady interval with a maximum
	   of 60 calls per second by default.
		 
	However, being a timer, the exact amount of time between calls is necessary to be useful.		
		To do this the dt variable inherited from SAGE2_App is used.
			dt is the time since the draw function was last called automatically

	*/
	draw: function (date) {
		// Only if the timer is active, increase the timeCounter
		if (this.active) {
			// Delta time (dt) since the last time draw() was called automatically by SAGE2
			// dt is given in seconds.
			this.timeCounter += this.dt;
		}
		let minutes = Math.floor(this.timeCounter / 60);
		let seconds = Math.floor(this.timeCounter) - (minutes * 60);
		let tenths  = Math.floor(this.timeCounter * 100) % 100;
		// Formatting. Prefix with 0 to ensure there are always 2 digits
		minutes     = (minutes < 10) ? "0" + minutes : "" + minutes;
		seconds     = (seconds < 10) ? "0" + seconds : "" + seconds;
		tenths      = (tenths < 10)  ? "0" + tenths  : "" + tenths;

		// Show the information by directly manipulating the node
		this.element.innerHTML = this.message + "<br>"
			+ minutes + ":" + seconds + ":" + tenths;
	},

	// ------------------------------------------------------------------------------------------------------------------------
	/*
	* To enable right click context menu support this function needs to be present.
	*
	* Must return an array of entries. An entry is an object with at least three properties:
	*   description: what is to be displayed to the viewer.
	*   callback: String containing the name of the function to activate in the app. It must exist and match exactly.
	*   parameters: an object with specified datafields to be given to the function.
	*		The following attributes will be automatically added by server to parameters
	*			serverDate, on the return back, server will fill this with time object.
	*			clientId, unique identifier (ip and port) for the client that selected entry.
	*			clientName, the name input for their pointer. Note: users are not required to do so.
	*			clientInput, if entry is marked as input, the value will be in this property. See pdf_viewer.js for example.
	*     If any of these are specified within parameters, the server will end up overriding the value.
	*
	*/
	getContextEntries: function() {
		var entries = [];
		// Note there is an if statement here that either shows the Start or Pause action.
		// Which one shown is based on the active property of this app
		if (!this.active) {
			entries.push({
				description: "Start",
				// Coloring is optional and has not functional effect on the entry
				entryColor: "lightgreen",
				// The callback must match exactly with an existing function on the app.
				// See below for the definition of interactWithTimer
				callback: "interactWithTimer",
				// Parameters will be given to the function if declared
				parameters: {
					action: "start"
				}
			});
		} else {
			entries.push({
				description: "Pause",
				callback: "interactWithTimer",
				parameters: {
					action: "pause"
				}
			});
		}
		// Separators will add a visual horizontal line between the entries.
		entries.push({ description: "separator"});
		entries.push({
			description: "Reset",
			entryColor: "lightsalmon",
			callback: "interactWithTimer",
			parameters: {
				action: "reset"
			}
		});
		entries.push({ description: "separator"});
		entries.push({
			description: "Set Message:",
			callback: "setMessage",
			parameters: { },
			inputField: true,
			inputFieldSize: 20,
		});

		return entries;
	},

	/*
	* This function is designed work the the context menu entries.
	* Functions activated by the context menu will receive a responseObject as the parameter.
	*/
	interactWithTimer: function(responseObject) {
		if (responseObject.action === "start") {
			this.active = true;
		} else if (responseObject.action === "pause") {
			this.active = false;
		} else if (responseObject.action === "reset") {
			this.active = false;
			this.timeCounter = 0;
		}

		// This will re-evaluate the context menu entries.
		// In particular whether or not it should show Start or Pause.
		this.getFullContextMenuAndUpdate();
	},

	/*
	* The setMessage function will get the responseObject as parameter which contains the
	*    user input as property clientInput.
  *    The clientInput property will always be a string, even if the user types a number.
	*    You may need to parseInt() or parseFloat() if you are expecting a number.
	*/
	setMessage: function(responseObject) {
		this.message = responseObject.clientInput;
		this.getFullContextMenuAndUpdate();
	},
});