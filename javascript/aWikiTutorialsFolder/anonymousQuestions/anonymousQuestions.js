// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// Copyright (c) 2019

var anonymousQuestions = SAGE2_App.extend({
	// Inherited function from SAGE2_App, entry point into the app.
	init: function (data) {
		// Required, activate parent constructor.
		// Makes a div node for this.element
		this.SAGE2Init("div", data);
		
		// Set the background to white to ensure text visibility.
		this.element.style.background = "white";
		/*
			No need to add variables since the instructions.json file will add all values
				in the load property into this app's state.
			As of now, the state variable will have properties:
				questionList
				questionUpvotes
				displayMode
			They can be accessed with the following lines respectively:
				this.state.questionList
				this.state.questionUpvotes
				this.state.displayMode
		*/
	},

	// Whenever the app state is updated, also update the visuals.
	load: function() {
		this.draw();
	},

	// Inherited function, overriding and being used to display visuals based on this.state.displayMode
	draw: function(date) {
		// Start by replacing this.element contents with title
		this.element.innerHTML = "<h1>Questions:</h1><hr>";
		// Depending on the display mode, the different logic for visuals is called.
		switch(this.state.displayMode) {
			case "list":
				this.showQuestionsInList();
				break;
			case "upvoteDescending":
				this.showQuestionsOrderedByUpvotes();
				break;
			case "upvotesIncreaseTextSize":
				this.showQuestionsInList(true);
				break;
			default:
				break;
		}
		// Update the UI menu to allow for upvoting
		this.getFullContextMenuAndUpdate();
	},

	/*
		Will show the questions in submission order.
		If the parameter increaseSizeByUpvote is true, will also increase the text size.
		By default, increaseSizeByUpvote will be false.
	*/
	showQuestionsInList: function(increaseSizeByUpvote = false) {
		let div;
		// For each question submitted
		for (let i = 0; i < this.state.questionList.length; i++) {
			// Make a div for it
			div = document.createElement("div");
			// Increase the size if in upvotesIncreaseTextSize mode
			if (increaseSizeByUpvote) {
				div.style.fontSize = (ui.titleTextSize + this.state.questionUpvotes[i]) + "px";
			} else {
				// Otherwise keep it the same size as app title font size
				div.style.fontSize = ui.titleTextSize + "px";
			}
			// Put the question in the div
			div.textContent = (i + 1) + ". (" + (this.state.questionUpvotes[i] + 1)
				+ ") " + this.state.questionList[i];
			// Add the div so it will be visible
			this.element.appendChild(div);
		}
	},

	// Will show the questions in upvote descending order.
	showQuestionsOrderedByUpvotes: function() {
		let div;
		// Combine the two state array values (questionList and questionUpvotes) into one array by making them object properties.
		let questions = this.state.questionList.map((element, index) => {
			return { "question": element, "upvotes": this.state.questionUpvotes[index] };
		});
		// Sort them based on the upvotes (higher upvote gets lower index)
		questions = questions.sort((a, b) => {
			if (a.upvotes > b.upvotes) { return -1; }
			return 1;
		});
		// Put each question into a div and add to this.element to make visible
		for (let i = 0; i < questions.length; i++) {
			div = document.createElement("div");
			div.style.fontSize = ui.titleTextSize + "px";
			div.textContent = (i + 1) + ". (" + (questions[i].upvotes + 1)
				+ ") " + questions[i].question;
			this.element.appendChild(div);
		}
	},

	// Describes entry generation for the UI context menu (the menu that shows up when right clicking on an app).
	getContextEntries: function() {
		// Must return an array, even if empty.
		var entries = [];
		
		// Allow users to submit a question
		entries.push({
			// What the user will see for this entry
			description: "Add question:",
			// Which function in this app should handle the interaction
			callback: "addQuestion",
			// No custom parameters need to be given to the function
			parameters: {},
			// Turn this entry into an input field, rather than a button
			inputField: true,
			inputFieldSize: 20,
		});

		// Add some visual separators
		entries.push({description: "separator"});
		entries.push({description: "separator"});

		// This shows how to make a pop out selection on the context menu
		entry = {};
		// The top level cannot be interacted with, but when hovered over...
		entry.description = "Select Display Mode";
		// ... will show the children entries, which can be interacted with.
		entry.children = [
			{
				// What the user will see for this child
				description: "List questions in order",
				// Which function in this app will handle the interaction
				callback: "changeDisplayMode",
				// Add a custom parameter, mode with value "list" that will be passed to the function
				parameters: { mode: "list" }
			},
			{
				description: "Show questions in descending order of upvotes",
				callback: "changeDisplayMode",
				parameters: { mode: "upvoteDescending" }
			},
			{
				description: "Upvotes increase text size of questions",
				callback: "changeDisplayMode",
				parameters: { mode: "upvotesIncreaseTextSize" }
			},
		];
		// Don't forget to add it
		entries.push(entry);

		// Add separators only if there are questions
		if (this.state.questionList.length > 0) {
			entries.push({description: "separator"});
			entries.push({description: "separator"});
		}

		// Create entries to allow a user to upvote a question
		for (let i = 0; i < this.state.questionList.length; i++) {
			entries.push({
				description: "Upvote: " + this.state.questionList[i],
				callback: "upvoteQuestion",
				parameters: { index: i},
			});
		}

		return entries;
	},

	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	// Draw Support Functions


	// ---------------------------------------------------------------------------
	// ---------------------------------------------------------------------------
	// Handlers for context menu

	// Handle interaction from usage of context menu "Add question" entry.
	addQuestion: function(responseObject) {
		// Add the question to the state list
		// The clientInput property is added for inputField entries.
		this.state.questionList.push(responseObject.clientInput);
		// Add a corresponding 0 for upvote counts.
		this.state.questionUpvotes.push(0);
		// Update the visuals
		this.draw();
		// Synchronize the state variable.
		// The parameter true, means push this to remote sites if any are connected.
		this.SAGE2Sync(true);
	},

	// Handle interaction from usage of any context menu "Upvote" entry.
	upvoteQuestion: function(responseObject) {
		/*
			Increase the corresponding upvote entry
			The index property was a custom parameter defined when making the context menu entries

			entries.push({
				description: "Upvote: " + this.state.questionList[i],
				callback: "upvoteQuestion",
				parameters: { index: i},
			});
		*/
		this.state.questionUpvotes[responseObject.index]++;
		// Update visuals
		this.draw();
		// Synchronize the state variable
		this.SAGE2Sync(true);
	},

	// Handle interaction from usage of any context menu changeDisplayMode entry.
	changeDisplayMode: function (responseObject) {
		/*
			The mode property is a custom parameter added during context menu creation
			...
			entry.children = [
				{
					description: "List questions in order",
					callback: "changeDisplayMode",
					parameters: { mode: "list" }
				},
				{
					description: "Show questions in descending order of upvotes",
					callback: "changeDisplayMode",
					parameters: { mode: "upvoteDescending" }
				},
				{
					description: "Upvotes increase text size of questions",
					callback: "changeDisplayMode",
					parameters: { mode: "upvotesIncreaseTextSize" }
				},
			];

		*/
		this.state.displayMode = responseObject.mode;
		// Update visuals
		this.draw();
		// Synchronize the state variable
		this.SAGE2Sync(true);
	},

	
});
