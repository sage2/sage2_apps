
var maybeSyncBug = SAGE2_App.extend({
	init: function (data) {
		this.SAGE2Init("div", data);
		this.element.style.background = "white";

		this.showEveryone = document.createElement("div");
		this.showMaster = document.createElement("div");

		this.element.appendChild(this.showEveryone);
		this.element.appendChild(this.showMaster);
	},
	draw: function() {},
	load: function() {
		this.showValues();
	},
	
	getContextEntries: function() {
		// Must return an array, even if empty.
		var entries = [];
		entries.push({
			description: "Everyone Show Time",
			callback: "everyoneShowTime",
			parameters: {},
		});
		entries.push({
			description: "Time based on Master Display",
			callback: "showTimeBasedOnMaster",
			parameters: {},
		});
		return entries;
	},

	everyoneShowTime: function() {
		this.state.everyoneSet = Date.now();
		this.showValues();
	},

	showTimeBasedOnMaster: function() {
		this.state.masterSet = Date.now();
		// Only master should send the sync packet
		this.SAGE2Sync(true); // Both true and false have no effect
	},

	showValues: function(localInfo) {
		this.showEveryone.textContent = "Everyone:" + this.state.everyoneSet;
		this.showMaster.textContent = "Master:" + this.state.masterSet;
	},

});