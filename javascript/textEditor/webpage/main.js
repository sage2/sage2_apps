// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-17

"use strict";

/* global  SAGE2Connection*/

/*
File has parts:
	1. Any actions needed to run before connecting to server.
	2. Connect to server
	3. Functions that the app expects to call
	4. Functions for usage in the page

*/

/* ------------------------------------------------------------------------------------------------------------------
// 1
// ------------------------------------------------------------------------------------------------------------------
Finalize the page
*/


var lastEditorText = null;
var editor = ace.edit("editor");
var detectedFileName = "script.js";
var startingText =
`
Loading...
`;
var session = new ace.EditSession(startingText);
editor.setSession(session);
editor.setTheme("ace/theme/monokai");
editor.session.setMode("ace/mode/javascript");
var lastTextDeltaReceived = null;
var lastTextDeltaSent = null;
var textDeltaIgnoreCount = 0; // necessary when getting initial condition of the text

console.log(editor.getValue());

// setInterval(checkIfNeedToSendServerText, 10);

// registering change callback
editor.on("change", function(delta) {
	console.log("change: ", delta);
	// Strings are comparable
	delta = JSON.stringify(delta);
	if (textDeltaIgnoreCount > 0) {
		textDeltaIgnoreCount--;
		return;
	}
	// This is a string comparison
	if (lastTextDeltaReceived === delta) {
		lastTextDeltaReceived = null;
		return;
	}
	if (SAGE2Connection.isConnected) {
		sendServerTextDelta(delta);
	}
}, false );

button_download.addEventListener("click", downloadTextAsFile);

addListenerToInterceptCtrlS();


/* ------------------------------------------------------------------------------------------------------------------
// 2
// ------------------------------------------------------------------------------------------------------------------
Connect to the server
*/

// first describe any reaction after connecting
SAGE2Connection.afterSAGE2Connection = addThisClientAsEditor;

// This this is part of app code, it will use the current window values to connect.
// But if the page was hosted elsewhere, parameters would be required.
SAGE2Connection.initS2Connection();

/* ------------------------------------------------------------------------------------------------------------------
// 3
// ------------------------------------------------------------------------------------------------------------------
The following functions are for communication handling.
*/

// After establishing connection, call this function to let app know this page is acting as editor.
function addThisClientAsEditor() {
	// callFunctionOnApp: function(functionName, parameterObject) { // autofilled id by server
	SAGE2Connection.callFunctionOnApp("addClientIdAsEditor", {});
}


// Has the initial state on connection
// HAVE to use this name for compliance with UI version
function setTextInEditor(data) {
	// When setting text, this causes two deltas. Full delete, then the paste as an addition.
	// Ignore the erase and paste
	textDeltaIgnoreCount = 2;
	editor.setValue(data.currentText);
	lastEditorText = data.currentText;
	editor.clearSelection();
	if (data.title) {
		detectedFileName = data.title;
	}
}
function editorTextDelta(data) {
	let delta = JSON.parse(data.delta);
	lastTextDeltaReceived = data.delta;
	editor.getSession().getDocument().applyDeltas([delta]); // NEEDS array
}
// To server
function sendServerThisEditorText() {
	let currentEditorText = editor.getValue();
	lastEditorText = currentEditorText;
	// Func name, param object
	SAGE2Connection.callFunctionOnApp("handleTextUpdateFromClient", {editorText: currentEditorText});
}

function sendServerTextDelta(delta) {
	lastTextDeltaSent = delta;
	SAGE2Connection.callFunctionOnApp("handleTextDeltaFromClient", {"delta": delta});
}




/* ------------------------------------------------------------------------------------------------------------------
// 4
// ------------------------------------------------------------------------------------------------------------------
Functions for usage in the page
*/

function checkIfNeedToSendServerText() {
	if (lastEditorText == null) {
		return;
	} else if (lastEditorText != editor.getValue()) {
		console.log("Sending diff");
		sendServerThisEditorText();
	}
}

function downloadTextAsFile() {
	var link = document.createElement("a");
	let contents = editor.getValue();
	link.href = "data:text/plain;charset=utf-8," + encodeURIComponent(contents);
	link.download = detectedFileName;
	// link.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
	// link.setAttribute("download", filename);
	link.style.display = "none";
	document.body.appendChild(link);
	link.click();
	document.body.removeChild(link);
}

function addListenerToInterceptCtrlS() {
	document.addEventListener("keydown", function(e) {
		if (
			(e.ctrlKey || e.metaKey)
			&& (e.key.toLowerCase() == "s") 
		){
			e.preventDefault();
			downloadTextAsFile();
		}
	})
}
