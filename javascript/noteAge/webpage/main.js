// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-17

"use strict";

/* global  SAGE2Connection*/

/*
File has parts:
	1. Any actions needed to run before connecting to server.
	2. Connect to server
	3. Functions that the app expects to call
	4. Functions for usage in the page

*/

/* ------------------------------------------------------------------------------------------------------------------
// 1
// ------------------------------------------------------------------------------------------------------------------
Finalize the page
*/



let entryReferences = [];

addInitialEntriesToEntryReferences();
addPageButtonHandlers();




/* ------------------------------------------------------------------------------------------------------------------
// 2
// ------------------------------------------------------------------------------------------------------------------
Connect to the server
*/

// first describe any reaction after connecting
SAGE2Connection.afterSAGE2Connection = toS2_addThisClientAsEditor;

// This this is part of app code, it will use the current window values to connect.
// But if the page was hosted elsewhere, parameters would be required.
SAGE2Connection.initS2Connection();

/* ------------------------------------------------------------------------------------------------------------------
// 3
// ------------------------------------------------------------------------------------------------------------------
The following functions are for communication handling.
*/

// After establishing connection, call this function to let app know this page is acting as editor.
function toS2_addThisClientAsEditor() {
	// callFunctionOnApp: function(functionName, parameterObject) { // autofilled id by server
	SAGE2Connection.callFunctionOnApp("fromControls_addClientIdAsEditor", {});
}

function fromS2_timeGroups(data) {
	// match up entry count
	while (entryReferences.length < data.timeGroups.length) {
		addTimeEntry();
	}
	while (entryReferences.length > data.timeGroups.length) {
		entryReferences[entryReferences.length - 1].removeButton.onclick();
	}
	// fill in entries
	let entry;
	for (let i = 0; i < data.timeGroups.length; i++) {
		if (i > 0) entryReferences[i].input.t.value = data.timeGroups[i].time;
		entryReferences[i].input.r.value = data.timeGroups[i].color[0];
		entryReferences[i].input.g.value = data.timeGroups[i].color[1];
		entryReferences[i].input.b.value = data.timeGroups[i].color[2];
	}
}

function toS2_uploadNewTimeGroups() {
	let timeGroups = getTimeGroupsIfValid();
	if (timeGroups && window.confirm("Are you use you want to upload?")) {
		SAGE2Connection.callFunctionOnApp("fromControl_updateTimeGroups", {timeGroups});
	}
}

function getTimeGroupsIfValid() {
	let timeGroups = [];
	let entry, prevEntry;
	for (let i = 0; i < entryReferences.length; i++) {
		entry = {
			time: parseInt(entryReferences[i].input.t.value),
			color: [
				parseInt(entryReferences[i].input.r.value),
				parseInt(entryReferences[i].input.g.value),
				parseInt(entryReferences[i].input.b.value),
			]
		};
		if (!isEntryValid(entry, prevEntry)) {
			alert("Entry " + (i + 1) + " is invalid");
			return null;
		}
		prevEntry = entry;
		timeGroups.push(entry);
	}
	return timeGroups;
}

function isEntryValid(entry, prevEntry) {
	for (let i = 0; i < entry.color.length; i++) {
		if (isNaN(entry.color[i])) return false;
		if (entry.color[i] < 0) return false;
		if (entry.color[i] > 255) return false;
	}
	if (isNaN(entry.time)) return false;
	if (prevEntry) {
		if (entry.time < prevEntry.time) return false;
	}
	// console.log("erase me, valid entry", entry);
	return true;
}




/* ------------------------------------------------------------------------------------------------------------------
// 4
// ------------------------------------------------------------------------------------------------------------------
Functions for usage in the page
*/

function addInitialEntriesToEntryReferences() {
	let entry = {
		number: 0,
		validColor: false,
		validTime: true,
		td: {
			r: td_e1r,
			g: td_e1g,
			b: td_e1b,
		},
		input: {
			t: {value: 0},
			r: input_e1r,
			g: input_e1g,
			b: input_e1b,
		}
	}

	let keyupColorCheck = function(e) {
		let r = parseInt(entry.input.r.value),
			g = parseInt(entry.input.g.value),
			b = parseInt(entry.input.b.value);
		if ((r != NaN) && (g != NaN) && (b != NaN)
			&& (r >= 0) && (g >= 0) && (b >= 0)
			&& (r <= 255) && (g <= 255) && (b <= 255)) {
				entry.td.r.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.td.g.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.td.b.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.input.r.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.input.g.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.input.b.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.validColor = true;
		} else {
			entry.td.r.style.background = "white";
			entry.td.g.style.background = "white";
			entry.td.b.style.background = "white";
			entry.input.r.style.background = "white";
			entry.input.g.style.background = "white";
			entry.input.b.style.background = "white";
			entry.validColor = false;
		}
	} // end keyupColorCheck
	entry.input.r.onkeyup = keyupColorCheck;
	entry.input.g.onkeyup = keyupColorCheck;
	entry.input.b.onkeyup = keyupColorCheck;
	
	let entry2 = {
		number: 0,
		validColor: false,
		validTime: true,
		td: {
			t: td_e2t,
			r: td_e2r,
			g: td_e2g,
			b: td_e2b,
		},
		input: {
			t: input_e2t,
			r: input_e2r,
			g: input_e2g,
			b: input_e2b,
		}
	}
	let keyupTimeCheck = function(e) {
		console.log("erase me, 2nd entry check time");
		let time = parseInt(entry2.input.t.value);
		if ((time != NaN) && (time > 0)) {
			entry2.td.t.style.background = "white";
			entry2.input.t.style.background = "white";
			entry2.validTime = true;
		} else {
			entry2.td.t.style.background = "salmon";
			entry2.input.t.style.background = "salmon";
			entry2.validTime = false;
		}
	} // timeCheck
	let keyupColorCheckForEntry2 = function(e) {
		let r = parseInt(entry2.input.r.value),
			g = parseInt(entry2.input.g.value),
			b = parseInt(entry2.input.b.value);
		if ((r != NaN) && (g != NaN) && (b != NaN)
			&& (r >= 0) && (g >= 0) && (b >= 0)
			&& (r <= 255) && (g <= 255) && (b <= 255)) {
				entry2.td.r.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry2.td.g.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry2.td.b.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry2.input.r.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry2.input.g.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry2.input.b.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry2.validColor = true;
		} else {
			entry2.td.r.style.background = "white";
			entry2.td.g.style.background = "white";
			entry2.td.b.style.background = "white";
			entry2.input.r.style.background = "white";
			entry2.input.g.style.background = "white";
			entry2.input.b.style.background = "white";
			entry2.validColor = false;
		}
	} // end keyupColorCheck
	entry2.input.t.onkeyup = keyupTimeCheck;
	entry2.input.r.onkeyup = keyupColorCheckForEntry2;
	entry2.input.g.onkeyup = keyupColorCheckForEntry2;
	entry2.input.b.onkeyup = keyupColorCheckForEntry2;
	
	entryReferences.push(entry);
	entryReferences.push(entry2);
}

function addPageButtonHandlers() {
	button_addEntry.addEventListener("click", addTimeEntry);
	button_upload.addEventListener("click", toS2_uploadNewTimeGroups);
}

function addTimeEntry() {
	// tr, td x6, input x 4, button
	let entry = {};
	entry.number = entryReferences.length + 1;
	entry.validTime = false;
	entry.validColor = false;
	entry.tr = document.createElement("tr");
	entry.td = {};
	entry.td.e = document.createElement("td");
	entry.td.t = document.createElement("td");
	entry.td.r = document.createElement("td");
	entry.td.g = document.createElement("td");
	entry.td.b = document.createElement("td");
	entry.td.removeButton = document.createElement("td");
	entry.input = {};
	entry.input.t = document.createElement("input");
	entry.input.r = document.createElement("input");
	entry.input.g = document.createElement("input");
	entry.input.b = document.createElement("input");
	entry.removeButton = document.createElement("button");

	// Content and formatting
	// entry.td.e.textContent = entry.number;
	entry.input.t.size = 4;
	entry.input.r.size = 4;
	entry.input.g.size = 4;
	entry.input.b.size = 4;
	entry.removeButton.textContent = "Remove Entry";

	// All to their td
	entry.td.t.appendChild(entry.input.t);
	entry.td.r.appendChild(entry.input.r);
	entry.td.g.appendChild(entry.input.g);
	entry.td.b.appendChild(entry.input.b);
	entry.td.removeButton.appendChild(entry.removeButton);
	// td onto tr
	entry.tr.appendChild(entry.td.e);
	entry.tr.appendChild(entry.td.t);
	entry.tr.appendChild(entry.td.r);
	entry.tr.appendChild(entry.td.g);
	entry.tr.appendChild(entry.td.b);
	entry.tr.appendChild(entry.td.removeButton);
	// tr to body
	tbody_entries.appendChild(entry.tr);

	// functionality
	let keyupTimeCheck = function(e) {
		let time = parseInt(entry.input.t.value);
		let prevEntryTime = parseInt(entryReferences[entryReferences.indexOf(entry) - 1].input.t.value);
		if ((time != NaN)
			&& (prevEntryTime != NaN)
			&& (time > prevEntryTime)) {
				entry.td.t.style.background = "white";
				entry.input.t.style.background = "white";
				entry.validTime = true;
		} else {
			entry.td.t.style.background = "salmon";
			entry.input.t.style.background = "salmon";
			entry.validTime = false;
		}
	} // timeCheck
	let keyupColorCheck = function(e) {
		let r = parseInt(entry.input.r.value),
			g = parseInt(entry.input.g.value),
			b = parseInt(entry.input.b.value);
		if ((r != NaN) && (g != NaN) && (b != NaN)
			&& (r >= 0) && (g >= 0) && (b >= 0)
			&& (r <= 255) && (g <= 255) && (b <= 255)) {
				entry.td.r.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.td.g.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.td.b.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.input.r.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.input.g.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.input.b.style.background = "rgb(" + r + "," + g + "," + b + ")";
				entry.validColor = true;
		} else {
			entry.td.r.style.background = "white";
			entry.td.g.style.background = "white";
			entry.td.b.style.background = "white";
			entry.input.r.style.background = "white";
			entry.input.g.style.background = "white";
			entry.input.b.style.background = "white";
			entry.validColor = false;
		}
	} // end keyupColorCheck
	let clickRemoveEntry = function(e) {
		entry.tr.remove();
		entryReferences.splice(entryReferences.indexOf(entry), 1);
	} // removeEntry

	entry.input.t.onkeyup = keyupTimeCheck;
	entry.input.r.onkeyup = keyupColorCheck;
	entry.input.g.onkeyup = keyupColorCheck;
	entry.input.b.onkeyup = keyupColorCheck;
	entry.removeButton.onclick = clickRemoveEntry;

	// add to references
	entryReferences.push(entry);
}