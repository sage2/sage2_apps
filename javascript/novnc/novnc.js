//
// SAGE2 application: novnc
// by: Luc Renambot <renambot@gmail.com>
//
// Copyright (c) 2015
//

"use strict";


/* global RFB */

var novnc = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous"; // onfinish
		// this.moveEvents   = "continuous";

		if (this.state.server && this.state.pass) {
			this.connect(this.state.server, this.state.pass);
		}
	},

	load: function(date) {
		console.log('novnc> Load', this.state.server, this.state.pass);
		if (this.state.server && this.state.pass) {
			this.connect(this.state.server, this.state.pass);
		}
		this.refresh(date);
	},

	draw: function(date) {
		// console.log('novnc> Draw');
	},

	resize: function(date) {
		// send a resize to the canvas
		if (this.rfb) {
			this.rfb._updateScale();
		}
		// Called when window is resized
		this.refresh(date);
	},

	move: function(date) {
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
		if (this.rfb) {
			// Close the VNC connection
			this.rfb.disconnect();
		}
	},

	connect: function(server, pass) {
		var _this = this;
		if (this.rfb) {
			// Close the VNC connection
			this.rfb.disconnect();
		}
		// the RFB module default export
		var VNC = RFB.default;
		this.rfb = new VNC(this.element,
			server,
			{
				shared: true,
				credentials: { password: pass }
			});
		this.rfb.scaleViewport = true;
		this.rfb.resizeSession = false;
		this.rfb.viewOnly = true;
		this.rfb.addEventListener("connect",  function(evt) {
			_this.log('noVNC', 'connected', evt.detail);
		});
		this.rfb.addEventListener("disconnect",  function(evt) {
			_this.log('noVNC', 'disconnect', evt.detail);
		});
		this.rfb.addEventListener("credentialsrequired", function (evt) {
			_this.log('noVNC', 'credentialsrequired', evt.detail);
		});
		this.rfb.addEventListener("desktopname", function (evt) {
			_this.log('noVNC', 'desktopname', evt.detail.name);
			_this.updateTitle("noVNC: " + evt.detail.name);
		});

	},

	setURL: function(msgParams) {
		let newurl = msgParams.clientInput;
		if (newurl) {
			this.state.server = newurl;
			this.SAGE2Sync(true);
			this.getFullContextMenuAndUpdate();
			if (this.state.server && this.state.pass) {
				this.connect(this.state.server, this.state.pass);
			}
		}
	},

	setPassword: function(msgParams) {
		let newpass = msgParams.clientInput;
		if (newpass) {
			this.state.pass = newpass;
			this.SAGE2Sync(true);
			this.getFullContextMenuAndUpdate();
			if (this.state.server && this.state.pass) {
				this.connect(this.state.server, this.state.pass);
			}
		}
	},

	getContextEntries: function() {
		var entries = [];

		var entry   = {};
		// label of them menu
		entry.description = "Type websocket URL:";
		// callback
		entry.callback = "setURL";
		// parameters of the callback function
		entry.parameters     = {};
		entry.value          = this.state.server;
		entry.inputDefault   = this.state.server;
		entry.inputField     = true;
		entry.inputFieldSize = 40;
		entries.push(entry);

		entry = {};
		// label of them menu
		entry.description = "Type password:";
		// callback
		entry.callback = "setPassword";
		// parameters of the callback function
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 38;
		entries.push(entry);

		return entries;
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		} else if (eventType === "pointerMove" && this.dragging) {
			// move
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// click release
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// widget events
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") {
				// left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.refresh(date);
			}
		}
	}
});
