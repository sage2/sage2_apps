
//
// SAGE2 application: skeletonWebviewApp
// by: Dylan Kobayashi <dylank@hawaii.edu>
//
// Copyright (c) 2018
//

imageplot_global = {
	map: null,
	imageGeoPath: null,
	imageGeoPathPoints: [],
	lastKnownWindowWidth: -1,
	lastKnownWindowHeight: -1,
	resizeCheckerFunction: null,
	mapHeightPercent: 0.75,
	imageHeightPercent: 0.22, // May not total 100% depending

	marker: {
		lastClicked: null,
		lastInfoObject: null,
	},
	slides: {
		lastScroll: null,
		allThumbnailPaths: [],
	},
	selection: {
		current: null,
		index: null,
		preventChangeDuration: 100,
		lastChangeTime: Date.now(),
	}
}

allImages = [];

initializeGoogleMaps();
startResizeChecker();
addImageSlidesScrollListener();
startListenerForSage2StateUpdate();
addDocumentListenersForArrowControl();


// ------------------------------------------------------------------------------------------------------------------------
function initializeGoogleMaps() {
	// The function to initialize is actually below, it is called after the API loads
}
function postLoadInitializationForGoogleMaps() {
	imageplot_global.map = new google.maps.Map(document.getElementById("mapDiv"), {
		zoom: 7,
		center: {lat: 21.29843, lng: -157.816434},
	});
}

// ------------------------------------------------------------------------------------------------------------------------
// Interval to check if need to resize
function startResizeChecker() {
	resizeToMatchWindowDimensions();

	imageplot_global.resizeCheckerFunction = setInterval(() => {
		if ( (imageplot_global.lastKnownWindowWidth !== window.innerWidth)
			|| imageplot_global.lastKnownWindowHeight !== window.innerHeight) {
				resizeToMatchWindowDimensions();
		}
	}, 1000); // How often?
}
// Need to resize the image divs
function resizeToMatchWindowDimensions() {
	imageplot_global.lastKnownWindowWidth = window.innerWidth;
	imageplot_global.lastKnownWindowHeight = window.innerHeight;

	mapDiv.style.height = window.innerHeight * imageplot_global.mapHeightPercent - 20 + "px";
	mapDiv.style.width = window.innerWidth * 0.95 + "px";
	//
	imageSlides.style.height = window.innerHeight * imageplot_global.imageHeightPercent - 20 + "px";
	imageSlides.style.width = window.innerWidth * 0.95 + "px";
	// imageSlides.style.top = window.innerHeight - (window.innerHeight * imageplot_global.imageHeightPercent) - 10 + "px";

	document.body.style.overflow = "hidden";

	if (imageplot_global.slides.lastScroll === null) {
		for (let i = 0; i < allImages.length; i++) {
			allImages[i].div.style.width = getImageMaxHeightAsNumber() + "px";
			allImages[i].div.style.height = getImageMaxHeightAsNumber() + "px";
			allImages[i].div.style.left = getImageMaxHeightAsNumber() * i + "px";
		}
	}
}

function rebuildImageGeoPath() {
	let path = [];
	for (let i = 0; i < allImages.length; i++) {
		path.push(new google.maps.LatLng(allImages[i].geolocation.lat, allImages[i].geolocation.lng));
	}

	if (!imageplot_global.imageGeoPath) {
		imageplot_global.imageGeoPath = new google.maps.Polyline({
			path: path,
			strokeColor: "#FF0000",
			strokeOpacity: 1.0,
			strokeWeight: 2,
		});
		imageplot_global.imageGeoPath.setMap(imageplot_global.map);
	} else {
		imageplot_global.imageGeoPath.setPath(path);
	}
}

function plotMarkerOnMap(geolocation, infoObject) {
	var marker = new google.maps.Marker({
		position: geolocation,
		map: imageplot_global.map,
		title: "Hello World!"
	});
	marker.addListener("click", function() {
		showImageThisMarkerIsFor(marker, infoObject);
	});
	infoObject.marker = marker;
}

function showImageThisMarkerIsFor(marker, infoObject) {
	// First take care of the symbols, check in function
	makeMarkerAndImageLastClickedNormal();
	setMarkerAndImageToSelectedVisuals(infoObject);
	moveImageToLeft(infoObject);
}

function moveImageToLeft(infoObject) {
	// Now place the image in the slider at appropriate location
	let i = allImages.indexOf(infoObject);
	if (i < 0) {
		console.log("Error, selected marker has no image correlation:", marker, infoObject);
	}
	imageplot_global.selection.current = infoObject;
	imageplot_global.selection.index = i;
	// Move slider
	imageSlides.style.left = -1 * i * getImageMaxHeightAsNumber() + "px";
	// Remove the last known scroll value
	imageplot_global.slides.lastScroll = -1 * i * getImageMaxHeightAsNumber() + "px";
}

function setMarkerAndImageToSelectedVisuals(infoObject) {
	infoObject.marker.setIcon({
		path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
		scale: 5,
	});
	infoObject.image.className = "imageDivSelected";
	imageplot_global.marker.lastClicked = infoObject;
}

function makeMarkerAndImageLastClickedNormal() {
	if (imageplot_global.marker.lastClicked) {
		// null means use default, i guess
		imageplot_global.marker.lastClicked.marker.setIcon(null);
	
		// return selection border from white to red
		imageplot_global.marker.lastClicked.image.className = "imageDivNeutral";
	}
}




// ------------------------------------------------------------------------------------------------------------------------
function addImage(path, geolocation, exifTimeModifed) {
	// Cannot order without geo
	if (!geolocation) {
		return;
	}
	//create image node, it isn't yet attached to the page, since needs to check time
	let infoObject = createImageInformationObject(path);

	if (typeof geolocation === "string") {
		infoObject.geolocation = makeGeoLocationFromString(geolocation);
	} else if (typeof geolocation === "object") {
		infoObject.geolocation = geolocation;
	}

	// Place image correct in allImages array and the div band
	insertNewImage(infoObject, exifTimeModifed);

	// Plot and rebuild the path of the map if necessary
	rebuildImageGeoPath();

	// Show marker
	plotMarkerOnMap(infoObject.geolocation, infoObject);

	// Focus map on new point
	imageplot_global.map.setCenter(infoObject.geolocation);

	// Add click listener to image
	addImageClickListener(infoObject);
}

// Assumes was correctly converted in SAGE2 section
function makeGeoLocationFromString(geolocation) {
	let parts = geolocation.split(" ");
	if (parts.length < 2) {
		console.log("ERROR: geolocation problem, incorrect format of " + geolocation);
	}
	parts = {lat: parseFloat(parts[0]), lng: parseFloat(parts[1])};
	return parts;
}

// Returns object with the image and container elemnt as properties: image, div
function createImageInformationObject(path) {
	let image = document.createElement("img");
	image.src = path;

	// Images are square size limited
	image.style.height = "90%"; // Account for border space
	image.style.width = "90%";
	// Center the image
	image.style.left = "50%";
	image.style.top = "50%";
	image.style.transform = "translate(-50%, -50%);";
	// Allow them to "stack"
	image.className = "imageDivNeutral";
	
	// Make div container, so image can center inside of it
	let imageDiv = document.createElement("div");
	imageDiv.style.width = getImageMaxHeightAsNumber() + "px";
	imageDiv.style.height = getImageMaxHeightAsNumber() + "px";
	imageDiv.className = "imageDiv";
	imageDiv.appendChild(image);

	let imageInformationObject = {
		image,
		div: imageDiv,
		path
	};
	return imageInformationObject;
}

// Image height is based on the size of the window
function getImageMaxHeightAsNumber() {
	return (window.innerHeight * imageplot_global.imageHeightPercent);
}

// Image height is based on the size of the window
function insertNewImage(infoObject, exifTimeModifed) {

	const dateObject = getProperDateObject(exifTimeModifed);
	infoObject.date = dateObject;
	let wasBefore = false;
	// For each image being tracked, check if the new one was taken at a time prior to any in the set
	for (let i = 0; i < allImages.length; i++) {
		// If the current date is behind the one being checked (negative result)
		if (dateObject - allImages[i].date < 0) {
			// Insert to page, allImages
			imageSlides.insertBefore(infoObject.div, allImages[i].div);
			allImages.splice(i, 0, infoObject);
			wasBefore = true;
			infoObject.div.style.left = getImageMaxHeightAsNumber() * i + "px";
			infoObject.div.style.top ="0px";
			i++; // increment because inserted before another
			while (i < allImages.length) {
				allImages[i].div.style.left = getImageMaxHeightAsNumber() * i + "px";
				i++;
			}
			break;
		}
	}

	if (!wasBefore) {
		// special case, if this is the first one
		if (allImages.length === 0) {
			imageplot_global.selection.current = infoObject;
			imageplot_global.selection.index = 0;
		}
		infoObject.div.style.left = getImageMaxHeightAsNumber() * allImages.length + "px";
		infoObject.div.style.top ="0px";
		allImages.push(infoObject);
		imageplot_global.slides.allThumbnailPaths.push(infoObject.path);
		imageSlides.appendChild(infoObject.div);
	}
}

// Correct format should be "2018-06-04 14:12:56"
function getProperDateObject(exifTimeModifed){
	if (!exifTimeModifed) {
		return new Date(); // return now if not given a time
	}
	let parts = exifTimeModifed.split(" "); // example "2018:06:04 14:12:56-10:00"
	// Convert year month day to - separated
	let proper = parts[0].replace(/:/g, "-"); // g(globally in string) replace all : with -
	// Get rid of the -10:00, unsure where that came from. If - is not present, 0 will still work.
	proper += " " + parts[1].split("-")[0];

	return new Date(proper);
}

function addImageClickListener(infoObject) {
	infoObject.image.addEventListener("click", function() {
		// Set last to normal
		makeMarkerAndImageLastClickedNormal();
		// Set this one to selected
		setMarkerAndImageToSelectedVisuals(infoObject);
		imageplot_global.map.panTo(infoObject.geolocation);
		moveImageToLeft(infoObject);
	});
}



// ------------------------------------------------------------------------------------------------------------------------
function addImageSlidesScrollListener() {
	document.addEventListener("wheel", function(e) {
		if (!imageplot_global.slides.lastScroll) {
			imageplot_global.slides.lastScroll = 0;
		}
		imageplot_global.slides.lastScroll += e.wheelDelta;
		imageSlides.style.left = imageplot_global.slides.lastScroll + "px";
		// console.log(e.wheelDelta, imageSlides.style.left, imageplot_global.slides.lastScroll);
		e.preventDefault();
	});
}



// ------------------------------------------------------------------------------------------------------------------------
function startListenerForSage2StateUpdate() {
	SAGE2_AppState.addFullStateHandler(s2FullStateHandler);
	console.log("Ready to receive full state update from SAGE2");
}

function s2FullStateHandler(state) {
	// need TODO;

	for (i = 0; i < state.images.length; i++) {
		// If haven't already plotted images
		if (imageplot_global.slides.allThumbnailPaths.indexOf(state.images[i].path) === -1) {
			addImage(state.images[i].path, state.images[i].geolocation, state.images[i].date);
			imageplot_global.slides.allThumbnailPaths.push(state.images[i].path)
		} else {
		}
	}
}

// ------------------------------------------------------------------------------------------------------------------------
function addDocumentListenersForArrowControl() {
	// Add listner for arrow left and right to cycle through images.
	document.body.addEventListener("keydown", (e) => {
		console.log("keydown:" + e.keyCode);
		let mod = 0;
		if ((e.keyCode === 37) || (e.keyCode === 38)) { // left (37) or up (38)
			mod = -1;
		} else if ((e.keyCode === 39) || (e.keyCode === 40)) { // right (39) or down (40)
			mod = 1;
		}

		if (Date.now() > imageplot_global.selection.lastChangeTime + imageplot_global.selection.preventChangeDuration) {
			imageplot_global.selection.lastChangeTime = Date.now();
			if ((mod != 0) && allImages.length > 0) {
				imageplot_global.selection.index += mod;
				if (imageplot_global.selection.index < 0) {
					imageplot_global.selection.index = allImages.length - 1;
				} else if (imageplot_global.selection.index >= allImages.length) {
					imageplot_global.selection.index = 0;
				}
	
				// Dispatch click event
				imageplot_global.selection.current = allImages[imageplot_global.selection.index];
				imageplot_global.selection.current.image.dispatchEvent(new MouseEvent("click"));
			}
		}
	});

}


// ------------------------------------------------------------------------------------------------------------------------

function mapZoom(dir) {
	if (dir === "zoomin") {
		imageplot_global.map.setZoom(imageplot_global.map.getZoom() + 1);
	} else if (dir === "zoomout") {
		imageplot_global.map.setZoom(imageplot_global.map.getZoom() - 1);
	}
}


// ------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------
// Test functions


function test_addImage1() {
	// addImage(path, geolocation, exifTimeModifed)
	// addImage("https://mokulua.manoa.hawaii.edu/user/assets/sage.jpg_512.jpg", {lat: 21.298385, lng: -157.817340});
	addImage("https://localhost/user/assets/sage.jpg_512.jpg", {lat: 21.298385, lng: -157.817340});
}

function test_addImage2() {
	// addImage(path, geolocation, exifTimeModifed)
	// addImage("https://mokulua.manoa.hawaii.edu/user/assets/sage.jpg_512.jpg", "21.271822 -157.697177");
	addImage("https://localhost/user/assets/sage.jpg_512.jpg", "21.271822 -157.697177");
}
