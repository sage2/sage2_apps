// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014-17

"use strict";

/* global  SAGE2Connection*/

/*
File has parts:
	1. Any actions needed to run before connecting to server.
	2. Connect to server
	3. Functions that the app expects to call
	4. Functions for usage in the page

*/

/* ------------------------------------------------------------------------------------------------------------------
// 1
// ------------------------------------------------------------------------------------------------------------------
Finalize the page
*/


let sessionNameList = [];
let currentRadioSelection = null;


// add event handlers

// handlers for making new sessions
input_makeNewSession.addEventListener("keyup", (e) => {
	if ((e.code === "Enter") || (e.code === "NumpadEnter")) this.toS2_clearAndSendMakeNewSessionRequest() ;// console.log(e); // code "Enter"
	// console.log(e.code);
});
button_makeNewSession.addEventListener("click", toS2_clearAndSendMakeNewSessionRequest);

// handlers for switching session
button_switch.addEventListener("click", toS2_switchToSelectedRadioSession);
button_remove.addEventListener("click", toS2_removeSession);


// add manual save event
button_saveNow.addEventListener("click", toS2_saveSessionNow);


/* ------------------------------------------------------------------------------------------------------------------
// 2
// ------------------------------------------------------------------------------------------------------------------
Connect to the server
*/

// first describe any reaction after connecting
SAGE2Connection.afterSAGE2Connection = toS2_addThisClientAsEditor;

// This this is part of app code, it will use the current window values to connect.
// But if the page was hosted elsewhere, parameters would be required.
SAGE2Connection.initS2Connection();

/* ------------------------------------------------------------------------------------------------------------------
// 3
// ------------------------------------------------------------------------------------------------------------------
The following functions are for communication handling.
*/

// After establishing connection, call this function to let app know this page is acting as editor.
function toS2_addThisClientAsEditor() {
	// callFunctionOnApp: function(functionName, parameterObject) { // autofilled id by server
	SAGE2Connection.callFunctionOnApp("addClientIdAsEditor", {});
}

function fromS2_sessionNameList(data) {
	setSessionNameList(data.nameList);
}

function fromS2_newSessionName(data) {
	addSessionNameToList(data.newSessionName);
}

function fromS2_switchedToSession(data) {
	console.log("erase me, fromS2_switchedToSession", data);
	currentSessionName.textContent = data.sessionName;
}


function toS2_clearAndSendMakeNewSessionRequest() {
	let newName = input_makeNewSession.value;
	newName = newName.trim(); // clear pre and post whitepsace
	input_makeNewSession.value = ""; // clear
	if (newName.length > 0) {
		SAGE2Connection.callFunctionOnApp("fromControls_handleNewSessionRequest", {newSessionName: newName});
	}
}

function toS2_switchToSelectedRadioSession() {
	SAGE2Connection.callFunctionOnApp("fromControls_switchToSessionNameRequest", {sessionName: currentRadioSelection});
}

function toS2_removeSession() {
	if (currentRadioSelection === null) {
		alert("Must select a session before removing");
	} else if (currentSessionName.textContent.trim() === currentRadioSelection.trim()) {
		alert("Sorry, you cannot remove a session while it is active");
	} else if (window.confirm("Are you sure you want to remove session: " + currentRadioSelection)) {
		SAGE2Connection.callFunctionOnApp("fromControls_removeSession", {sessionName: currentRadioSelection});
	}
}

function toS2_saveSessionNow() {
	SAGE2Connection.callFunctionOnApp("saveCurrentSession", {});
}







/* ------------------------------------------------------------------------------------------------------------------
// 4
// ------------------------------------------------------------------------------------------------------------------
Functions for usage in the page
*/

function setSessionNameList (nameList) {
	sessionNameList = nameList;
	form_sessionNameList.innerHTML = ""; // clear out
	for (let i = 0; i < sessionNameList.length; i++) {
		form_sessionNameList.appendChild(makeSessionEntry("sessionEntry" + i, sessionNameList[i]).div);
	}
}

function makeSessionEntry(id, sessionName) {
	let data = {};
	data.div = document.createElement("div");
	data.input = document.createElement("input");
	data.label = document.createElement("label");

	data.input.type = "radio";
	data.input.id = id;
	data.input.sessionName = sessionName;
	data.input.name = "sessionList";
	data.input.value = sessionName;
	data.input.addEventListener("change", (e) => {
		currentRadioSelection = e.target.value;
	})

	data.label.setAttribute("for", id);
	data.label.textContent = " " + sessionName;

	data.div.appendChild(data.input);
	data.div.appendChild(data.label);
	return data;
}

function addSessionNameToList(name) {
	sessionNameList.push(name);
	form_sessionNameList.appendChild(makeSessionEntry("sessionEntry" + (sessionNameList.length - 1), sessionNameList[sessionNameList.length - 1]).div);
}

