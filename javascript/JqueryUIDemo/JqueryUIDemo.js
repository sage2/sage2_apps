//
// SAGE2 application: JqueryUIDemo
// by: Luc Renambot <renambot@gmail.com>
//
// Copyright (c) 2016
//

"use strict";

/* global $ */


var JqueryUIDemo = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("div", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous";

		// SAGE2 Application Settings
		// inner container
		var design_id = "desdiv_" + this.id;
		this.designdiv = document.createElement("div");
		this.designdiv.id = design_id;
		this.designdiv.style.position  = "absolute";
		this.designdiv.style.top       = 0;
		this.designdiv.style.left      = 0;
		this.designdiv.style.width     = "100%";
		this.designdiv.style.height    = "100%";

		// wrapper container
		var wrap_id = "wrapdiv_" + this.id;
		this.wrapdiv = document.createElement("div");
		this.wrapdiv.id = wrap_id;
		this.wrapdiv.style.position = "absolute";
		this.wrapdiv.style.left = 0;
		this.wrapdiv.style.top  = 0;

		this.element.appendChild(this.wrapdiv);
		this.wrapdiv.appendChild(this.designdiv);

		// button inside the inner container
		$('#' + design_id).append('<input type="button" value="Red" id=toto1>');
		var button = $('#toto1').button();
		button.css({
			position: "absolute",
			width: 200,
			top: 4, left: 4,
			fontSize:  3 * this.config.ui.titleTextSize
		});
		button.click(this.button1.bind(this));

		$('#' + design_id).append('<input type="button" value="Green" id=toto2>');
		button = $('#toto2').button();
		button.css({
			position: "absolute",
			top: 100, left: 4,
			width: 200,
			fontSize:  3 * this.config.ui.titleTextSize
		});
		button.click(this.button2.bind(this));

		$('#' + design_id).append('<input type="button" value="Blue" id=toto3>');
		button = $('#toto3').button();
		button.css({
			position: "absolute",
			top: 200, left: 4,
			width: 200,
			fontSize:  3 * this.config.ui.titleTextSize
		});
		button.click(this.button3.bind(this));

		// save the original width
		this.initialWidth = data.width;


		this.enableControls = false;
		// Pass SAGE2 events into the DOM
		this.passSAGE2PointerAsMouseEvents = true;
	},

	resize: function(date) {
		// Scale the content div
		var scaleup = this.sage2_width / this.initialWidth;
		this.designdiv.style.transform = "scale(" + scaleup + ")";
		this.refresh(date);
	},

	// Callbacks from JQuery buttons
	button1: function(evt) {
		console.log('Button1 was clicked');
		this.element.style.backgroundColor = 'red';
	},
	button2: function(evt) {
		console.log('Button2 was clicked');
		this.element.style.backgroundColor = 'green';
	},
	button3: function(evt) {
		console.log('Button2 was clicked');
		this.element.style.backgroundColor = 'blue';
	},

	load: function(date) {
		// console.log('JqueryUIDemo> Load');
		this.refresh(date);
	},

	draw: function(date) {
		// console.log('JqueryUIDemo> Draw');
	},

	move: function(date) {
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
	}
});
