// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2015

var twitter_frame = SAGE2_App.extend({
	init: function(data) {
		this.SAGE2Init("div", data);

		this.resizeEvents = "continuous"; //"onfinish";
		this.maxFPS = 2;

		this.element.id = "div" + data.id;

		this.twitterDiv = document.createElement("div");
		this.twitterDiv.innerHTML = "<a class=\"twitter-timeline\" " +
			"href=\"https://twitter.com/search?q=%23dmdii%20OR%20%23uilabs%20OR%20%23cityworks%20OR%20%23innovationisland\" " +
			"data-widget-id=\"596787236365971456\" width=\"520\" height=\"800\" data-theme=\"dark\">" +
			"Tweets about #dmdii OR #uilabs OR #cityworks OR #innovationisland</a>";
		var scale = this.element.clientWidth / 520;
		this.twitterDiv.style.webkitTransform = "scale(" + scale + ")";
		this.twitterDiv.style.mozTransform = "scale(" + scale + ")";
		this.twitterDiv.style.transform = "scale(" + scale + ")";
		this.twitterDiv.style.webkitTransformOrigin = "0% 0%";
		this.twitterDiv.style.mozTransformOrigin = "0% 0%";
		this.twitterDiv.style.transformOrigin = "0% 0%";
		var twitterScript = document.createElement('script');
		twitterScript.type = "text/javascript";
		twitterScript.onload = function() {
			console.log("twitter script loaded!");
		};
		this.twitterDiv.appendChild(twitterScript);
		twitterScript.textContent = "!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?" +
		"'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://" +
		"platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");";

		this.element.appendChild(this.twitterDiv);
		this.element.style.backgroundColor = "white";
	},

	load: function(date) {
	},

	draw: function(date) {
		var scale = this.element.clientWidth / 520;

		this.twitterDiv.style.webkitTransform = "scale(1)";
		this.twitterDiv.style.mozTransform = "scale(1)";
		this.twitterDiv.style.transform = "scale(1)";

		// var tmp = this.twitterDiv.offsetHeight;

		this.twitterDiv.style.webkitTransform = "scale(" + scale + ")";
		this.twitterDiv.style.mozTransform = "scale(" + scale + ")";
		this.twitterDiv.style.transform = "scale(" + scale + ")";
	},

	resize: function(date) {
		var scale = this.element.clientWidth / 520;

		this.twitterDiv.style.webkitTransform = "scale(" + scale + ")";
		this.twitterDiv.style.mozTransform = "scale(" + scale + ")";
		this.twitterDiv.style.transform = "scale(" + scale + ")";
		this.refresh(date);
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		}
		if (eventType === "pointerMove") {
			// move
		}
		if (eventType === "pointerRelease" && (data.button === "left")) {
			// release
		}
	}

});

