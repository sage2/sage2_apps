#include <stdio.h>
#include <stdlib.h>
#include <image.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/types.h>
#include "image.h"
#include "imginfo.h"

IMG *
SGImakedisp(fname)
	char *fname;
{
	IMAGE *image;
	register IMG *im;
	unsigned short *trow;
	register int k;
	int width, height;
	int y, z;
	static int one = 1;

	int littleendian = (*(char *)&one == 1);

	image = iopen(fname, "r");
	if(image == NULL)
	    return NULL;

	width = image->xsize;
	height = image->ysize;
	im = (IMG *)malloc(sizeof(IMG));
	im->rowbytes = sizeof(u_int32_t) * width;
	im->type = IT_LONG;
	im->xsize = width;
	im->ysize = height;
	im->data = (unsigned char *)malloc(im->rowbytes * height + (sizeof(short)*width));
	if(im->data == NULL) {
	    fprintf(stderr, "Can't malloc %d bytes of memory for image\n",
		im->rowbytes * height);
	    exit(2);
	}
	trow = (unsigned short *)(im->data + im->rowbytes * height);
	if(image->zsize == 3)
	    memset(im->data, 255, im->rowbytes * height);

	for(y = 0; y < image->ysize; y++) {
	    for(z = 0; z < image->zsize; z++) {
		register unsigned char *op;
		register unsigned short *ip;
		

		getrow(image, trow, y, z);
		k = image->xsize;
		op = (unsigned char *) (im->data + im->rowbytes * y)
					+ (littleendian ? z : 3 - z);
		ip = trow;
		do {
		    *op = *ip++;
		    op += sizeof(u_int32_t);
		} while(--k > 0);
	    }
	}
	if(image->zsize == 1) {
	    register u_int32_t *op;
	    register int v;

	    k = image->xsize * image->ysize;
	    op = (u_int32_t *)im->data;
	    do {
		v = (*op & 0xFF);	/* XXX byte-order dependent */
		*op++ = v | (v<<8) | (v<<16) | 0xFF000000;
	    } while(--k > 0);
	}
	iclose(image);

	return im;
}
