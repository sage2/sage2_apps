/*
 * Given a PNG file, return a DISPIMAGE
 * containing its raster data and with size <= (xsize, ysize).
 */

#include <stdio.h>
#include <stdlib.h>
#include <png.h>
#include "imginfo.h"

#ifndef alloca
# include <alloca.h>
#endif

static int littleendian() {
    static int one = 1;
    return(*(char *)&one);	/* = 1 on little-endians, 0 on big-endians */
}


IMG *
pngmakedisp(char *filename)
{
    png_structp png_ptr;
    png_infop info_ptr, end_info;
    FILE *fp;
    int i, width, height, color_type, bit_depth;
    IMG *im;
    png_bytep *row_pointers;

    if((fp = fopen(filename, "rb")) == NULL) {
	return NULL;
    }

    png_ptr = png_create_read_struct(
	   PNG_LIBPNG_VER_STRING, (png_voidp)NULL/*user_error_ptr*/,
	   NULL/*user_error_fn*/, NULL/*user_warning_fn*/);
    if (!png_ptr)
	   return NULL;

    if((info_ptr = png_create_info_struct(png_ptr)) == NULL) {
	png_destroy_read_struct( &png_ptr,
	      (png_infopp)NULL, (png_infopp)NULL);
	   return NULL;
    }

    if((end_info = png_create_info_struct(png_ptr)) == NULL) {
	png_destroy_read_struct( &png_ptr, &info_ptr,
	     (png_infopp)NULL );
	return NULL;
    }

    if( setjmp( png_jmpbuf(png_ptr) ) ) {
	/* png exception */
	fprintf(stderr, "pngdisp(\"%s\"): png exception?\n", filename);
	png_destroy_read_struct( &png_ptr, &info_ptr, &end_info );
	return NULL;
    }

    png_init_io( png_ptr, fp);
    png_read_info( png_ptr, info_ptr );

    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    color_type = png_get_color_type(png_ptr, info_ptr);

    if(color_type == PNG_COLOR_TYPE_PALETTE)
	png_set_palette_to_rgb(png_ptr);

    if(color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
	png_set_gray_1_2_4_to_8(png_ptr);

    if(png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
	png_set_tRNS_to_alpha(png_ptr);

    if(bit_depth == 16)
	png_set_strip_16(png_ptr);
    else if(bit_depth < 8)
	png_set_packing(png_ptr);

    if(color_type == PNG_COLOR_TYPE_GRAY ||
	    color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
	png_set_gray_to_rgb(png_ptr);

    if(littleendian()) {

	if(color_type == PNG_COLOR_TYPE_RGB)
	    png_set_filler(png_ptr, 0xFF, PNG_FILLER_AFTER);

    } else {

	/* big-endian. */
	png_set_bgr(png_ptr);

	if(color_type == PNG_COLOR_TYPE_RGB_ALPHA) {
	    png_set_swap_alpha(png_ptr);
	} else if(color_type == PNG_COLOR_TYPE_RGB) {
	    png_set_filler(png_ptr, 0xFF, PNG_FILLER_BEFORE);
	}
    }


    /* now all transformations have been registered --
     * update info so we know rowbytes
     */
    png_read_update_info(png_ptr, info_ptr);

    im = (IMG *)malloc(sizeof(IMG));
    im->xsize = width;
    im->ysize = height;
    im->type = IT_LONG;

    im->rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    im->data = (void *)malloc( height * im->rowbytes );
    if(im->data == NULL) {
	fprintf(stderr, "libimg pngdisp: %s: no room for %dx%d => %d*%d = %d byte image\n",
		filename, width, height, height,
		im->rowbytes, height*im->rowbytes);
	png_destroy_read_struct( &png_ptr, &info_ptr, &end_info );
	fclose(fp);
	return NULL;
    }

    row_pointers = (png_bytep *)malloc( height * sizeof(png_bytep) );

    for(i = 0; i < height; i++)
	row_pointers[i] = im->data + (height-1 - i)*im->rowbytes;

    png_read_image( png_ptr, row_pointers );
    png_read_end(png_ptr, NULL);

    png_destroy_read_struct( &png_ptr, &info_ptr, &end_info );
    fclose(fp);

    return im;
}
