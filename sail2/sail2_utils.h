// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <math.h>
#include <ctime>
#include <string>

char *base64_encode(const unsigned char *data, std::size_t input_length, size_t *output_length);

std::string convertToJpeg(unsigned char* buffer, int width, int height);

double getTime();
void   initTime();

