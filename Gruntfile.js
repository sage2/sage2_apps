var fs   = require('fs');
var path = require('path');

module.exports = function(grunt) {
	// Init
	grunt.initConfig({
		// ESLINT
		eslint: {
			options: {
				// fix: true
			},
			appsFiles: {
				files: {
					src: [
					"javascript/CSV_Reader/CSV_Reader.js",
					"javascript/ChemViewer/ChemViewer.js",
					"javascript/JqueryUIDemo/JqueryUIDemo.js",
					"javascript/ParaSAGE/ParaSAGE.js",
					"javascript/Timer/Timer.js",
					"javascript/Timezone/Timezone.js",
					"javascript/aMepTest/aMepTest.js",
					"javascript/atom_smasher/atom_smasher.js",
					"javascript/bounce/bounce.js",
					"javascript/car_threejs/car_threejs.js",
					"javascript/cave2_monitor/cave2_monitor.js",
					"javascript/clock/clock.js",
					"javascript/d3_sample/d3_sample.js",
					"javascript/dash/dash.js",
					"javascript/evl_photos/evl_photos.js",
					"javascript/evl_weather/weather.js",
					"javascript/forecast/forecast.js",
					"javascript/gmaps/gmaps.js",
					"javascript/iframe/iframe.js",
					"javascript/image360/image360.js",
					"javascript/kinetic_animation/kinetic_animation.js",
					"javascript/kinetic_oscillating/kinetic_oscillating.js",
					"javascript/modest/modest.js",
					"javascript/multi_pdf_viewer/pdf_viewer.js",
					"javascript/osgjs/osgjs.js",
					"javascript/p5demo/p5demo.js",
					"javascript/physics3d/physics3d.js",
					"javascript/playcanvas/playcanvas.js",
					"javascript/presentation/presentation.js",
					"javascript/prime/prime.js",
					"javascript/radar/radar.js",
					"javascript/scale_div/scale_div.js",
					"javascript/snap_one/snap_one.js",
					"javascript/stereo_viewer/stereo_viewer.js",
					"javascript/tank/tank.js",
					"javascript/texture_cube/texture_cube.js",
					"javascript/threejs_sample/threejs_sample.js",
					"javascript/threejs_shader/threejs_shader.js",
					"javascript/tweetcloud/tweetcloud.js",
					"javascript/tweetcloud2/tweetcloud.js",
					"javascript/twitter_frame/twitter_frame.js",
					"javascript/unity/unity.js",
					"javascript/webrtc/webrtc.js",
					"javascript/whiteboard/whiteboard.js",
					"javascript/widget_demo/widget_demo.js"
					]
				}, 
				options: { configFile: "config/eslint_client_rc"}
			}
		}
	});

	// Load the dependencies
	grunt.loadNpmTasks('grunt-eslint');

	// this would be run by typing "grunt test" on the command line
	grunt.registerTask('all', ['eslint']);

	// the default task can be run just by typing "grunt" on the command line
	grunt.registerTask('default', ['eslint']);
};

