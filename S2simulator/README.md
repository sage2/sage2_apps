Test SAGE2 applications outside the SAGE2 server

* Copy your application into the 'apps' folder
* Edit app.js file and fill up the appData variable, mimicing instructions.json (see commented examples)
* Open index.html in your browser
	* Check the broswer javascript console for errors 
	* Often applications make request to files that needs to be accessed through a webserver. For this, you need to expose the folder to a webserver. This can be done easily from the command line in Python or Node.JS
		* using python (builtin on MacOSX for instance)
			* *python -m SimpleHTTPServer*
				- exposes the current folder and its files through HTTP 
				- open in your browser: http://localhost:8000/
			* to change the default port number, add a number
				- *python -m SimpleHTTPServer 3000*
		* using Node.js, install the 'serve' package
			* install: *npm install -g serve*
				- -g: global installation
			* now, use the command 'serve' to expose the current folder
			* *serve*
  				- open in your browser: http://localhost:3000/
			* to change the default port, use the '-p' option
				* *serve* -p 8000


December 2015
Luc Renambot - renambot@gmail.com
