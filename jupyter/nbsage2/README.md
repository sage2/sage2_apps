# SAGE2 extension for Jupyter

Works in Python3 notebook (kernel)

Needs widget to be loaded 

* jupyter nbextension enable --py widgetsnbextension

To install:

* % jupyter nbextension install nbsage2 --user
* —> copy into  ~/.local/share/jupyter/nbextensions/nbsage2

Enable it:

* % jupyter nbextension enable sage2/sageExtension
* —>   ~/.jupyter/nbconfig/notebook.json

Using it:
* using a python kernel
* connect to a SAGE2 running instance (see 'cloud' icon in screenshot)
* push the image from the current cell (see 'arrow up' icon in screenshot)
* continue interacting with graph in jupyter, the graph in SAGE2 gets updated

